
if ("$?EUPS_DIR" == "1" ) then
   setenv PATH `echo $PATH | perl -pe "s|:$EUPS_DIR/bin||g"`
   if ("$?PYTHONPATH" == "1" ) then
      setenv PYTHONPATH `echo $PYTHONPATH | perl -pe "s|:$EUPS_DIR/python||g"`
   endif
endif

setenv EUPS_DIR /net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/1.2.30
if ("$?EUPS_PATH" == "0" ) then
    setenv EUPS_PATH ""
endif

# Set EUPS_PATH, appending any pre-existing EUPS_PATH (and only keeping
# one copy of each directory)
setenv EUPS_PATH `python` << EOT

import sys
pp = []
for d in "$EUPS_PATH".split(":"):
    if d and d not in pp:
        pp += [d]
if not "/net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/packages" in pp:
    pp = ["/net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/packages"] + pp
print ":".join(pp)
EOT
# Set SETUP_EUPS so that a "setup eups" will remove this EUPS_DIR/bin from PATH
setenv SETUP_EUPS "eups"
# Deprecated variables
unsetenv PROD_DIR_PREFIX
unsetenv PRODUCTS

setenv PATH ${PATH}:$EUPS_DIR/bin
if ("$?PYTHONPATH" == "1" ) then
   setenv PYTHONPATH ${PYTHONPATH}:$EUPS_DIR/python
else
   setenv PYTHONPATH $EUPS_DIR/python
endif

# csh shells die if variables are undefined, so we'll define an (empty) DYLD_LIBRARY_PATH
# if there isn't one already
if (! $?DYLD_LIBRARY_PATH) then
    setenv DYLD_LIBRARY_PATH ""
endif

alias setup 'eval `$EUPS_DIR/bin/eups_setup DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH} \!*`'
alias unsetup 'eval `$EUPS_DIR/bin/eups_setup DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH} --unsetup \!*`'


