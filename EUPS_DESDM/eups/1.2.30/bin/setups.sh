
if [ "$EUPS_DIR" != "" ]; then
   PATH=`echo $PATH | perl -pe "s|:$EUPS_DIR/bin||g"`
   PYTHONPATH=`echo $PYTHONPATH | perl -pe "s|:$EUPS_DIR/python||g"`
fi

export EUPS_DIR=/net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/1.2.30
# Set EUPS_PATH, appending any pre-existing EUPS_PATH (and only keeping
# one copy of each directory)
export EUPS_PATH=`python -c '
import sys
pp = []
for d in sys.argv[1].split(":"):
    if d and d not in pp:
        pp += [d]
if not sys.argv[2] in pp:
    pp = [sys.argv[2]] + pp
print ":".join(pp)' "$EUPS_PATH" "/net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/packages"`
# Set SETUP_EUPS so that a "setup eups" will remove this EUPS_DIR/bin from PATH
export SETUP_EUPS="eups"
# Deprecated variables
unset PROD_DIR_PREFIX
unset PRODUCTS

export PATH="$PATH:$EUPS_DIR/bin"
if [ X"$PYTHONPATH" != X"" ]; then
    export PYTHONPATH="$PYTHONPATH:$EUPS_DIR/python"
else
    export PYTHONPATH="$EUPS_DIR/python"
fi

function setup   { eval `$EUPS_DIR/bin/eups_setup           DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH} "$@"`; }; export -f setup
function unsetup { eval `$EUPS_DIR/bin/eups_setup --unsetup DYLD_LIBRARY_PATH=${DYLD_LIBRARY_PATH} "$@"`; }; export -f unsetup

[[ -n $BASH_COMPLETION ]] && [[ -f "$EUPS_DIR/etc/bash_completion.d/eups" ]] && source "$EUPS_DIR/etc/bash_completion.d/eups"

