#!/usr/bin/env csh
#
# Sets up the environment variables for this EUPS installation.
# Generated on 2017-08-29 12:52:55.849640
#
# Version of the installer that created this script.
setenv DESDM_EUPSINSTALL_VERSION "2015_Q2"
#
# This script first cleans up potential residual from other EUPS
# installations and then sources the setup file that comes with EUPS.
# It also sets some DESDM specific environment variables.
#
unset EUPS_DIR
unset EUPS_PATH
unset SETUP_EUPS
#
# Remove all other EUPS installations from PATH.
# (Removes all entries from PATH that contain an executable named 'eups'.)
setenv PATH `bash -c 'for path in $(echo $PATH | tr ":" "\n"); do if [ ! -x $path/eups ]; then echo $path; fi done | tr "\n" ":" | sed "s/:*$//" | sed "s/^:*//"'`
#
# Remove all other EUPS installations from PYTHONPATH.
# (Removes all entries from PYTHONPATH that contain a directory named 'eups'.)
setenv PYTHONPATH `bash -c 'for path in $(echo $PYTHONPATH | tr ":" "\n"); do if [ ! -x $path/eups ]; then echo $path; fi done | tr "\n" ":" | sed "s/:*$//" | sed "s/^:*//"'`
#
# EUPS setup/unsetup depend on a correctly set SHELL environment variable.
setenv SHELL `/usr/bin/env bash -c 'which csh'`
#
# Compiler to use
setenv CC /usr/bin/gcc
setenv CXX /usr/bin/g++
setenv GFORTRAN /usr/bin/gfortran
#
setenv EUPS_PKGROOT http://desbuild.cosmology.illinois.edu/eeups/webservice/repository
setenv SVNROOT https://dessvn.cosmology.illinois.edu/svn/desdm/devel
source /net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/1.2.30/bin/setups.csh
# No ICC setup.