#!/usr/bin/env bash
#
# Sets up the environment variables for this EUPS installation.
# Generated on 2017-08-29 12:52:55.373054
#
# Version of the installer that created this script.
export DESDM_EUPSINSTALL_VERSION="2015_Q2"
#
# This script first cleans up potential residual from other EUPS
# installations and then sources the setup file that comes with EUPS.
# It also sets some DESDM specific environment variables.
#
unset EUPS_DIR
unset EUPS_PATH
unset SETUP_EUPS
#
# Remove all other EUPS installations from PATH.
# (Removes all entries from PATH that contain an executable named 'eups'.)
export PATH=`for path in $(echo $PATH | tr ':' '\n'); do if [ ! -x $path/eups ]; then echo $path; fi done | tr '\n' ':' | sed 's/:*$//' | sed 's/^:*//'`
#
# Remove all other EUPS installations from PYTHONPATH.
# (Removes all entries from PYTHONPATH that contain a directory named 'eups'.)
export PYTHONPATH=`for path in $(echo $PYTHONPATH | tr ':' '\n'); do if [ ! -d $path/eups ]; then echo $path; fi done | tr '\n' ':' | sed 's/:*$//' | sed 's/^:*//'`
#
# EUPS setup/unsetup depend on a correctly set SHELL environment variable.
export SHELL=`/usr/bin/env bash -c 'which bash'`
#
# Compilers to use
export CC=/usr/bin/gcc
export CXX=/usr/bin/g++
export GFORTRAN=/usr/bin/gfortran
#
export EUPS_PKGROOT=http://desbuild.cosmology.illinois.edu/eeups/webservice/repository
export SVNROOT=https://dessvn.cosmology.illinois.edu/svn/desdm/devel
source /net/decdata1/deccp/CPDEV/EUPS_DESDM/eups/1.2.30/bin/setups.sh
# No ICC setup.