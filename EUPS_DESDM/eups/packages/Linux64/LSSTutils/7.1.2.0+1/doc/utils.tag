<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>unordered_map.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/tr1/</path>
    <filename>unordered__map_8h</filename>
    <namespace>std::tr1</namespace>
  </compound>
  <compound kind="file">
    <name>Demangle.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>_demangle_8h</filename>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>std::string</type>
      <name>demangleType</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ae1208efb0e6f3f7b26b3f426b3660915</anchor>
      <arglist>(std::string const _typeName)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ieee.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>ieee_8h</filename>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>int</type>
      <name>fpclassify</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ada43a47ce7ca055a4072fc12f606d726</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isfinite</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ab74f53d5e0e316c0f0f2be592c69527a</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isinf</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a647e1973ab64c1b91a11147e9caf867b</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isnan</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a1367cb73af62182638888fbcfae17bbf</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isnormal</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a92f92eb6b52dabcc938666b4ee4a9cc7</anchor>
      <arglist>(T t)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PowFast.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>_pow_fast_8h</filename>
    <class kind="class">lsst::utils::PowFast</class>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>const PowFast &amp;</type>
      <name>getPowFast</name>
      <anchorfile>group___pow_fast.html</anchorfile>
      <anchor>gac1074d3e592f32c1a54659ac6ecb3e0d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>RaDecStr.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>_ra_dec_str_8h</filename>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>std::string</type>
      <name>raRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga73a99622c20021ac3cee863fc98f4061</anchor>
      <arglist>(double raRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga446b89c51bbd8a082a390dd1c39a963f</anchor>
      <arglist>(double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gac860c577efa94346027ef679470888bf</anchor>
      <arglist>(double raDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga2170b4e98f737ebb07800e567242f4bd</anchor>
      <arglist>(double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gae83de81361b436d4dd8ce17e2294dd0c</anchor>
      <arglist>(double raRad, double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gad0ae4bb6cf353325ba2455fff2f91c76</anchor>
      <arglist>(double raDeg, double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gab02e8014d0a24b97e40f319995607108</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga3ecb6c013d1423dfaa9f1d9f715aae4c</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gaf23dd06e3649293be9ee9f56689df43b</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga6d9a60e4fc10a8111f0b05f491e37d0a</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Utils.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>_utils_8h</filename>
    <namespace>lsst::utils</namespace>
    <namespace>lsst::utils::eups</namespace>
    <member kind="function">
      <type>void</type>
      <name>guessSvnVersion</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a19c09fadd9f8a0b0b24266510f8d8eb2</anchor>
      <arglist>(std::string const &amp;headURL, std::string &amp;OUTPUT)</arglist>
    </member>
    <member kind="function">
      <type>boost::any</type>
      <name>stringToAny</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>aff07b6a24ee178ca7ac520cf5d99d938</anchor>
      <arglist>(std::string valueString)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>productDir</name>
      <anchorfile>namespacelsst_1_1utils_1_1eups.html</anchorfile>
      <anchor>a43f4dc304bbcaa63c5283ef0c18a146d</anchor>
      <arglist>(std::string const &amp;product, std::string const &amp;version=&quot;setup&quot;)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/</path>
    <filename>utils_2____init_____8py</filename>
    <namespace>lsst::utils</namespace>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/multithreading/</path>
    <filename>utils_2multithreading_2____init_____8py</filename>
    <namespace>lsst::utils::multithreading</namespace>
  </compound>
  <compound kind="file">
    <name>lockProtection.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/multithreading/</path>
    <filename>lock_protection_8py</filename>
    <class kind="class">lsst::utils::multithreading::lockProtection::LockProtected</class>
    <class kind="class">lsst::utils::multithreading::lockProtection::UnsafeAccessError</class>
    <namespace>lsst::utils::multithreading::lockProtection</namespace>
    <member kind="variable">
      <type></type>
      <name>SharedLock</name>
      <anchorfile>namespacelsst_1_1utils_1_1multithreading_1_1lock_protection.html</anchorfile>
      <anchor>ab136d0c9e2be28cdab3be3b7b643ddd8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>SharedData.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/multithreading/</path>
    <filename>_shared_data_8py</filename>
    <class kind="class">lsst::utils::multithreading::SharedData::SharedData</class>
    <namespace>lsst::utils::multithreading::SharedData</namespace>
  </compound>
  <compound kind="file">
    <name>tests.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/</path>
    <filename>tests_8py</filename>
    <class kind="class">lsst::utils::tests::MemoryTestCase</class>
    <class kind="class">lsst::utils::tests::temporaryFile</class>
    <class kind="class">lsst::utils::tests::TestCase</class>
    <namespace>lsst::utils::tests</namespace>
    <member kind="function">
      <type>def</type>
      <name>init</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a5bab07ad72171b4e4d32e319ae48c48a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aeccdd73ff613ee2c82d22ba92c336c71</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>findFileFromRoot</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a156496bc53b98d284ac2f566541fd281</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>inTestCase</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>abb181edbaa5a315634fc831c291d8742</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertRaisesLsstCpp</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a334612b7151d17e5751bdff7dc0f0a14</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>debugger</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a6f633ec6dca6b50162991d6a912b1513</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>plotImageDiff</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aa3c8faecbb24954296e0f10bece766b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertClose</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a295346a241f1f4b55769f2b472df1e96</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertNotClose</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aebfa9e2726be61b63b13587231f6bc67</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dafBase</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aa0f34ec9ca8afd5014e7581872e09fea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>memId0</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a24f03616994e75c41aa2e52367d30618</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nleakPrintMax</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a213fd34405c62b599a3c79acb877e821</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/</path>
    <filename>version_8py</filename>
    <namespace>lsst::utils::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>afbc9e0adc96deb972e19c2db1a2ccddf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>ac365e35445925e5fde03491198bf8cf2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a344978e85f4d13e5e29e9b3b04d7ccd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a0446bb3a4a2b6435d3c765f6918de6a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a1793d0fbd8196423420f8719ee6ecda1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a77bb2667510e9a3a3287653db0ef72bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>ae5068e0cac0e4653a10c6b1596a0c4de</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Demangle.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/src/</path>
    <filename>_demangle_8cc</filename>
    <includes id="_demangle_8h" name="Demangle.h" local="yes" imported="no">lsst/utils/Demangle.h</includes>
    <class kind="class">lsst::utils::Symbol</class>
    <class kind="struct">lsst::utils::n</class>
    <class kind="struct">lsst::utils::key</class>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>std::string</type>
      <name>demangleType</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ae1208efb0e6f3f7b26b3f426b3660915</anchor>
      <arglist>(std::string const _typeName)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PowFast.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/src/</path>
    <filename>_pow_fast_8cc</filename>
    <includes id="_pow_fast_8h" name="PowFast.h" local="yes" imported="no">lsst/utils/PowFast.h</includes>
    <namespace>lsst::utils</namespace>
  </compound>
  <compound kind="file">
    <name>RaDecStr.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/src/</path>
    <filename>_ra_dec_str_8cc</filename>
    <includes id="_ra_dec_str_8h" name="RaDecStr.h" local="yes" imported="no">lsst/utils/RaDecStr.h</includes>
    <member kind="function">
      <type>double</type>
      <name>radToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga7b4e37b9437ad1d963b3124818ca8b0b</anchor>
      <arglist>(long double angleInRadians)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>degToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga2783b29e3a8dfb30958dc44e17a8f57d</anchor>
      <arglist>(long double angleInDegrees)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Utils.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/src/</path>
    <filename>_utils_8cc</filename>
    <includes id="_utils_8h" name="Utils.h" local="yes" imported="no">lsst/utils/Utils.h</includes>
    <namespace>lsst::utils</namespace>
    <member kind="function">
      <type>void</type>
      <name>guessSvnVersion</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a19c09fadd9f8a0b0b24266510f8d8eb2</anchor>
      <arglist>(std::string const &amp;headURL, std::string &amp;OUTPUT)</arglist>
    </member>
    <member kind="function">
      <type>boost::any</type>
      <name>stringToAny</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>aff07b6a24ee178ca7ac520cf5d99d938</anchor>
      <arglist>(std::string valueString)</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>utils</name>
    <title>LSST general-purpose utilities</title>
    <filename>group__utils.html</filename>
  </compound>
  <compound kind="group">
    <name>PowFast</name>
    <title>lsst::utils::PowFast</title>
    <filename>group___pow_fast.html</filename>
    <class kind="class">lsst::utils::PowFast</class>
    <member kind="function">
      <type>const PowFast &amp;</type>
      <name>getPowFast</name>
      <anchorfile>group___pow_fast.html</anchorfile>
      <anchor>gac1074d3e592f32c1a54659ac6ecb3e0d</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="group">
    <name>RaDec</name>
    <title>Right Ascension and Declination Parsers</title>
    <filename>group___ra_dec.html</filename>
    <member kind="function">
      <type>double</type>
      <name>radToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga7b4e37b9437ad1d963b3124818ca8b0b</anchor>
      <arglist>(long double angleInRadians)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>degToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga2783b29e3a8dfb30958dc44e17a8f57d</anchor>
      <arglist>(long double angleInDegrees)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga73a99622c20021ac3cee863fc98f4061</anchor>
      <arglist>(double raRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gac860c577efa94346027ef679470888bf</anchor>
      <arglist>(double raDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga446b89c51bbd8a082a390dd1c39a963f</anchor>
      <arglist>(double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga2170b4e98f737ebb07800e567242f4bd</anchor>
      <arglist>(double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gae83de81361b436d4dd8ce17e2294dd0c</anchor>
      <arglist>(double raRad, double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gad0ae4bb6cf353325ba2455fff2f91c76</anchor>
      <arglist>(double raDeg, double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gab02e8014d0a24b97e40f319995607108</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga3ecb6c013d1423dfaa9f1d9f715aae4c</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gaf23dd06e3649293be9ee9f56689df43b</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga6d9a60e4fc10a8111f0b05f491e37d0a</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils</name>
    <filename>namespacelsst_1_1utils.html</filename>
    <namespace>lsst::utils::eups</namespace>
    <namespace>lsst::utils::multithreading</namespace>
    <namespace>lsst::utils::tests</namespace>
    <namespace>lsst::utils::version</namespace>
    <class kind="class">lsst::utils::PowFast</class>
    <class kind="class">lsst::utils::Symbol</class>
    <class kind="struct">lsst::utils::n</class>
    <class kind="struct">lsst::utils::key</class>
    <member kind="function">
      <type>std::string</type>
      <name>demangleType</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ae1208efb0e6f3f7b26b3f426b3660915</anchor>
      <arglist>(std::string const _typeName)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>fpclassify</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ada43a47ce7ca055a4072fc12f606d726</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isfinite</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>ab74f53d5e0e316c0f0f2be592c69527a</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isinf</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a647e1973ab64c1b91a11147e9caf867b</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isnan</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a1367cb73af62182638888fbcfae17bbf</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>isnormal</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a92f92eb6b52dabcc938666b4ee4a9cc7</anchor>
      <arglist>(T t)</arglist>
    </member>
    <member kind="function">
      <type>const PowFast &amp;</type>
      <name>getPowFast</name>
      <anchorfile>group___pow_fast.html</anchorfile>
      <anchor>gac1074d3e592f32c1a54659ac6ecb3e0d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga73a99622c20021ac3cee863fc98f4061</anchor>
      <arglist>(double raRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga446b89c51bbd8a082a390dd1c39a963f</anchor>
      <arglist>(double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gac860c577efa94346027ef679470888bf</anchor>
      <arglist>(double raDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>decDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga2170b4e98f737ebb07800e567242f4bd</anchor>
      <arglist>(double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecRadToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gae83de81361b436d4dd8ce17e2294dd0c</anchor>
      <arglist>(double raRad, double decRad)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>raDecDegToStr</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gad0ae4bb6cf353325ba2455fff2f91c76</anchor>
      <arglist>(double raDeg, double decDeg)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gab02e8014d0a24b97e40f319995607108</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>raStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga3ecb6c013d1423dfaa9f1d9f715aae4c</anchor>
      <arglist>(std::string raStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToRad</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>gaf23dd06e3649293be9ee9f56689df43b</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>decStrToDeg</name>
      <anchorfile>group___ra_dec.html</anchorfile>
      <anchor>ga6d9a60e4fc10a8111f0b05f491e37d0a</anchor>
      <arglist>(std::string decStr, std::string delimiter=&quot;:&quot;)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>guessSvnVersion</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>a19c09fadd9f8a0b0b24266510f8d8eb2</anchor>
      <arglist>(std::string const &amp;headURL, std::string &amp;OUTPUT)</arglist>
    </member>
    <member kind="function">
      <type>boost::any</type>
      <name>stringToAny</name>
      <anchorfile>namespacelsst_1_1utils.html</anchorfile>
      <anchor>aff07b6a24ee178ca7ac520cf5d99d938</anchor>
      <arglist>(std::string valueString)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::PowFast</name>
    <filename>classlsst_1_1utils_1_1_pow_fast.html</filename>
    <member kind="function">
      <type>float</type>
      <name>two</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>a1bbb7bb8aa0ae2f49ba1045e67472152</anchor>
      <arglist>(float x) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>exp</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>a218ecf67498b523cb9cd92891797a791</anchor>
      <arglist>(float x) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>ten</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>ae7efe1e235b6c08280bd64ddb260572e</anchor>
      <arglist>(float x) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>r</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>a32999f5b67d6f2f2b77255cc76b7ab16</anchor>
      <arglist>(float logr, float x) const </arglist>
    </member>
    <member kind="function">
      <type>unsigned int</type>
      <name>getPrecision</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>a9c13cbb2a0ce3b704ce97b6c485d6667</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="friend">
      <type>friend const PowFast &amp;</type>
      <name>getPowFast</name>
      <anchorfile>classlsst_1_1utils_1_1_pow_fast.html</anchorfile>
      <anchor>a34726f298c9504a43d9c5d59941488b5</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::Symbol</name>
    <filename>classlsst_1_1utils_1_1_symbol.html</filename>
    <member kind="function">
      <type></type>
      <name>Symbol</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>ae61a67a5730a763dd10b0f5be90198ea</anchor>
      <arglist>(std::string key)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Symbol</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>a6c3e88f59f4d262b45c1cdfd62609e29</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>print</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>ab1a94a3871d663d22985650f7cef9fde</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>af602af715ce1b2629f5e8ea079d45b78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>n</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>a7a28a493ed1a011b33865518a9023135</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>key</name>
      <anchorfile>classlsst_1_1utils_1_1_symbol.html</anchorfile>
      <anchor>a0a9b486ed0f9e3672dbd2d9f20f436ef</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lsst::utils::n</name>
    <filename>structlsst_1_1utils_1_1n.html</filename>
  </compound>
  <compound kind="struct">
    <name>lsst::utils::key</name>
    <filename>structlsst_1_1utils_1_1key.html</filename>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::eups</name>
    <filename>namespacelsst_1_1utils_1_1eups.html</filename>
    <member kind="function">
      <type>std::string</type>
      <name>productDir</name>
      <anchorfile>namespacelsst_1_1utils_1_1eups.html</anchorfile>
      <anchor>a43f4dc304bbcaa63c5283ef0c18a146d</anchor>
      <arglist>(std::string const &amp;product, std::string const &amp;version=&quot;setup&quot;)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::multithreading</name>
    <filename>namespacelsst_1_1utils_1_1multithreading.html</filename>
    <namespace>lsst::utils::multithreading::lockProtection</namespace>
    <namespace>lsst::utils::multithreading::SharedData</namespace>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::multithreading::lockProtection</name>
    <filename>namespacelsst_1_1utils_1_1multithreading_1_1lock_protection.html</filename>
    <class kind="class">lsst::utils::multithreading::lockProtection::LockProtected</class>
    <class kind="class">lsst::utils::multithreading::lockProtection::UnsafeAccessError</class>
    <member kind="variable">
      <type></type>
      <name>SharedLock</name>
      <anchorfile>namespacelsst_1_1utils_1_1multithreading_1_1lock_protection.html</anchorfile>
      <anchor>ab136d0c9e2be28cdab3be3b7b643ddd8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::multithreading::lockProtection::LockProtected</name>
    <filename>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_lock_protected.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_lock_protected.html</anchorfile>
      <anchor>a4655dc9993eedf1a00a1ba40969f660f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__enter__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_lock_protected.html</anchorfile>
      <anchor>a2d60002ddc6968ba5a3afdd6057cd211</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__exit__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_lock_protected.html</anchorfile>
      <anchor>a0cc77e86d1bbe163d45f20e738d8d4c1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::multithreading::lockProtection::UnsafeAccessError</name>
    <filename>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_unsafe_access_error.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1lock_protection_1_1_unsafe_access_error.html</anchorfile>
      <anchor>ab008943f1566a5d1c227bfad1b045796</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::multithreading::SharedData</name>
    <filename>namespacelsst_1_1utils_1_1multithreading_1_1_shared_data.html</filename>
    <class kind="class">lsst::utils::multithreading::SharedData::SharedData</class>
  </compound>
  <compound kind="class">
    <name>lsst::utils::multithreading::SharedData::SharedData</name>
    <filename>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a8307be2eddaaf851ecfc0d88db809711</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__enter__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a8fb5ddc56487eb693b24b9cf611df45a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__exit__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a22d865ccad68923f0be53584e13ddc63</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getattribute__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>ab55e7d9d6fafb47649e91bbb1ba7c634</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setattr__</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a4aa3d014d246c5ff8a5acad9558a87b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>initData</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a6619dcbb550b292c606f1a04b0b9fceb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dir</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>ac7dab97c8740149670ee6305d3e92c62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>acquire</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a286faa3d5a29547bb741e7f1bad3a3ed</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>release</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a7bc34587718d8f492d3e4a309d4b7adf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>notify</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a402de98a332711e1d12a233f0d93833c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>notifyAll</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>ac07a7cb061a5cdf7c7d385d90ad07243</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>wait</name>
      <anchorfile>classlsst_1_1utils_1_1multithreading_1_1_shared_data_1_1_shared_data.html</anchorfile>
      <anchor>a352bc348cbd2ae590b8f730539819435</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::tests</name>
    <filename>namespacelsst_1_1utils_1_1tests.html</filename>
    <class kind="class">lsst::utils::tests::MemoryTestCase</class>
    <class kind="class">lsst::utils::tests::temporaryFile</class>
    <class kind="class">lsst::utils::tests::TestCase</class>
    <member kind="function">
      <type>def</type>
      <name>init</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a5bab07ad72171b4e4d32e319ae48c48a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aeccdd73ff613ee2c82d22ba92c336c71</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>findFileFromRoot</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a156496bc53b98d284ac2f566541fd281</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>inTestCase</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>abb181edbaa5a315634fc831c291d8742</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertRaisesLsstCpp</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a334612b7151d17e5751bdff7dc0f0a14</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>debugger</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a6f633ec6dca6b50162991d6a912b1513</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>plotImageDiff</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aa3c8faecbb24954296e0f10bece766b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertClose</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a295346a241f1f4b55769f2b472df1e96</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assertNotClose</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aebfa9e2726be61b63b13587231f6bc67</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dafBase</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>aa0f34ec9ca8afd5014e7581872e09fea</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>memId0</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a24f03616994e75c41aa2e52367d30618</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>nleakPrintMax</name>
      <anchorfile>namespacelsst_1_1utils_1_1tests.html</anchorfile>
      <anchor>a213fd34405c62b599a3c79acb877e821</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::tests::MemoryTestCase</name>
    <filename>classlsst_1_1utils_1_1tests_1_1_memory_test_case.html</filename>
    <member kind="function">
      <type>def</type>
      <name>setUp</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1_memory_test_case.html</anchorfile>
      <anchor>ad8c9ac164f53d941fd387aa0deded24d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>testLeaks</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1_memory_test_case.html</anchorfile>
      <anchor>a8e7ac069fb77c4cc277827fd6607eb08</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::tests::temporaryFile</name>
    <filename>classlsst_1_1utils_1_1tests_1_1temporary_file.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1temporary_file.html</anchorfile>
      <anchor>ae21586dccd3c763d189ccdbbc3b12500</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__enter__</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1temporary_file.html</anchorfile>
      <anchor>aad72e911f4876683d0d6f8ce96a2242b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__exit__</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1temporary_file.html</anchorfile>
      <anchor>a6224792e0cfbb35cd03de0ea00beb02f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>filename</name>
      <anchorfile>classlsst_1_1utils_1_1tests_1_1temporary_file.html</anchorfile>
      <anchor>a01993cef45583fbbe9c33696b1289909</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::utils::tests::TestCase</name>
    <filename>classlsst_1_1utils_1_1tests_1_1_test_case.html</filename>
  </compound>
  <compound kind="namespace">
    <name>lsst::utils::version</name>
    <filename>namespacelsst_1_1utils_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>afbc9e0adc96deb972e19c2db1a2ccddf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>ac365e35445925e5fde03491198bf8cf2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a344978e85f4d13e5e29e9b3b04d7ccd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a0446bb3a4a2b6435d3c765f6918de6a1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a1793d0fbd8196423420f8719ee6ecda1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>a77bb2667510e9a3a3287653db0ef72bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1utils_1_1version.html</anchorfile>
      <anchor>ae5068e0cac0e4653a10c6b1596a0c4de</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std::tr1</name>
    <filename>namespacestd_1_1tr1.html</filename>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/utils</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/tr1</dir>
    <dir>include/lsst/utils</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/utils/multithreading</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/multithreading/</path>
    <filename>dir_e0f099b2b20b5d3bdb63bcf407ea5d6c.html</filename>
    <file>__init__.py</file>
    <file>lockProtection.py</file>
    <file>SharedData.py</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>Demangle.cc</file>
    <file>PowFast.cc</file>
    <file>RaDecStr.cc</file>
    <file>Utils.cc</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/tr1</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/tr1/</path>
    <filename>dir_bb429c825fcc57624078b1f6e0e23fa5.html</filename>
    <file>unordered_map.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/utils</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/python/lsst/utils/</path>
    <filename>dir_9127d96152e80f7bc1efa493bcb170d3.html</filename>
    <dir>python/lsst/utils/multithreading</dir>
    <file>__init__.py</file>
    <file>tests.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/utils</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/utils-7.1.2.0+1/utils-7.1.2.0/include/lsst/utils/</path>
    <filename>dir_5e70a7d5421414fa1132226c084780ef.html</filename>
    <file>Demangle.h</file>
    <file>ieee.h</file>
    <file>PowFast.h</file>
    <file>RaDecStr.h</file>
    <file>Utils.h</file>
  </compound>
</tagfile>
