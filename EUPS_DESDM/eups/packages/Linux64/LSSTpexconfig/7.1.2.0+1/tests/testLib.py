# This file was automatically generated by SWIG (http://www.swig.org).
# Version 2.0.4
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.


"""

Various swigged-up C++ classes for testing

"""


from sys import version_info
if version_info >= (2,6,0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_testLib', [dirname(__file__)])
        except ImportError:
            import _testLib
            return _testLib
        if fp is not None:
            try:
                _mod = imp.load_module('_testLib', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _testLib = swig_import_helper()
    del swig_import_helper
else:
    import _testLib
del version_info
try:
    _swig_property = property
except NameError:
    pass # Python < 2.2 doesn't have 'property'.
def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "thisown"): return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    if (name == "thisown"): return self.this.own()
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError(name)

def _swig_repr(self):
    try: strthis = "proxy of " + self.this.__repr__()
    except: strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0


class SwigPyIterator(_object):
    """Proxy of C++ swig::SwigPyIterator class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, SwigPyIterator, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SwigPyIterator, name)
    def __init__(self, *args, **kwargs): raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _testLib.delete_SwigPyIterator
    __del__ = lambda self : None;
    def value(self):
        """value(self) -> PyObject"""
        return _testLib.SwigPyIterator_value(self)

    def incr(self, n = 1):
        """
        incr(self, size_t n = 1) -> SwigPyIterator
        incr(self) -> SwigPyIterator
        """
        return _testLib.SwigPyIterator_incr(self, n)

    def decr(self, n = 1):
        """
        decr(self, size_t n = 1) -> SwigPyIterator
        decr(self) -> SwigPyIterator
        """
        return _testLib.SwigPyIterator_decr(self, n)

    def distance(self, *args):
        """distance(self, SwigPyIterator x) -> ptrdiff_t"""
        return _testLib.SwigPyIterator_distance(self, *args)

    def equal(self, *args):
        """equal(self, SwigPyIterator x) -> bool"""
        return _testLib.SwigPyIterator_equal(self, *args)

    def copy(self):
        """copy(self) -> SwigPyIterator"""
        return _testLib.SwigPyIterator_copy(self)

    def next(self):
        """next(self) -> PyObject"""
        return _testLib.SwigPyIterator_next(self)

    def __next__(self):
        """__next__(self) -> PyObject"""
        return _testLib.SwigPyIterator___next__(self)

    def previous(self):
        """previous(self) -> PyObject"""
        return _testLib.SwigPyIterator_previous(self)

    def advance(self, *args):
        """advance(self, ptrdiff_t n) -> SwigPyIterator"""
        return _testLib.SwigPyIterator_advance(self, *args)

    def __eq__(self, *args):
        """__eq__(self, SwigPyIterator x) -> bool"""
        return _testLib.SwigPyIterator___eq__(self, *args)

    def __ne__(self, *args):
        """__ne__(self, SwigPyIterator x) -> bool"""
        return _testLib.SwigPyIterator___ne__(self, *args)

    def __iadd__(self, *args):
        """__iadd__(self, ptrdiff_t n) -> SwigPyIterator"""
        return _testLib.SwigPyIterator___iadd__(self, *args)

    def __isub__(self, *args):
        """__isub__(self, ptrdiff_t n) -> SwigPyIterator"""
        return _testLib.SwigPyIterator___isub__(self, *args)

    def __add__(self, *args):
        """__add__(self, ptrdiff_t n) -> SwigPyIterator"""
        return _testLib.SwigPyIterator___add__(self, *args)

    def __sub__(self, *args):
        """
        __sub__(self, ptrdiff_t n) -> SwigPyIterator
        __sub__(self, SwigPyIterator x) -> ptrdiff_t
        """
        return _testLib.SwigPyIterator___sub__(self, *args)

    def __iter__(self): return self
SwigPyIterator_swigregister = _testLib.SwigPyIterator_swigregister
SwigPyIterator_swigregister(SwigPyIterator)

class ios_base(_object):
    """Proxy of C++ std::ios_base class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, ios_base, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ios_base, name)
    def __init__(self, *args, **kwargs): raise AttributeError("No constructor defined")
    __repr__ = _swig_repr
    erase_event = _testLib.ios_base_erase_event
    imbue_event = _testLib.ios_base_imbue_event
    copyfmt_event = _testLib.ios_base_copyfmt_event
    def register_callback(self, *args):
        """register_callback(self, event_callback __fn, int __index)"""
        return _testLib.ios_base_register_callback(self, *args)

    def flags(self, *args):
        """
        flags(self) -> fmtflags
        flags(self, fmtflags __fmtfl) -> fmtflags
        """
        return _testLib.ios_base_flags(self, *args)

    def setf(self, *args):
        """
        setf(self, fmtflags __fmtfl) -> fmtflags
        setf(self, fmtflags __fmtfl, fmtflags __mask) -> fmtflags
        """
        return _testLib.ios_base_setf(self, *args)

    def unsetf(self, *args):
        """unsetf(self, fmtflags __mask)"""
        return _testLib.ios_base_unsetf(self, *args)

    def precision(self, *args):
        """
        precision(self) -> streamsize
        precision(self, streamsize __prec) -> streamsize
        """
        return _testLib.ios_base_precision(self, *args)

    def width(self, *args):
        """
        width(self) -> streamsize
        width(self, streamsize __wide) -> streamsize
        """
        return _testLib.ios_base_width(self, *args)

    def sync_with_stdio(__sync = True):
        """
        sync_with_stdio(bool __sync = True) -> bool
        sync_with_stdio() -> bool
        """
        return _testLib.ios_base_sync_with_stdio(__sync)

    if _newclass:sync_with_stdio = staticmethod(sync_with_stdio)
    __swig_getmethods__["sync_with_stdio"] = lambda x: sync_with_stdio
    def imbue(self, *args):
        """imbue(self, locale __loc) -> locale"""
        return _testLib.ios_base_imbue(self, *args)

    def getloc(self):
        """getloc(self) -> locale"""
        return _testLib.ios_base_getloc(self)

    def xalloc():
        """xalloc() -> int"""
        return _testLib.ios_base_xalloc()

    if _newclass:xalloc = staticmethod(xalloc)
    __swig_getmethods__["xalloc"] = lambda x: xalloc
    def iword(self, *args):
        """iword(self, int __ix) -> long"""
        return _testLib.ios_base_iword(self, *args)

    def pword(self, *args):
        """pword(self, int __ix) -> void"""
        return _testLib.ios_base_pword(self, *args)

    __swig_destroy__ = _testLib.delete_ios_base
    __del__ = lambda self : None;
ios_base_swigregister = _testLib.ios_base_swigregister
ios_base_swigregister(ios_base)
cvar = _testLib.cvar
ios_base.boolalpha = _testLib.cvar.ios_base_boolalpha
ios_base.dec = _testLib.cvar.ios_base_dec
ios_base.fixed = _testLib.cvar.ios_base_fixed
ios_base.hex = _testLib.cvar.ios_base_hex
ios_base.internal = _testLib.cvar.ios_base_internal
ios_base.left = _testLib.cvar.ios_base_left
ios_base.oct = _testLib.cvar.ios_base_oct
ios_base.right = _testLib.cvar.ios_base_right
ios_base.scientific = _testLib.cvar.ios_base_scientific
ios_base.showbase = _testLib.cvar.ios_base_showbase
ios_base.showpoint = _testLib.cvar.ios_base_showpoint
ios_base.showpos = _testLib.cvar.ios_base_showpos
ios_base.skipws = _testLib.cvar.ios_base_skipws
ios_base.unitbuf = _testLib.cvar.ios_base_unitbuf
ios_base.uppercase = _testLib.cvar.ios_base_uppercase
ios_base.adjustfield = _testLib.cvar.ios_base_adjustfield
ios_base.basefield = _testLib.cvar.ios_base_basefield
ios_base.floatfield = _testLib.cvar.ios_base_floatfield
ios_base.badbit = _testLib.cvar.ios_base_badbit
ios_base.eofbit = _testLib.cvar.ios_base_eofbit
ios_base.failbit = _testLib.cvar.ios_base_failbit
ios_base.goodbit = _testLib.cvar.ios_base_goodbit
ios_base.app = _testLib.cvar.ios_base_app
ios_base.ate = _testLib.cvar.ios_base_ate
ios_base.binary = _testLib.cvar.ios_base_binary
ios_base.ios_base_in = _testLib.cvar.ios_base_ios_base_in
ios_base.out = _testLib.cvar.ios_base_out
ios_base.trunc = _testLib.cvar.ios_base_trunc
ios_base.beg = _testLib.cvar.ios_base_beg
ios_base.cur = _testLib.cvar.ios_base_cur
ios_base.end = _testLib.cvar.ios_base_end

def ios_base_sync_with_stdio(__sync = True):
  """
    sync_with_stdio(bool __sync = True) -> bool
    ios_base_sync_with_stdio() -> bool
    """
  return _testLib.ios_base_sync_with_stdio(__sync)

def ios_base_xalloc():
  """ios_base_xalloc() -> int"""
  return _testLib.ios_base_xalloc()

class ios(ios_base):
    """Proxy of C++ std::basic_ios<(char)> class"""
    __swig_setmethods__ = {}
    for _s in [ios_base]: __swig_setmethods__.update(getattr(_s,'__swig_setmethods__',{}))
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, ios, name, value)
    __swig_getmethods__ = {}
    for _s in [ios_base]: __swig_getmethods__.update(getattr(_s,'__swig_getmethods__',{}))
    __getattr__ = lambda self, name: _swig_getattr(self, ios, name)
    __repr__ = _swig_repr
    def rdstate(self):
        """rdstate(self) -> iostate"""
        return _testLib.ios_rdstate(self)

    def clear(self, *args):
        """
        clear(self, iostate __state = goodbit)
        clear(self)
        """
        return _testLib.ios_clear(self, *args)

    def setstate(self, *args):
        """setstate(self, iostate __state)"""
        return _testLib.ios_setstate(self, *args)

    def good(self):
        """good(self) -> bool"""
        return _testLib.ios_good(self)

    def eof(self):
        """eof(self) -> bool"""
        return _testLib.ios_eof(self)

    def fail(self):
        """fail(self) -> bool"""
        return _testLib.ios_fail(self)

    def bad(self):
        """bad(self) -> bool"""
        return _testLib.ios_bad(self)

    def exceptions(self, *args):
        """
        exceptions(self) -> iostate
        exceptions(self, iostate __except)
        """
        return _testLib.ios_exceptions(self, *args)

    def __init__(self, *args): 
        """__init__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> ios"""
        this = _testLib.new_ios(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_ios
    __del__ = lambda self : None;
    def tie(self, *args):
        """
        tie(self) -> ostream
        tie(self, ostream __tiestr) -> ostream
        """
        return _testLib.ios_tie(self, *args)

    def rdbuf(self, *args):
        """
        rdbuf(self) -> std::basic_streambuf<(char,std::char_traits<(char)>)>
        rdbuf(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> std::basic_streambuf<(char,std::char_traits<(char)>)>
        """
        return _testLib.ios_rdbuf(self, *args)

    def copyfmt(self, *args):
        """copyfmt(self, ios __rhs) -> ios"""
        return _testLib.ios_copyfmt(self, *args)

    def fill(self, *args):
        """
        fill(self) -> char_type
        fill(self, char_type __ch) -> char_type
        """
        return _testLib.ios_fill(self, *args)

    def imbue(self, *args):
        """imbue(self, locale __loc) -> locale"""
        return _testLib.ios_imbue(self, *args)

    def narrow(self, *args):
        """narrow(self, char_type __c, char __dfault) -> char"""
        return _testLib.ios_narrow(self, *args)

    def widen(self, *args):
        """widen(self, char __c) -> char_type"""
        return _testLib.ios_widen(self, *args)

ios_swigregister = _testLib.ios_swigregister
ios_swigregister(ios)

class ostream(ios):
    """Proxy of C++ std::basic_ostream<(char)> class"""
    __swig_setmethods__ = {}
    for _s in [ios]: __swig_setmethods__.update(getattr(_s,'__swig_setmethods__',{}))
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, ostream, name, value)
    __swig_getmethods__ = {}
    for _s in [ios]: __swig_getmethods__.update(getattr(_s,'__swig_getmethods__',{}))
    __getattr__ = lambda self, name: _swig_getattr(self, ostream, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> ostream"""
        this = _testLib.new_ostream(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_ostream
    __del__ = lambda self : None;
    def __lshift__(self, *args):
        """
        __lshift__(self, ostream __pf) -> ostream
        __lshift__(self, ios __pf) -> ostream
        __lshift__(self, ios_base __pf) -> ostream
        __lshift__(self, long __n) -> ostream
        __lshift__(self, unsigned long __n) -> ostream
        __lshift__(self, bool __n) -> ostream
        __lshift__(self, short __n) -> ostream
        __lshift__(self, unsigned short __n) -> ostream
        __lshift__(self, int __n) -> ostream
        __lshift__(self, unsigned int __n) -> ostream
        __lshift__(self, long long __n) -> ostream
        __lshift__(self, unsigned long long __n) -> ostream
        __lshift__(self, double __f) -> ostream
        __lshift__(self, float __f) -> ostream
        __lshift__(self, long double __f) -> ostream
        __lshift__(self, void __p) -> ostream
        __lshift__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> ostream
        __lshift__(self, std::basic_string<(char,std::char_traits<(char)>,std::allocator<(char)>)> s) -> ostream
        """
        return _testLib.ostream___lshift__(self, *args)

    def put(self, *args):
        """put(self, char_type __c) -> ostream"""
        return _testLib.ostream_put(self, *args)

    def write(self, *args):
        """write(self, char_type __s, streamsize __n) -> ostream"""
        return _testLib.ostream_write(self, *args)

    def flush(self):
        """flush(self) -> ostream"""
        return _testLib.ostream_flush(self)

    def tellp(self):
        """tellp(self) -> pos_type"""
        return _testLib.ostream_tellp(self)

    def seekp(self, *args):
        """
        seekp(self, pos_type arg0) -> ostream
        seekp(self, off_type arg0, seekdir arg1) -> ostream
        """
        return _testLib.ostream_seekp(self, *args)

ostream_swigregister = _testLib.ostream_swigregister
ostream_swigregister(ostream)
cin = cvar.cin
cout = cvar.cout
cerr = cvar.cerr
clog = cvar.clog

class istream(ios):
    """Proxy of C++ std::basic_istream<(char)> class"""
    __swig_setmethods__ = {}
    for _s in [ios]: __swig_setmethods__.update(getattr(_s,'__swig_setmethods__',{}))
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, istream, name, value)
    __swig_getmethods__ = {}
    for _s in [ios]: __swig_getmethods__.update(getattr(_s,'__swig_getmethods__',{}))
    __getattr__ = lambda self, name: _swig_getattr(self, istream, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> istream"""
        this = _testLib.new_istream(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_istream
    __del__ = lambda self : None;
    def __rshift__(self, *args):
        """
        __rshift__(self, istream __pf) -> istream
        __rshift__(self, ios __pf) -> istream
        __rshift__(self, ios_base __pf) -> istream
        __rshift__(self, bool __n) -> istream
        __rshift__(self, short __n) -> istream
        __rshift__(self, unsigned short __n) -> istream
        __rshift__(self, int __n) -> istream
        __rshift__(self, unsigned int __n) -> istream
        __rshift__(self, long __n) -> istream
        __rshift__(self, unsigned long __n) -> istream
        __rshift__(self, long long __n) -> istream
        __rshift__(self, unsigned long long __n) -> istream
        __rshift__(self, float __f) -> istream
        __rshift__(self, double __f) -> istream
        __rshift__(self, long double __f) -> istream
        __rshift__(self, void __p) -> istream
        __rshift__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> istream
        """
        return _testLib.istream___rshift__(self, *args)

    def gcount(self):
        """gcount(self) -> streamsize"""
        return _testLib.istream_gcount(self)

    def get(self, *args):
        """
        get(self) -> int_type
        get(self, char_type __c) -> istream
        get(self, char_type __s, streamsize __n, char_type __delim) -> istream
        get(self, char_type __s, streamsize __n) -> istream
        get(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb, 
            char_type __delim) -> istream
        get(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> istream
        """
        return _testLib.istream_get(self, *args)

    def getline(self, *args):
        """
        getline(self, char_type __s, streamsize __n, char_type __delim) -> istream
        getline(self, char_type __s, streamsize __n) -> istream
        """
        return _testLib.istream_getline(self, *args)

    def ignore(self, *args):
        """
        ignore(self, streamsize __n = 1, int_type __delim = std::char_traits< char >::eof()) -> istream
        ignore(self, streamsize __n = 1) -> istream
        ignore(self) -> istream
        """
        return _testLib.istream_ignore(self, *args)

    def peek(self):
        """peek(self) -> int_type"""
        return _testLib.istream_peek(self)

    def read(self, *args):
        """read(self, char_type __s, streamsize __n) -> istream"""
        return _testLib.istream_read(self, *args)

    def readsome(self, *args):
        """readsome(self, char_type __s, streamsize __n) -> streamsize"""
        return _testLib.istream_readsome(self, *args)

    def putback(self, *args):
        """putback(self, char_type __c) -> istream"""
        return _testLib.istream_putback(self, *args)

    def unget(self):
        """unget(self) -> istream"""
        return _testLib.istream_unget(self)

    def sync(self):
        """sync(self) -> int"""
        return _testLib.istream_sync(self)

    def tellg(self):
        """tellg(self) -> pos_type"""
        return _testLib.istream_tellg(self)

    def seekg(self, *args):
        """
        seekg(self, pos_type arg0) -> istream
        seekg(self, off_type arg0, seekdir arg1) -> istream
        """
        return _testLib.istream_seekg(self, *args)

istream_swigregister = _testLib.istream_swigregister
istream_swigregister(istream)

class iostream(istream,ostream):
    """Proxy of C++ std::basic_iostream<(char)> class"""
    __swig_setmethods__ = {}
    for _s in [istream,ostream]: __swig_setmethods__.update(getattr(_s,'__swig_setmethods__',{}))
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, iostream, name, value)
    __swig_getmethods__ = {}
    for _s in [istream,ostream]: __swig_getmethods__.update(getattr(_s,'__swig_getmethods__',{}))
    __getattr__ = lambda self, name: _swig_getattr(self, iostream, name)
    __repr__ = _swig_repr
    def __init__(self, *args): 
        """__init__(self, std::basic_streambuf<(char,std::char_traits<(char)>)> __sb) -> iostream"""
        this = _testLib.new_iostream(*args)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_iostream
    __del__ = lambda self : None;
iostream_swigregister = _testLib.iostream_swigregister
iostream_swigregister(iostream)

endl_cb_ptr = _testLib.endl_cb_ptr

def endl(*args):
  """endl(ostream arg0) -> ostream"""
  return _testLib.endl(*args)
endl = _testLib.endl
ends_cb_ptr = _testLib.ends_cb_ptr

def ends(*args):
  """ends(ostream arg0) -> ostream"""
  return _testLib.ends(*args)
ends = _testLib.ends
flush_cb_ptr = _testLib.flush_cb_ptr

def flush(*args):
  """flush(ostream arg0) -> ostream"""
  return _testLib.flush(*args)
flush = _testLib.flush
SHARED_PTR_DISOWN = _testLib.SHARED_PTR_DISOWN
LSST_BASE_BASE_H = _testLib.LSST_BASE_BASE_H
import lsst.pex.exceptions

class StringVec(_object):
    """Proxy of C++ std::vector<(std::string)> class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, StringVec, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, StringVec, name)
    __repr__ = _swig_repr
    def iterator(self):
        """iterator(self) -> SwigPyIterator"""
        return _testLib.StringVec_iterator(self)

    def __iter__(self): return self.iterator()
    def __nonzero__(self):
        """__nonzero__(self) -> bool"""
        return _testLib.StringVec___nonzero__(self)

    def __bool__(self):
        """__bool__(self) -> bool"""
        return _testLib.StringVec___bool__(self)

    def __len__(self):
        """__len__(self) -> size_type"""
        return _testLib.StringVec___len__(self)

    def pop(self):
        """pop(self) -> value_type"""
        return _testLib.StringVec_pop(self)

    def __getslice__(self, *args):
        """__getslice__(self, difference_type i, difference_type j) -> StringVec"""
        return _testLib.StringVec___getslice__(self, *args)

    def __setslice__(self, *args):
        """
        __setslice__(self, difference_type i, difference_type j, StringVec v = std::vector< std::string,std::allocator< std::string > >())
        __setslice__(self, difference_type i, difference_type j)
        """
        return _testLib.StringVec___setslice__(self, *args)

    def __delslice__(self, *args):
        """__delslice__(self, difference_type i, difference_type j)"""
        return _testLib.StringVec___delslice__(self, *args)

    def __delitem__(self, *args):
        """
        __delitem__(self, difference_type i)
        __delitem__(self, PySliceObject slice)
        """
        return _testLib.StringVec___delitem__(self, *args)

    def __getitem__(self, *args):
        """
        __getitem__(self, PySliceObject slice) -> StringVec
        __getitem__(self, difference_type i) -> value_type
        """
        return _testLib.StringVec___getitem__(self, *args)

    def __setitem__(self, *args):
        """
        __setitem__(self, PySliceObject slice, StringVec v)
        __setitem__(self, PySliceObject slice)
        __setitem__(self, difference_type i, value_type x)
        """
        return _testLib.StringVec___setitem__(self, *args)

    def append(self, *args):
        """append(self, value_type x)"""
        return _testLib.StringVec_append(self, *args)

    def empty(self):
        """empty(self) -> bool"""
        return _testLib.StringVec_empty(self)

    def size(self):
        """size(self) -> size_type"""
        return _testLib.StringVec_size(self)

    def clear(self):
        """clear(self)"""
        return _testLib.StringVec_clear(self)

    def swap(self, *args):
        """swap(self, StringVec v)"""
        return _testLib.StringVec_swap(self, *args)

    def get_allocator(self):
        """get_allocator(self) -> allocator_type"""
        return _testLib.StringVec_get_allocator(self)

    def begin(self):
        """begin(self) -> iterator"""
        return _testLib.StringVec_begin(self)

    def end(self):
        """end(self) -> iterator"""
        return _testLib.StringVec_end(self)

    def rbegin(self):
        """rbegin(self) -> reverse_iterator"""
        return _testLib.StringVec_rbegin(self)

    def rend(self):
        """rend(self) -> reverse_iterator"""
        return _testLib.StringVec_rend(self)

    def pop_back(self):
        """pop_back(self)"""
        return _testLib.StringVec_pop_back(self)

    def erase(self, *args):
        """
        erase(self, iterator pos) -> iterator
        erase(self, iterator first, iterator last) -> iterator
        """
        return _testLib.StringVec_erase(self, *args)

    def __init__(self, *args): 
        """
        __init__(self) -> StringVec
        __init__(self, StringVec arg0) -> StringVec
        __init__(self, size_type size) -> StringVec
        __init__(self, size_type size, value_type value) -> StringVec
        """
        this = _testLib.new_StringVec(*args)
        try: self.this.append(this)
        except: self.this = this
    def push_back(self, *args):
        """push_back(self, value_type x)"""
        return _testLib.StringVec_push_back(self, *args)

    def front(self):
        """front(self) -> value_type"""
        return _testLib.StringVec_front(self)

    def back(self):
        """back(self) -> value_type"""
        return _testLib.StringVec_back(self)

    def assign(self, *args):
        """assign(self, size_type n, value_type x)"""
        return _testLib.StringVec_assign(self, *args)

    def resize(self, *args):
        """
        resize(self, size_type new_size)
        resize(self, size_type new_size, value_type x)
        """
        return _testLib.StringVec_resize(self, *args)

    def insert(self, *args):
        """
        insert(self, iterator pos, value_type x) -> iterator
        insert(self, iterator pos, size_type n, value_type x)
        """
        return _testLib.StringVec_insert(self, *args)

    def reserve(self, *args):
        """reserve(self, size_type n)"""
        return _testLib.StringVec_reserve(self, *args)

    def capacity(self):
        """capacity(self) -> size_type"""
        return _testLib.StringVec_capacity(self)

    __swig_destroy__ = _testLib.delete_StringVec
    __del__ = lambda self : None;
StringVec_swigregister = _testLib.StringVec_swigregister
StringVec_swigregister(StringVec)

class InnerControlObject(_object):
    """Proxy of C++ InnerControlObject class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, InnerControlObject, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, InnerControlObject, name)
    __repr__ = _swig_repr
    def _doc_p():
        """_doc_p() -> char"""
        return _testLib.InnerControlObject__doc_p()

    if _newclass:_doc_p = staticmethod(_doc_p)
    __swig_getmethods__["_doc_p"] = lambda x: _doc_p
    def _type_p():
        """_type_p() -> char"""
        return _testLib.InnerControlObject__type_p()

    if _newclass:_type_p = staticmethod(_type_p)
    __swig_getmethods__["_type_p"] = lambda x: _type_p
    __swig_setmethods__["p"] = _testLib.InnerControlObject_p_set
    __swig_getmethods__["p"] = _testLib.InnerControlObject_p_get
    if _newclass:p = _swig_property(_testLib.InnerControlObject_p_get, _testLib.InnerControlObject_p_set)
    def __init__(self, p_ = 2.0): 
        """
        __init__(self, double p_ = 2.0) -> InnerControlObject
        __init__(self) -> InnerControlObject
        """
        this = _testLib.new_InnerControlObject(p_)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_InnerControlObject
    __del__ = lambda self : None;
InnerControlObject_swigregister = _testLib.InnerControlObject_swigregister
InnerControlObject_swigregister(InnerControlObject)

def InnerControlObject__doc_p():
  """InnerControlObject__doc_p() -> char"""
  return _testLib.InnerControlObject__doc_p()

def InnerControlObject__type_p():
  """InnerControlObject__type_p() -> char"""
  return _testLib.InnerControlObject__type_p()

class OuterControlObject(_object):
    """Proxy of C++ OuterControlObject class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, OuterControlObject, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, OuterControlObject, name)
    __repr__ = _swig_repr
    def _doc_a():
        """_doc_a() -> char"""
        return _testLib.OuterControlObject__doc_a()

    if _newclass:_doc_a = staticmethod(_doc_a)
    __swig_getmethods__["_doc_a"] = lambda x: _doc_a
    def _type_a():
        """_type_a() -> char"""
        return _testLib.OuterControlObject__type_a()

    if _newclass:_type_a = staticmethod(_type_a)
    __swig_getmethods__["_type_a"] = lambda x: _type_a
    def _module_a():
        """_module_a() -> char"""
        return _testLib.OuterControlObject__module_a()

    if _newclass:_module_a = staticmethod(_module_a)
    __swig_getmethods__["_module_a"] = lambda x: _module_a
    __swig_setmethods__["a"] = _testLib.OuterControlObject_a_set
    __swig_getmethods__["a"] = _testLib.OuterControlObject_a_get
    if _newclass:a = _swig_property(_testLib.OuterControlObject_a_get, _testLib.OuterControlObject_a_set)
    def _doc_b():
        """_doc_b() -> char"""
        return _testLib.OuterControlObject__doc_b()

    if _newclass:_doc_b = staticmethod(_doc_b)
    __swig_getmethods__["_doc_b"] = lambda x: _doc_b
    def _type_b():
        """_type_b() -> char"""
        return _testLib.OuterControlObject__type_b()

    if _newclass:_type_b = staticmethod(_type_b)
    __swig_getmethods__["_type_b"] = lambda x: _type_b
    __swig_setmethods__["b"] = _testLib.OuterControlObject_b_set
    __swig_getmethods__["b"] = _testLib.OuterControlObject_b_get
    if _newclass:b = _swig_property(_testLib.OuterControlObject_b_get, _testLib.OuterControlObject_b_set)
    def __init__(self, b_ = 0): 
        """
        __init__(self, int b_ = 0) -> OuterControlObject
        __init__(self) -> OuterControlObject
        """
        this = _testLib.new_OuterControlObject(b_)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_OuterControlObject
    __del__ = lambda self : None;
OuterControlObject_swigregister = _testLib.OuterControlObject_swigregister
OuterControlObject_swigregister(OuterControlObject)

def OuterControlObject__doc_a():
  """OuterControlObject__doc_a() -> char"""
  return _testLib.OuterControlObject__doc_a()

def OuterControlObject__type_a():
  """OuterControlObject__type_a() -> char"""
  return _testLib.OuterControlObject__type_a()

def OuterControlObject__module_a():
  """OuterControlObject__module_a() -> char"""
  return _testLib.OuterControlObject__module_a()

def OuterControlObject__doc_b():
  """OuterControlObject__doc_b() -> char"""
  return _testLib.OuterControlObject__doc_b()

def OuterControlObject__type_b():
  """OuterControlObject__type_b() -> char"""
  return _testLib.OuterControlObject__type_b()

class ControlObject(_object):
    """Proxy of C++ ControlObject class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr_nondynamic(self, ControlObject, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ControlObject, name)
    __repr__ = _swig_repr
    def _doc_foo():
        """_doc_foo() -> char"""
        return _testLib.ControlObject__doc_foo()

    if _newclass:_doc_foo = staticmethod(_doc_foo)
    __swig_getmethods__["_doc_foo"] = lambda x: _doc_foo
    def _type_foo():
        """_type_foo() -> char"""
        return _testLib.ControlObject__type_foo()

    if _newclass:_type_foo = staticmethod(_type_foo)
    __swig_getmethods__["_type_foo"] = lambda x: _type_foo
    __swig_setmethods__["foo"] = _testLib.ControlObject_foo_set
    __swig_getmethods__["foo"] = _testLib.ControlObject_foo_get
    if _newclass:foo = _swig_property(_testLib.ControlObject_foo_get, _testLib.ControlObject_foo_set)
    def _doc_bar():
        """_doc_bar() -> char"""
        return _testLib.ControlObject__doc_bar()

    if _newclass:_doc_bar = staticmethod(_doc_bar)
    __swig_getmethods__["_doc_bar"] = lambda x: _doc_bar
    def _type_bar():
        """_type_bar() -> char"""
        return _testLib.ControlObject__type_bar()

    if _newclass:_type_bar = staticmethod(_type_bar)
    __swig_getmethods__["_type_bar"] = lambda x: _type_bar
    __swig_setmethods__["bar"] = _testLib.ControlObject_bar_set
    __swig_getmethods__["bar"] = _testLib.ControlObject_bar_get
    if _newclass:bar = _swig_property(_testLib.ControlObject_bar_get, _testLib.ControlObject_bar_set)
    def __init__(self, foo_ = 1): 
        """
        __init__(self, int foo_ = 1) -> ControlObject
        __init__(self) -> ControlObject
        """
        this = _testLib.new_ControlObject(foo_)
        try: self.this.append(this)
        except: self.this = this
    __swig_destroy__ = _testLib.delete_ControlObject
    __del__ = lambda self : None;
ControlObject_swigregister = _testLib.ControlObject_swigregister
ControlObject_swigregister(ControlObject)

def ControlObject__doc_foo():
  """ControlObject__doc_foo() -> char"""
  return _testLib.ControlObject__doc_foo()

def ControlObject__type_foo():
  """ControlObject__type_foo() -> char"""
  return _testLib.ControlObject__type_foo()

def ControlObject__doc_bar():
  """ControlObject__doc_bar() -> char"""
  return _testLib.ControlObject__doc_bar()

def ControlObject__type_bar():
  """ControlObject__type_bar() -> char"""
  return _testLib.ControlObject__type_bar()


def checkControl(*args):
  """checkControl(ControlObject ctrl, int fooVal, StringVec barVal) -> bool"""
  return _testLib.checkControl(*args)

def checkNestedControl(*args):
  """checkNestedControl(OuterControlObject ctrl, double apVal, int bVal) -> bool"""
  return _testLib.checkNestedControl(*args)
import lsst.pex.config
InnerConfigObject = lsst.pex.config.makeConfigClass(InnerControlObject)
OuterConfigObject = lsst.pex.config.makeConfigClass(OuterControlObject)
ConfigObject = lsst.pex.config.makeConfigClass(ControlObject)


# This file is compatible with both classic and new-style classes.


