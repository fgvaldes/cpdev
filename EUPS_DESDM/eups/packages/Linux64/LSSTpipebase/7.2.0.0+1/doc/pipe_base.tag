<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>main.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/doc/</path>
    <filename>main_8dox</filename>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>a2e0352fbe093110645adc117c6b2fd6d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/</path>
    <filename>pipe_2____init_____8py</filename>
    <namespace>lsst::pipe</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pipe.html</anchorfile>
      <anchor>a4300e34d564f30fa4d23368723ee1c59</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>pipe_2base_2____init_____8py</filename>
    <namespace>lsst::pipe::base</namespace>
  </compound>
  <compound kind="file">
    <name>argumentParser.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>argument_parser_8py</filename>
    <class kind="class">lsst::pipe::base::argumentParser::DataIdContainer</class>
    <class kind="class">lsst::pipe::base::argumentParser::DataIdArgument</class>
    <class kind="class">lsst::pipe::base::argumentParser::DatasetArgument</class>
    <class kind="class">lsst::pipe::base::argumentParser::ArgumentParser</class>
    <class kind="class">lsst::pipe::base::argumentParser::ConfigValueAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::ConfigFileAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::IdValueAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::TraceLevelAction</class>
    <namespace>lsst::pipe::base::argumentParser</namespace>
    <member kind="function">
      <type>def</type>
      <name>setDottedAttr</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ade5693101c0c3fa23d71e526f35d9281</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDottedAttr</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ab547965606f5d9e229a24849debdbabc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dataExists</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a8e9952625a46a07524ee14ca4a0dea46</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a1f41901989d35eaa9819d9a3481c999b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_INPUT_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ae9601b3dd1c0a74e4d75271f68f4011b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_CALIB_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a6b600b2e3d03afbcc914f35288d7cde6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_OUTPUT_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>afff6ce829a7c7cdedbe43383ef57ad69</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>cmdLineTask.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>cmd_line_task_8py</filename>
    <class kind="class">lsst::pipe::base::cmdLineTask::TaskRunner</class>
    <class kind="class">lsst::pipe::base::cmdLineTask::CmdLineTask</class>
    <namespace>lsst::pipe::base::cmdLineTask</namespace>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1cmd_line_task.html</anchorfile>
      <anchor>a70ce4bc33743a2750005aecc7ee6a0be</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>struct.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>struct_8py</filename>
    <class kind="class">lsst::pipe::base::struct::Struct</class>
    <namespace>lsst::pipe::base::struct</namespace>
  </compound>
  <compound kind="file">
    <name>task.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>task_8py</filename>
    <class kind="class">lsst::pipe::base::task::Ds9Warning</class>
    <class kind="class">lsst::pipe::base::task::TaskError</class>
    <class kind="class">lsst::pipe::base::task::Task</class>
    <namespace>lsst::pipe::base::task</namespace>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1task.html</anchorfile>
      <anchor>ab7244c2d4ee97ea3cf469bce7868c3d8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>timer.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>timer_8py</filename>
    <namespace>lsst::pipe::base::timer</namespace>
    <member kind="function">
      <type>def</type>
      <name>logPairs</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>a2c144572c6ac328d009c95d3847065f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>logInfo</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>a9f105dc9610d5d850a8e01fb49a4a5df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>timeMethod</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>afd07c25ed9b2009250bf937a3563adc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>adf3359ae0dc807492e67489e1c0c2f16</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>version_8py</filename>
    <namespace>lsst::pipe::base::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>ab1806488504a91f6282a31f240d41abd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a4994e1341a48ab8a50e45f3ba47d261f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>af214cbe10c2bf1f35d0f2ee9df9b0a62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>acefedd6aa340facff3e318e91b2a3a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>ad34091b58e1d29b203c08a812a22a3fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a3ec4f30c162faf287c63a8a6a65e2475</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a948373cdd84d6f611b720ad6f581f87c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe</name>
    <filename>namespacelsst_1_1pipe.html</filename>
    <namespace>lsst::pipe::base</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pipe.html</anchorfile>
      <anchor>a4300e34d564f30fa4d23368723ee1c59</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base</name>
    <filename>namespacelsst_1_1pipe_1_1base.html</filename>
    <namespace>lsst::pipe::base::argumentParser</namespace>
    <namespace>lsst::pipe::base::cmdLineTask</namespace>
    <namespace>lsst::pipe::base::struct</namespace>
    <namespace>lsst::pipe::base::task</namespace>
    <namespace>lsst::pipe::base::timer</namespace>
    <namespace>lsst::pipe::base::version</namespace>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::argumentParser</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</filename>
    <class kind="class">lsst::pipe::base::argumentParser::DataIdContainer</class>
    <class kind="class">lsst::pipe::base::argumentParser::DataIdArgument</class>
    <class kind="class">lsst::pipe::base::argumentParser::DatasetArgument</class>
    <class kind="class">lsst::pipe::base::argumentParser::ArgumentParser</class>
    <class kind="class">lsst::pipe::base::argumentParser::ConfigValueAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::ConfigFileAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::IdValueAction</class>
    <class kind="class">lsst::pipe::base::argumentParser::TraceLevelAction</class>
    <member kind="function">
      <type>def</type>
      <name>setDottedAttr</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ade5693101c0c3fa23d71e526f35d9281</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDottedAttr</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ab547965606f5d9e229a24849debdbabc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dataExists</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a8e9952625a46a07524ee14ca4a0dea46</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a1f41901989d35eaa9819d9a3481c999b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_INPUT_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>ae9601b3dd1c0a74e4d75271f68f4011b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_CALIB_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>a6b600b2e3d03afbcc914f35288d7cde6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>DEFAULT_OUTPUT_NAME</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1argument_parser.html</anchorfile>
      <anchor>afff6ce829a7c7cdedbe43383ef57ad69</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::DataIdContainer</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a009c323b4efdfe0d64611f63eda4037b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setDatasetType</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>afc63661a82fa5d71c43a6f5bea9dd907</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>castDataIds</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a6d98c5b24b12298e5f90c88c7ddabd40</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeDataRefList</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a4824b3b87d7b2de8939b2390c54b83d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>datasetType</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a56ca0403481b5dbb9ef0bf8f6fedf0bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>level</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a32cb2187a01e615c2d957ebc3712b93f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>idList</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>ad51e21b7156fcf3cfa99a2a2942c7db1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>refList</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_container.html</anchorfile>
      <anchor>a3bef42c318bba2b78d1044a2e41e1883</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::DataIdArgument</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a40acd5e3ae05227a3e562dead34ac7f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>isDynamicDatasetType</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a1e9c7dbe6f0098e6810867348015be05</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDatasetType</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a95d7cbbffd7c17001f8f6b201f3bd2bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>name</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a4e98a58e2d664dae0baccb753d255482</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>datasetType</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a229ecad8873a43f1aa9f6e38ce1e8761</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>level</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>ae1ff63d12e0ab4196595b9a60aecac89</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>doMakeDataRefList</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a38ccc39689bc746fc189dc9a74f8d39a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>ContainerClass</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a3f9565027c04f5d7758ecb98a7d34dd2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>argName</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>a5cdf4fb7fdfff42e9432baceb64604d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>datasetTypeName</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_data_id_argument.html</anchorfile>
      <anchor>ae419e671568404b2730e8154bcf4a649</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::DatasetArgument</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</anchorfile>
      <anchor>aed7bae63b1f58baeb1f20a86c8e86aca</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>required</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</anchorfile>
      <anchor>aceb9b984eafd00e2414b9d2228577246</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>name</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</anchorfile>
      <anchor>a46aa897d6a83f14589473e93c6c49fe9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>help</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</anchorfile>
      <anchor>a59cb31ee8aaa4d392f3b4499f9965f12</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>default</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_dataset_argument.html</anchorfile>
      <anchor>a4bd514faea755ad942beee6dd1e02b71</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::ArgumentParser</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</anchorfile>
      <anchor>a45858b5665c6cb00dd2b69f9ef3ea9d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>add_id_argument</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</anchorfile>
      <anchor>a9b82f823aacbaac2eb121108ef59cea8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parse_args</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</anchorfile>
      <anchor>ab12a0a068503b5e8ae7205d0ce6baf53</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>handleCamera</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</anchorfile>
      <anchor>a32e058610b6f16e4839af38471def7fe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>convert_arg_line_to_args</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_argument_parser.html</anchorfile>
      <anchor>a86c9906ec5b769fd3dbdf3dba9973b93</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::ConfigValueAction</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_config_value_action.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_config_value_action.html</anchorfile>
      <anchor>aea2c6711dfde8528ed6e053774fa558d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::ConfigFileAction</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_config_file_action.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_config_file_action.html</anchorfile>
      <anchor>aefe91dd142be6a5e060d237d39948b8d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::IdValueAction</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_id_value_action.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_id_value_action.html</anchorfile>
      <anchor>aa8069d437dfc2db1b4ba07bf15145fb5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::argumentParser::TraceLevelAction</name>
    <filename>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_trace_level_action.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1argument_parser_1_1_trace_level_action.html</anchorfile>
      <anchor>a2b42628634ca8fba8c3debc83d4e7fdf</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::cmdLineTask</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1cmd_line_task.html</filename>
    <class kind="class">lsst::pipe::base::cmdLineTask::TaskRunner</class>
    <class kind="class">lsst::pipe::base::cmdLineTask::CmdLineTask</class>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1cmd_line_task.html</anchorfile>
      <anchor>a70ce4bc33743a2750005aecc7ee6a0be</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::cmdLineTask::TaskRunner</name>
    <filename>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a10a8869f3e966d855e7ff757684403bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>prepareForMultiProcessing</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a264520c525b0f5e9813f3e2c5c42e25f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a7f1f1f9fb847d35c0d62d09b60f41252</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>precall</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>acf25291d2d84c0e0e2d19b40fdd9d429</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>aae201803046b26ce22ff23f086aaab0f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>def</type>
      <name>getTargetList</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a345448f08445b7c2e11a270e24a90a95</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>TaskClass</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a54e6f461640a3c48f90be2ad2f001941</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>doReturnResults</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a934d4cfda0cabd60c40b5e613abeca67</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>config</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a40ce213325d99dcf832611442b08245c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>log</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a2d53b4de17f278b9f9100ddb51faab75</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>doRaise</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a87973528d34c57126dea488a6f64e12f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>clobberConfig</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>abbf5503ef117ced595fa3139a5a93385</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>numProcesses</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_task_runner.html</anchorfile>
      <anchor>a45a7537b1cbae663b9098d5664b171d2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::cmdLineTask::CmdLineTask</name>
    <filename>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</filename>
    <member kind="function">
      <type>def</type>
      <name>applyOverrides</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>aa6ba32c3d45346979bab78d5f8694130</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>parseAndRun</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>a5820e5ed05ca70b8e58efa8ee0567cdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>writeConfig</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>a2a7ce0ea7e5e5b5da078ff1908de6d81</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>writeSchemas</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>ae0d9227944ebb46ac2765c001912a005</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>writeMetadata</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>a9d2b57425f89d1e9f7547116e435659e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>RunnerClass</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>a2b42cd2d3374d35f1304c02a74164053</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>canMultiprocess</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1cmd_line_task_1_1_cmd_line_task.html</anchorfile>
      <anchor>a61fbb94eaf71878c5f56dbe1694150f1</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::struct</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1struct.html</filename>
    <class kind="class">lsst::pipe::base::struct::Struct</class>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::struct::Struct</name>
    <filename>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>acda27a5aeaa808e9203c2f41d2608516</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDict</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>a71b66baf286d0aa3ab2f81e8415a0af0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mergeItems</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>a4a72cf1f1dcfe1074ab870238aaff6f1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>copy</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>ae6600718b024021612a2f5b0ce583189</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__eq__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>ad18c9e37c435e0622b407f729113f0fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>adae18ef8b41b1b34b0795eac082ac5ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__repr__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1struct_1_1_struct.html</anchorfile>
      <anchor>a14a8ae6cad02744f9ec7cc55f4f610f7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::task</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1task.html</filename>
    <class kind="class">lsst::pipe::base::task::Ds9Warning</class>
    <class kind="class">lsst::pipe::base::task::TaskError</class>
    <class kind="class">lsst::pipe::base::task::Task</class>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1task.html</anchorfile>
      <anchor>ab7244c2d4ee97ea3cf469bce7868c3d8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::task::Ds9Warning</name>
    <filename>classlsst_1_1pipe_1_1base_1_1task_1_1_ds9_warning.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_ds9_warning.html</anchorfile>
      <anchor>a206ff8674cabbc4f9f689143ca356e51</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getattr__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_ds9_warning.html</anchorfile>
      <anchor>a00880877db30042bee7487517c4ff89e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setattr__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_ds9_warning.html</anchorfile>
      <anchor>a128c33dbcb1b914952d3ee3d27967b4d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__call__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_ds9_warning.html</anchorfile>
      <anchor>aa0f9956b979689084ae8c1617073a02f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::task::TaskError</name>
    <filename>classlsst_1_1pipe_1_1base_1_1task_1_1_task_error.html</filename>
  </compound>
  <compound kind="class">
    <name>lsst::pipe::base::task::Task</name>
    <filename>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a1773a024121ed2ce7294509b3e8b40e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>emptyMetadata</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a3a28a93bd9378abe50b84cba5427fe56</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getSchemaCatalogs</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a26695d9d036078e2dcc9c63cefb46b0d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getAllSchemaCatalogs</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a1840e60d74282278cda0d00c570bb4f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getFullMetadata</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a0f2fe08ae65af1ea5a7af67eb93abc15</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getFullName</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>aa0773d7ad1045803a148b72f0a101a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a727d2a3323310186aecb8370690413d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getTaskDict</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a985527142ee733f6354aac05d72ba7d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeSubtask</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a8014c5fc7649a4c912de7e6ac3be87e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>timer</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a695143e4adb0c85405bb4d7c7bc728e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>display</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a6d730f610ef6298d3abf5ed7a4a9b28d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>metadata</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>acf267b47c2b3a24628dec02af97df1e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>config</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a2ed729ee214580cad6e60e44c5602823</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>log</name>
      <anchorfile>classlsst_1_1pipe_1_1base_1_1task_1_1_task.html</anchorfile>
      <anchor>a098b536ed48888b6f6186021a4ac11c2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::timer</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1timer.html</filename>
    <member kind="function">
      <type>def</type>
      <name>logPairs</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>a2c144572c6ac328d009c95d3847065f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>logInfo</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>a9f105dc9610d5d850a8e01fb49a4a5df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>timeMethod</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>afd07c25ed9b2009250bf937a3563adc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1timer.html</anchorfile>
      <anchor>adf3359ae0dc807492e67489e1c0c2f16</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pipe::base::version</name>
    <filename>namespacelsst_1_1pipe_1_1base_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>ab1806488504a91f6282a31f240d41abd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a4994e1341a48ab8a50e45f3ba47d261f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>af214cbe10c2bf1f35d0f2ee9df9b0a62</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>acefedd6aa340facff3e318e91b2a3a11</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>ad34091b58e1d29b203c08a812a22a3fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a3ec4f30c162faf287c63a8a6a65e2475</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pipe_1_1base_1_1version.html</anchorfile>
      <anchor>a948373cdd84d6f611b720ad6f581f87c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pipe/base</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/base/</path>
    <filename>dir_67d9608fd67841e913d9d082b8b40c53.html</filename>
    <file>__init__.py</file>
    <file>argumentParser.py</file>
    <file>cmdLineTask.py</file>
    <file>struct.py</file>
    <file>task.py</file>
    <file>timer.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/</path>
    <filename>dir_72ee15413cedc0c3f0b5e86694afe00d.html</filename>
    <dir>python/lsst/pipe</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pipe</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/lsst/pipe/</path>
    <filename>dir_1a66c8d55c2cfcc01f618a81e4907a08.html</filename>
    <dir>python/lsst/pipe/base</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pipe_base-7.2.0.0+1/pipe_base-7.2.0.0/python/</path>
    <filename>dir_83ad09e52555a5eff12f7b316891801b.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>lsst::pipe::base; Base package for pipeline tasks</title>
    <filename>index</filename>
    <docanchor file="index">pipeBase_introduction</docanchor>
    <docanchor file="index">pipeBase_argumentParser</docanchor>
    <docanchor file="index">pipeBase_argumentParser_dataIDs</docanchor>
    <docanchor file="index">pipeBase_argumentParser_argumentFiles</docanchor>
    <docanchor file="index">pipeBase_argumentParser_configOverride</docanchor>
    <docanchor file="index">pipeBase_environmentVariables</docanchor>
    <docanchor file="index">pipeBase_configOverrideFiles</docanchor>
  </compound>
</tagfile>
