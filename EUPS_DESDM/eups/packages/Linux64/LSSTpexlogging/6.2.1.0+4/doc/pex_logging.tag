<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>howto.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/doc/</path>
    <filename>howto_8dox</filename>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>overview.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/doc/</path>
    <filename>overview_8dox</filename>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>logging.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/</path>
    <filename>logging_8h</filename>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <includes id="_debug_8h" name="Debug.h" local="yes" imported="no">lsst/pex/logging/Debug.h</includes>
    <includes id="_file_destination_8h" name="FileDestination.h" local="yes" imported="no">lsst/pex/logging/FileDestination.h</includes>
  </compound>
  <compound kind="file">
    <name>BlockTimingLog.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_block_timing_log_8h</filename>
    <includes id="_log_record_8h" name="LogRecord.h" local="yes" imported="no">lsst/pex/logging/LogRecord.h</includes>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <class kind="class">lsst::pex::logging::BlockTimingLog</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Component.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_component_8h</filename>
    <class kind="class">lsst::pex::logging::Component</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Debug.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_debug_8h</filename>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <class kind="class">lsst::pex::logging::Debug</class>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_DEBUGGING_ON</name>
      <anchorfile>_debug_8h.html</anchorfile>
      <anchor>a17b566f49b51b6b118f4749380918961</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_MAX_DEBUG</name>
      <anchorfile>_debug_8h.html</anchorfile>
      <anchor>a363ae64e477674557ee6024a93685057</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a50fd003d8332a8ee7ffdc55471eb7c69</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a2bf17a8708034632c4d314962f556e1e</anchor>
      <arglist>(const std::string &amp;name, const char *fmt,...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DualLog.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_dual_log_8h</filename>
    <includes id="_screen_log_8h" name="ScreenLog.h" local="yes" imported="no">lsst/pex/logging/ScreenLog.h</includes>
    <class kind="class">lsst::pex::logging::DualLog</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>FileDestination.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_file_destination_8h</filename>
    <includes id="_log_destination_8h" name="LogDestination.h" local="yes" imported="no">lsst/pex/logging/LogDestination.h</includes>
    <class kind="class">lsst::pex::logging::FileDestination</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Log.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_log_8h</filename>
    <includes id="_log_record_8h" name="LogRecord.h" local="yes" imported="no">lsst/pex/logging/LogRecord.h</includes>
    <includes id="_log_destination_8h" name="LogDestination.h" local="yes" imported="no">lsst/pex/logging/LogDestination.h</includes>
    <includes id="_memory_8h" name="Memory.h" local="yes" imported="no">lsst/pex/logging/threshold/Memory.h</includes>
    <class kind="class">lsst::pex::logging::Log</class>
    <class kind="class">lsst::pex::logging::LogRec</class>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_log_8h.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>ATTRIB_FORMAT</name>
      <anchorfile>_log_8h.html</anchorfile>
      <anchor>a796af0b384b6d10f8659fa46e75546ca</anchor>
      <arglist>(fmt, start)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEVELF</name>
      <anchorfile>_log_8h.html</anchorfile>
      <anchor>ade45fabf708055858237a39bb1c9ef79</anchor>
      <arglist>(fname, lev)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LEVELF</name>
      <anchorfile>_log_8h.html</anchorfile>
      <anchor>ade45fabf708055858237a39bb1c9ef79</anchor>
      <arglist>(fname, lev)</arglist>
    </member>
    <member kind="typedef">
      <type>LogRec</type>
      <name>Rec</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ac5d33786f6811233f1857a7df53b2cce</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>LogClient.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_log_client_8h</filename>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <class kind="class">lsst::pex::logging::LogClient</class>
    <class kind="class">lsst::pex::logging::LogClientHelper</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>LogDestination.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_log_destination_8h</filename>
    <includes id="_log_formatter_8h" name="LogFormatter.h" local="yes" imported="no">lsst/pex/logging/LogFormatter.h</includes>
    <includes id="enum_8h" name="enum.h" local="yes" imported="no">lsst/pex/logging/threshold/enum.h</includes>
    <class kind="class">lsst::pex::logging::LogDestination</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>LogFormatter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_log_formatter_8h</filename>
    <class kind="class">lsst::pex::logging::LogFormatter</class>
    <class kind="class">lsst::pex::logging::BriefFormatter</class>
    <class kind="class">lsst::pex::logging::IndentedFormatter</class>
    <class kind="class">lsst::pex::logging::NetLoggerFormatter</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>LogRecord.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_log_record_8h</filename>
    <class kind="class">lsst::pex::logging::RecordProperty</class>
    <class kind="class">lsst::pex::logging::Prop</class>
    <class kind="class">lsst::pex::logging::LogRecord</class>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_LP_COMMENT</name>
      <anchorfile>_log_record_8h.html</anchorfile>
      <anchor>af64a442b7766b4cf0267d579491f2ff8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_LP_TIMESTAMP</name>
      <anchorfile>_log_record_8h.html</anchorfile>
      <anchor>aba5779099cfafe81f1f31673bceca6c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_LP_DATE</name>
      <anchorfile>_log_record_8h.html</anchorfile>
      <anchor>a64f943249b1b3a976554ec8469b93d79</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_LP_LOG</name>
      <anchorfile>_log_record_8h.html</anchorfile>
      <anchor>a0c762a87226b569521cf8246705ff678</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_LP_LEVEL</name>
      <anchorfile>_log_record_8h.html</anchorfile>
      <anchor>aedd03f45413c54fe926167ddd51b8ec5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PropertyPrinter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_property_printer_8h</filename>
    <class kind="class">lsst::pex::logging::PrinterIter</class>
    <class kind="class">lsst::pex::logging::BaseTmplPrinterIter</class>
    <class kind="class">lsst::pex::logging::TmplPrinterIter</class>
    <class kind="class">lsst::pex::logging::WrappedPrinterIter</class>
    <class kind="class">lsst::pex::logging::PrinterList</class>
    <class kind="class">lsst::pex::logging::BaseTmplPrinterList</class>
    <class kind="class">lsst::pex::logging::TmplPrinterList</class>
    <class kind="class">lsst::pex::logging::DateTimePrinterIter</class>
    <class kind="class">lsst::pex::logging::DateTimePrinterList</class>
    <class kind="class">lsst::pex::logging::BoolPrinterIter</class>
    <class kind="class">lsst::pex::logging::BoolPrinterList</class>
    <class kind="class">lsst::pex::logging::PrinterFactory</class>
    <class kind="class">lsst::pex::logging::PropertyPrinter</class>
    <namespace>lsst::pex::logging</namespace>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makePrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ad6bc158647ddfb94e76e9381161eb360</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makeDateTimePrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ac5ba7459b7060418de72668ed07d3caa</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makeBoolPrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>af10fbc2806d6f004b4a3087d19b5632e</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ScreenLog.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_screen_log_8h</filename>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <class kind="class">lsst::pex::logging::ScreenLog</class>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>enum.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/threshold/</path>
    <filename>enum_8h</filename>
    <namespace>lsst::pex::logging</namespace>
    <namespace>lsst::pex::logging::threshold</namespace>
    <member kind="enumvalue">
      <name>INHERIT</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a09d4aa93a28d4a4f7793c25c9daedc4aa92926abec3b2a9ae58ec4cf8e41f0e01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PASS_ALL</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a507c7a5ccb6060295c5131d5d6b614a2aa6531362f3b98bc34c64562034372a9e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Memory.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/threshold/</path>
    <filename>_memory_8h</filename>
    <includes id="enum_8h" name="enum.h" local="yes" imported="no">lsst/pex/logging/threshold/enum.h</includes>
    <class kind="class">lsst::pex::logging::threshold::Family</class>
    <class kind="class">lsst::pex::logging::threshold::Memory</class>
    <namespace>lsst::pex::logging</namespace>
    <namespace>lsst::pex::logging::threshold</namespace>
    <member kind="typedef">
      <type>boost::tokenizer&lt; boost::char_separator&lt; char &gt; &gt;</type>
      <name>tokenizer</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a24bfa7847e251e7a0c22c54794450b2b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Trace.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>_trace_8h</filename>
    <includes id="_debug_8h" name="Debug.h" local="yes" imported="no">lsst/pex/logging/Debug.h</includes>
    <class kind="class">lsst::pex::logging::Trace</class>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_NO_TRACE</name>
      <anchorfile>_trace_8h.html</anchorfile>
      <anchor>a0bc8ca13f525259639b1b97558e68562</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_MAX_TRACE</name>
      <anchorfile>_trace_8h.html</anchorfile>
      <anchor>a38976cfd5857871a4654ca0b100ee08e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>TTrace</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a98fa95abdbadf925464d16d004ced055</anchor>
      <arglist>(const char *name, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>TTrace</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a3be21ad2ddefc080fc40287dd0f87925</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;fmt,...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/pex/</path>
    <filename>pex_2____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pex.html</anchorfile>
      <anchor>a911b93fdf688c7be7c768463f2d8d041</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/pex/logging/</path>
    <filename>pex_2logging_2____init_____8py</filename>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/pex/logging/</path>
    <filename>version_8py</filename>
    <namespace>lsst::pex::logging::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a0ff25a71e192432712cd06800ea5fa15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a782657e44eb6894737bc7d94162a24ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a8b12ff26d0777768f9b3b8db8d6c066f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a84893280b661c0bddd9db7874478f7ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>ae000dea61435b3b6af5644d2242036b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a6196f068fc274f5cfa84779937e3656f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a93838034009c015e7b3d80689e3fbe08</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>BlockTimingLog.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_block_timing_log_8cc</filename>
    <includes id="_block_timing_log_8h" name="BlockTimingLog.h" local="yes" imported="no">lsst/pex/logging/BlockTimingLog.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Component.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_component_8cc</filename>
    <includes id="_component_8h" name="Component.h" local="yes" imported="no">lsst/pex/logging/Component.h</includes>
  </compound>
  <compound kind="file">
    <name>DualLog.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_dual_log_8cc</filename>
    <includes id="_dual_log_8h" name="DualLog.h" local="yes" imported="no">lsst/pex/logging/DualLog.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>FileDestination.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_file_destination_8cc</filename>
    <includes id="_file_destination_8h" name="FileDestination.h" local="yes" imported="no">lsst/pex/logging/FileDestination.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Log.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_log_8cc</filename>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <includes id="_screen_log_8h" name="ScreenLog.h" local="yes" imported="no">lsst/pex/logging/ScreenLog.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>LogDestination.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_log_destination_8cc</filename>
    <includes id="_log_destination_8h" name="LogDestination.h" local="yes" imported="no">lsst/pex/logging/LogDestination.h</includes>
    <includes id="_log_record_8h" name="LogRecord.h" local="yes" imported="no">lsst/pex/logging/LogRecord.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>LogFormatter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_log_formatter_8cc</filename>
    <includes id="_log_formatter_8h" name="LogFormatter.h" local="yes" imported="no">lsst/pex/logging/LogFormatter.h</includes>
    <includes id="_log_record_8h" name="LogRecord.h" local="yes" imported="no">lsst/pex/logging/LogRecord.h</includes>
    <includes id="_log_8h" name="Log.h" local="yes" imported="no">lsst/pex/logging/Log.h</includes>
    <includes id="_property_printer_8h" name="PropertyPrinter.h" local="yes" imported="no">lsst/pex/logging/PropertyPrinter.h</includes>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_TL_ADD</name>
      <anchorfile>_log_formatter_8cc.html</anchorfile>
      <anchor>ac544e7984f703028415261284d63b076</anchor>
      <arglist>(T, C)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>LogRecord.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_log_record_8cc</filename>
    <includes id="_log_record_8h" name="LogRecord.h" local="yes" imported="no">lsst/pex/logging/LogRecord.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>PropertyPrinter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_property_printer_8cc</filename>
    <includes id="_property_printer_8h" name="PropertyPrinter.h" local="yes" imported="no">lsst/pex/logging/PropertyPrinter.h</includes>
    <namespace>lsst::pex::logging</namespace>
    <member kind="define">
      <type>#define</type>
      <name>PF_ADD</name>
      <anchorfile>_property_printer_8cc.html</anchorfile>
      <anchor>a71731fac2a7ca9a84707954761e44170</anchor>
      <arglist>(T)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ScreenLog.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>_screen_log_8cc</filename>
    <includes id="_screen_log_8h" name="ScreenLog.h" local="yes" imported="no">lsst/pex/logging/ScreenLog.h</includes>
    <namespace>lsst::pex::logging</namespace>
  </compound>
  <compound kind="file">
    <name>Memory.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/threshold/</path>
    <filename>_memory_8cc</filename>
    <includes id="_memory_8h" name="Memory.h" local="yes" imported="no">lsst/pex/logging/threshold/Memory.h</includes>
    <namespace>lsst::pex::logging</namespace>
    <namespace>lsst::pex::logging::threshold</namespace>
  </compound>
  <compound kind="page">
    <name>pgLogHowto</name>
    <title>The Logging How-To</title>
    <filename>pg_log_howto</filename>
    <docanchor file="pg_log_howto">secLogHTBasics</docanchor>
    <docanchor file="pg_log_howto">secLogHTNormal</docanchor>
    <docanchor file="pg_log_howto">secLogHTDebug</docanchor>
    <docanchor file="pg_log_howto">secLogHTTrace</docanchor>
    <docanchor file="pg_log_howto">secLogHTDetails</docanchor>
    <docanchor file="pg_log_howto">secLogHTRoot</docanchor>
    <docanchor file="pg_log_howto">secLogHTChild</docanchor>
    <docanchor file="pg_log_howto">secLogHTVerb</docanchor>
    <docanchor file="pg_log_howto">secLogHTSimple</docanchor>
    <docanchor file="pg_log_howto">secLogHTNewRoot</docanchor>
    <docanchor file="pg_log_howto">secLogHTProp</docanchor>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::logging</name>
    <filename>namespacelsst_1_1pex_1_1logging.html</filename>
    <namespace>lsst::pex::logging::threshold</namespace>
    <namespace>lsst::pex::logging::version</namespace>
    <class kind="class">lsst::pex::logging::BlockTimingLog</class>
    <class kind="class">lsst::pex::logging::Component</class>
    <class kind="class">lsst::pex::logging::Debug</class>
    <class kind="class">lsst::pex::logging::DualLog</class>
    <class kind="class">lsst::pex::logging::FileDestination</class>
    <class kind="class">lsst::pex::logging::Log</class>
    <class kind="class">lsst::pex::logging::LogRec</class>
    <class kind="class">lsst::pex::logging::LogClient</class>
    <class kind="class">lsst::pex::logging::LogClientHelper</class>
    <class kind="class">lsst::pex::logging::LogDestination</class>
    <class kind="class">lsst::pex::logging::LogFormatter</class>
    <class kind="class">lsst::pex::logging::BriefFormatter</class>
    <class kind="class">lsst::pex::logging::IndentedFormatter</class>
    <class kind="class">lsst::pex::logging::NetLoggerFormatter</class>
    <class kind="class">lsst::pex::logging::RecordProperty</class>
    <class kind="class">lsst::pex::logging::Prop</class>
    <class kind="class">lsst::pex::logging::LogRecord</class>
    <class kind="class">lsst::pex::logging::PrinterIter</class>
    <class kind="class">lsst::pex::logging::BaseTmplPrinterIter</class>
    <class kind="class">lsst::pex::logging::TmplPrinterIter</class>
    <class kind="class">lsst::pex::logging::WrappedPrinterIter</class>
    <class kind="class">lsst::pex::logging::PrinterList</class>
    <class kind="class">lsst::pex::logging::BaseTmplPrinterList</class>
    <class kind="class">lsst::pex::logging::TmplPrinterList</class>
    <class kind="class">lsst::pex::logging::DateTimePrinterIter</class>
    <class kind="class">lsst::pex::logging::DateTimePrinterList</class>
    <class kind="class">lsst::pex::logging::BoolPrinterIter</class>
    <class kind="class">lsst::pex::logging::BoolPrinterList</class>
    <class kind="class">lsst::pex::logging::PrinterFactory</class>
    <class kind="class">lsst::pex::logging::PropertyPrinter</class>
    <class kind="class">lsst::pex::logging::ScreenLog</class>
    <class kind="class">lsst::pex::logging::Trace</class>
    <member kind="typedef">
      <type>LogRec</type>
      <name>Rec</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ac5d33786f6811233f1857a7df53b2cce</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a50fd003d8332a8ee7ffdc55471eb7c69</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a2bf17a8708034632c4d314962f556e1e</anchor>
      <arglist>(const std::string &amp;name, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makePrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ad6bc158647ddfb94e76e9381161eb360</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makeDateTimePrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>ac5ba7459b7060418de72668ed07d3caa</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makeBoolPrinter</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>af10fbc2806d6f004b4a3087d19b5632e</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>TTrace</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a98fa95abdbadf925464d16d004ced055</anchor>
      <arglist>(const char *name, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>TTrace</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging.html</anchorfile>
      <anchor>a3be21ad2ddefc080fc40287dd0f87925</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;fmt,...)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BlockTimingLog</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</filename>
    <base>lsst::pex::logging::Log</base>
    <member kind="enumeration">
      <name>usageData</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NOUDATA</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a240d4052d62189d33b009920027dc86f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UTIME</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79ae13924df7fa6841c3b209d45bdd7bb50</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STIME</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79af02174cbf165b650a238af2b3ef8fbb3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>SUTIME</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79ab0f0f70add1682a1ca1233ae66b6a5af</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MEMSZ</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79ab64029e64b57a6208eb75a5884ff587d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>NSWAP</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a8bba38af451157dec5a07c8af62be178</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BLKIN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a48b394269daac5fa6b95f2eeaa20ee45</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BLKOUT</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79ac269ef2d60e175f2df11b03f0259a7d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BLKIO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a47082ad509065835b14d1cf14b7b6620</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MINFLT</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a8bb3ff19d951af8a138d4878fa463db4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MAJFLT</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79aa3d44818022f6b98085e896cd717ad47</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>LINUXUDATA</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79ad8fada14009588f499074e48e3798011</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ALLUDATA</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79abfb65e34c297e760c2ead8b6946995e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PARENTUDATA</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8c8f81687da2030633afb870bcfcaa79a36efd643094669cee7371cddb019b020</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BlockTimingLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8ab02b65b9a2e16b9d66ae463e7fda28</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;name, int tracelev=BlockTimingLog::INSTRUM, int usageFlags=PARENTUDATA, const std::string &amp;funcName=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BlockTimingLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a60d611b0948643ddc32928c7e8398feb</anchor>
      <arglist>(const BlockTimingLog &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>BlockTimingLog &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a14be20d691ba7242828fea0d5b282c4e</anchor>
      <arglist>(const BlockTimingLog &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getUsageFlags</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>ae15a3c8830335dfe9eca332caaeaf260</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setUsageFlags</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a4c53e9cf58f9e01748954b097df0f468</anchor>
      <arglist>(int flags)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addUsageFlags</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>abd280545e573c26689e4002f05e8243b</anchor>
      <arglist>(int flags)</arglist>
    </member>
    <member kind="function">
      <type>BlockTimingLog *</type>
      <name>createForBlock</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a095beaadde5c6c7a81d46d6d8d436024</anchor>
      <arglist>(const std::string &amp;name, int tracelev=Log::INHERIT_THRESHOLD, const std::string &amp;funcName=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type>BlockTimingLog *</type>
      <name>timeBlock</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a9c55ef10381397895508440c4041413b</anchor>
      <arglist>(const std::string &amp;name, int tracelev=Log::INHERIT_THRESHOLD, const std::string &amp;funcName=&quot;&quot;)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BlockTimingLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>aed1edea2de44317d6b84cfb4bb19b4ba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a8d16cdf0780e42c7a73ff3466746503b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>start</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>afb71d8b9d0ac588f324aed736cff8171</anchor>
      <arglist>(const std::string &amp;funcName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>done</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a1ceaa31e7b4de006fda3544adbbf3298</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getInstrumentationLevel</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>ab30ac65cf70ecf0246874e4f788cb9f6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getFunctionName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a80c08d29e7968c23184641b0a2f67fea</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addUsageProps</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a97ec787e37a6d3ac33eeaa9d690432ed</anchor>
      <arglist>(LogRecord &amp;rec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5ea246bd93a1f57b8a0bfc796ca102db</anchor>
      <arglist>(const int threshold=INFO, const std::string &amp;name=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abc7bc74e560cd042fa025db31e7d3c83</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO, bool defaultShowAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af49865ef88f9c3f6c6f8e1eff3db72d8</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;childName, int threshold=INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aba7f3c6f2461b131118d30863d45b230</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5a2b689e4ef4f473bbe33a1b393283a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Log &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a93f8685b2415aa3d8aaa3fc16dfc3a9b</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a53b2beea1c6b23045a394f2adc3515</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a0b35030b1193fc57729f1ed1f23322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af70a59bbe37df3b6cbea6c61347db864</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>sends</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2cb13a039c37f55534833ab6f8a208</anchor>
      <arglist>(int importance) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a445b2518f5c5b2783fe2e6926de66e0c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a889cff19f967c20e7b0ed84d8e420a2b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8d5502590612259ced05d1a7bfa28a40</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a59b7fe237c46de30076d88855f2a1f6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4db8595c790fcd491f60c3c5032ee166</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>afe1c0a37616d30b9a15e29562632d1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1c369d283ea7dc0335b54e064019da5d</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a226fa5caead5075f4ea252f3fa23b442</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>Log *</type>
      <name>createChildLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3032a9cc687af9dc962d43a35acfd82</anchor>
      <arglist>(const std::string &amp;childName, int threshold=INHERIT_THRESHOLD) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6e7aca8a63be0147180844a4251441b</anchor>
      <arglist>(int importance, const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af5e82e65d8b7d5fd8ebac286e7895053</anchor>
      <arglist>(int importance, const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acb2e325ea37fb993ef0c530d86bbba7a</anchor>
      <arglist>(int importance, const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acc0539b7de3738ea01ad78fdf67d0abb</anchor>
      <arglist>(int importance, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a25314b164eac96dcc85f7db549b97689</anchor>
      <arglist>(int importance, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4f2618cc9575ad5f43e215fde67e69c0</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2cb4167faa85207ec0e179c9e5097e91</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4e95be08f6ea47c96f1f2e8bdef2015b</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8a0efa3588c971e561af60a65a41fa9e</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9330a55bb22f22d311833a551932656c</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae8c958aeab853fba3038f4bd5bf9fc81</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5dda4dcd410d10bec971620cf34f93b3</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1a695b2d511d8f7775dfa15f7b8e57e5</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3c844f958960e331495c283cbf43f22c</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a27583bf3dddf2720135bd1eb49f1ef46</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac0edb0a5b545da2ab83c90b264efdac1</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a383296dfe1521df229ae2bc69016828b</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4d49a08d21672b02724920981d675e04</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a048ce3e1e432cb5acb4f5ddf9b26dc47</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fcdc8fbf5e3fda46ca8aff37afcc6c6</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2b94aedf6297fac70f744037d009446d</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6d35e4d8afa07d9d942bf4f8bd38145</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa5a1c03c0d56424d5d252839528ba389</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa8ea6716b8cc9164f787f5aaa0f7e485</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a6814ee85d0a1a0bd790c9c20650ba5df</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abdd252bc0329ee51fc6dfc483bfc5744</anchor>
      <arglist>(int importance, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debugf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac4c4b41daa6003606723f00c947b36e9</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>infof</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fb91f93ae359d51837f3fce07811c5e</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warnf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a744ca6d0c0db19e9130274549879b417</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatalf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3a00b106156319003053b3640a74fe05</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2b23e481173716a109ac828a6c6a19</anchor>
      <arglist>(const LogRecord &amp;record)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a12e1e7a5f74109f507f00cd19603d9b1</anchor>
      <arglist>(std::ostream &amp;destination, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a006ebee8bee8a86bf8b195a42bf780ae</anchor>
      <arglist>(std::ostream &amp;destination, int threshold, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a65f7afb296ece8024126ead591a77feb</anchor>
      <arglist>(const boost::shared_ptr&lt; LogDestination &gt; &amp;destination)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getPreamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a07a141da270ee8156acb45737dae86b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a382e1c4fa45c443fe907ca3c5bd4e573</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a80e998f8b3054dc0f90cd9f87e532618</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8b0610ca21a7dcad81a953640844875e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INSTRUM</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>a43602cd074119a2e51e1fbb6e2962bc5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>STATUS</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>ac4d64e990a36edf10896d2044692bc6e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>START</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>ac33164115be0569399aebcdab45380db</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>END</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_block_timing_log.html</anchorfile>
      <anchor>abf99efcdd9ebd62c65897d461a105759</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>DEBUG</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acd7e6b84345ad03108095d7248116fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INFO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af3f69688ac95f40e2dd0b46ba01ea7de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>WARN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1444b33e122c3a2eb19a3e57922b69d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INHERIT_THRESHOLD</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5e7f680fbc09576a4db6c5df7d84632f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>FATAL</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a56997b47bc199dd91016f982485d0153</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Log &amp;</type>
      <name>getDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a14b8432754d9ee1a1af9ea092a158332</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aaa60fcad95f0ce7e9e01e4ec48575a5c</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>closeDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2725662306312839a60938864731d4e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af1d5655fa2b4cea7887a8856861f6fbb</anchor>
      <arglist>(int threshold, int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3b1b897af38e393d8f743aff1d6541c</anchor>
      <arglist>(int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static void</type>
      <name>setDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a78b8046c875e1e03aa3ab547d4578215</anchor>
      <arglist>(Log *deflog)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; threshold::Memory &gt;</type>
      <name>_thresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad206c417fb04942f48d2db6c72249285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt;</type>
      <name>_destinations</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a11acd5225a6963a56cac78e11588b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_preamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a105ba415a35949cec51fc6f316bb3714</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static Log *</type>
      <name>defaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad0847f994499165035e09e230390b72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static const std::string</type>
      <name>_sep</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a95a99c329e6ecee6821b80e2141cf311</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::Component</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_component.html</filename>
    <member kind="enumvalue">
      <name>INHERIT_VERBOSITY</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a255aa4595dacb536b36e5a86b1388a25a2fb35feda8da5871a11434c6af09ba5e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Component</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a2f571f3aab4da848ac56fd64ccbd3eee</anchor>
      <arglist>(const std::string &amp;name=&quot;.&quot;, int verbosity=INHERIT_VERBOSITY)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Component</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>ab8378fa275af98e568a7e91d33d867af</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a8ec4da8c7363100ac406a1140d55c7cc</anchor>
      <arglist>(const std::string &amp;name, int verbosity, const std::string &amp;separator)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>ae0b314b384881ef2fc97a35422496bcb</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;separator)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>highestVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a788268335af7da533eb3d1b6105ebb7d</anchor>
      <arglist>(int highest=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a0fdc5d9bd8f858b6eabd0db6ac9d8b76</anchor>
      <arglist>(std::ostream &amp;fp, int depth=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_component.html</anchorfile>
      <anchor>a253496ea0c5081f6efdeb238728c483a</anchor>
      <arglist>(int verbosity)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::Debug</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_debug.html</filename>
    <base>lsst::pex::logging::Log</base>
    <member kind="function">
      <type></type>
      <name>Debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>ad78c2683d82fb680223fbe6d5bee3abe</anchor>
      <arglist>(const std::string &amp;name, int verbosity=-1 *Log::INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>a74124d7177caeb9efb4a2676994df438</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;name, int verbosity=-1 *Log::INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>ac82bfdba029c3cf6621ddb413d3ed7f2</anchor>
      <arglist>(const Debug &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>Debug &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>ae3ec1378edbd064a2b3bab7580402328</anchor>
      <arglist>(const Debug &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>ad2bba0024f1bbe08bb7df13d005188e2</anchor>
      <arglist>(int verbosity, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>a569e1ab63a0a01f4afea74b43292e9de</anchor>
      <arglist>(int verbosity, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>af4c9b17fc3b549e3de5579e1df534b08</anchor>
      <arglist>(int verbosity, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>a688347a0a3e2486664f7531c2ee1ac70</anchor>
      <arglist>(int verbosity, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>a0a362fdf9c9388134afbb83d207528f9</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_debug.html</anchorfile>
      <anchor>a93994f4167cecfb8d09c1592d5d80f44</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5ea246bd93a1f57b8a0bfc796ca102db</anchor>
      <arglist>(const int threshold=INFO, const std::string &amp;name=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abc7bc74e560cd042fa025db31e7d3c83</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO, bool defaultShowAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af49865ef88f9c3f6c6f8e1eff3db72d8</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;childName, int threshold=INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aba7f3c6f2461b131118d30863d45b230</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5a2b689e4ef4f473bbe33a1b393283a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Log &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a93f8685b2415aa3d8aaa3fc16dfc3a9b</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a53b2beea1c6b23045a394f2adc3515</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a0b35030b1193fc57729f1ed1f23322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af70a59bbe37df3b6cbea6c61347db864</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>sends</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2cb13a039c37f55534833ab6f8a208</anchor>
      <arglist>(int importance) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a445b2518f5c5b2783fe2e6926de66e0c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a889cff19f967c20e7b0ed84d8e420a2b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8d5502590612259ced05d1a7bfa28a40</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a59b7fe237c46de30076d88855f2a1f6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4db8595c790fcd491f60c3c5032ee166</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>afe1c0a37616d30b9a15e29562632d1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1c369d283ea7dc0335b54e064019da5d</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a226fa5caead5075f4ea252f3fa23b442</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>Log *</type>
      <name>createChildLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3032a9cc687af9dc962d43a35acfd82</anchor>
      <arglist>(const std::string &amp;childName, int threshold=INHERIT_THRESHOLD) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6e7aca8a63be0147180844a4251441b</anchor>
      <arglist>(int importance, const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af5e82e65d8b7d5fd8ebac286e7895053</anchor>
      <arglist>(int importance, const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acb2e325ea37fb993ef0c530d86bbba7a</anchor>
      <arglist>(int importance, const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acc0539b7de3738ea01ad78fdf67d0abb</anchor>
      <arglist>(int importance, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a25314b164eac96dcc85f7db549b97689</anchor>
      <arglist>(int importance, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4f2618cc9575ad5f43e215fde67e69c0</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2cb4167faa85207ec0e179c9e5097e91</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4e95be08f6ea47c96f1f2e8bdef2015b</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8a0efa3588c971e561af60a65a41fa9e</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9330a55bb22f22d311833a551932656c</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae8c958aeab853fba3038f4bd5bf9fc81</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5dda4dcd410d10bec971620cf34f93b3</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1a695b2d511d8f7775dfa15f7b8e57e5</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3c844f958960e331495c283cbf43f22c</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a27583bf3dddf2720135bd1eb49f1ef46</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac0edb0a5b545da2ab83c90b264efdac1</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a383296dfe1521df229ae2bc69016828b</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4d49a08d21672b02724920981d675e04</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a048ce3e1e432cb5acb4f5ddf9b26dc47</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fcdc8fbf5e3fda46ca8aff37afcc6c6</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2b94aedf6297fac70f744037d009446d</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6d35e4d8afa07d9d942bf4f8bd38145</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa5a1c03c0d56424d5d252839528ba389</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa8ea6716b8cc9164f787f5aaa0f7e485</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a6814ee85d0a1a0bd790c9c20650ba5df</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abdd252bc0329ee51fc6dfc483bfc5744</anchor>
      <arglist>(int importance, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debugf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac4c4b41daa6003606723f00c947b36e9</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>infof</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fb91f93ae359d51837f3fce07811c5e</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warnf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a744ca6d0c0db19e9130274549879b417</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatalf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3a00b106156319003053b3640a74fe05</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2b23e481173716a109ac828a6c6a19</anchor>
      <arglist>(const LogRecord &amp;record)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a12e1e7a5f74109f507f00cd19603d9b1</anchor>
      <arglist>(std::ostream &amp;destination, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a006ebee8bee8a86bf8b195a42bf780ae</anchor>
      <arglist>(std::ostream &amp;destination, int threshold, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a65f7afb296ece8024126ead591a77feb</anchor>
      <arglist>(const boost::shared_ptr&lt; LogDestination &gt; &amp;destination)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getPreamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a07a141da270ee8156acb45737dae86b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a382e1c4fa45c443fe907ca3c5bd4e573</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a80e998f8b3054dc0f90cd9f87e532618</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8b0610ca21a7dcad81a953640844875e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Log &amp;</type>
      <name>getDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a14b8432754d9ee1a1af9ea092a158332</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aaa60fcad95f0ce7e9e01e4ec48575a5c</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>closeDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2725662306312839a60938864731d4e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>DEBUG</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acd7e6b84345ad03108095d7248116fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INFO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af3f69688ac95f40e2dd0b46ba01ea7de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>WARN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1444b33e122c3a2eb19a3e57922b69d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INHERIT_THRESHOLD</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5e7f680fbc09576a4db6c5df7d84632f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>FATAL</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a56997b47bc199dd91016f982485d0153</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af1d5655fa2b4cea7887a8856861f6fbb</anchor>
      <arglist>(int threshold, int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3b1b897af38e393d8f743aff1d6541c</anchor>
      <arglist>(int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static void</type>
      <name>setDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a78b8046c875e1e03aa3ab547d4578215</anchor>
      <arglist>(Log *deflog)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; threshold::Memory &gt;</type>
      <name>_thresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad206c417fb04942f48d2db6c72249285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt;</type>
      <name>_destinations</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a11acd5225a6963a56cac78e11588b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_preamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a105ba415a35949cec51fc6f316bb3714</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static Log *</type>
      <name>defaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad0847f994499165035e09e230390b72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static const std::string</type>
      <name>_sep</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a95a99c329e6ecee6821b80e2141cf311</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::DualLog</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_dual_log.html</filename>
    <base>lsst::pex::logging::ScreenLog</base>
    <member kind="function">
      <type></type>
      <name>DualLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>afe92d47476846c9f815a04987515c2a6</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;filename, int filethresh=0, int screenthresh=0, bool screenVerbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DualLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a143112b1ad178c5f35ca8cbe4691bfcd</anchor>
      <arglist>(const std::string &amp;filename, int filethresh=0, int screenthresh=0, bool screenVerbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DualLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a2dbc9c6d520ae3e592b6864ba0654d0f</anchor>
      <arglist>(const DualLog &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DualLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>aafed583621e4d34a701e142433bc7d89</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>DualLog &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a2f799dba25ae660fb772a1dde2bd8520</anchor>
      <arglist>(const DualLog &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getFileThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a9afc464f97290e1d7c7efd04ee2a77db</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFileThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>afb6ec4884e54143261769cebe3474373</anchor>
      <arglist>(int thresh)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>ac0f8d075ad5c7462640813597cd58e05</anchor>
      <arglist>(bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a0b305695a70c376f150e88fb44dcf0ad</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>adea20f8a24b510a1b4166df5245dabda</anchor>
      <arglist>(const ScreenLog &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a01479c8d8b1c73db9372c401e5a28538</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ScreenLog &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a8afa41cad84fae5dd189a36339d4d7db</anchor>
      <arglist>(const ScreenLog &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getScreenThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>af79517d833d5bbe4c247851647711030</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>aeb13d6fedb0f2bf9ac845951b45c3dc8</anchor>
      <arglist>(int thresh)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a136124f09c4daf1db7da7a637731885d</anchor>
      <arglist>(bool printAll)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isScreenVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a8e19db8be9c05832e5f515be0b57ccf3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5ea246bd93a1f57b8a0bfc796ca102db</anchor>
      <arglist>(const int threshold=INFO, const std::string &amp;name=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abc7bc74e560cd042fa025db31e7d3c83</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO, bool defaultShowAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af49865ef88f9c3f6c6f8e1eff3db72d8</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;childName, int threshold=INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aba7f3c6f2461b131118d30863d45b230</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5a2b689e4ef4f473bbe33a1b393283a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Log &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a93f8685b2415aa3d8aaa3fc16dfc3a9b</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a53b2beea1c6b23045a394f2adc3515</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a0b35030b1193fc57729f1ed1f23322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af70a59bbe37df3b6cbea6c61347db864</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>sends</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2cb13a039c37f55534833ab6f8a208</anchor>
      <arglist>(int importance) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a445b2518f5c5b2783fe2e6926de66e0c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a889cff19f967c20e7b0ed84d8e420a2b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8d5502590612259ced05d1a7bfa28a40</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a59b7fe237c46de30076d88855f2a1f6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4db8595c790fcd491f60c3c5032ee166</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>afe1c0a37616d30b9a15e29562632d1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1c369d283ea7dc0335b54e064019da5d</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a226fa5caead5075f4ea252f3fa23b442</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>Log *</type>
      <name>createChildLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3032a9cc687af9dc962d43a35acfd82</anchor>
      <arglist>(const std::string &amp;childName, int threshold=INHERIT_THRESHOLD) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6e7aca8a63be0147180844a4251441b</anchor>
      <arglist>(int importance, const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af5e82e65d8b7d5fd8ebac286e7895053</anchor>
      <arglist>(int importance, const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acb2e325ea37fb993ef0c530d86bbba7a</anchor>
      <arglist>(int importance, const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acc0539b7de3738ea01ad78fdf67d0abb</anchor>
      <arglist>(int importance, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a25314b164eac96dcc85f7db549b97689</anchor>
      <arglist>(int importance, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4f2618cc9575ad5f43e215fde67e69c0</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2cb4167faa85207ec0e179c9e5097e91</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4e95be08f6ea47c96f1f2e8bdef2015b</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8a0efa3588c971e561af60a65a41fa9e</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9330a55bb22f22d311833a551932656c</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae8c958aeab853fba3038f4bd5bf9fc81</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5dda4dcd410d10bec971620cf34f93b3</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1a695b2d511d8f7775dfa15f7b8e57e5</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3c844f958960e331495c283cbf43f22c</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a27583bf3dddf2720135bd1eb49f1ef46</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac0edb0a5b545da2ab83c90b264efdac1</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a383296dfe1521df229ae2bc69016828b</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4d49a08d21672b02724920981d675e04</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a048ce3e1e432cb5acb4f5ddf9b26dc47</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fcdc8fbf5e3fda46ca8aff37afcc6c6</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2b94aedf6297fac70f744037d009446d</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6d35e4d8afa07d9d942bf4f8bd38145</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa5a1c03c0d56424d5d252839528ba389</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa8ea6716b8cc9164f787f5aaa0f7e485</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a6814ee85d0a1a0bd790c9c20650ba5df</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abdd252bc0329ee51fc6dfc483bfc5744</anchor>
      <arglist>(int importance, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debugf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac4c4b41daa6003606723f00c947b36e9</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>infof</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fb91f93ae359d51837f3fce07811c5e</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warnf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a744ca6d0c0db19e9130274549879b417</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatalf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3a00b106156319003053b3640a74fe05</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2b23e481173716a109ac828a6c6a19</anchor>
      <arglist>(const LogRecord &amp;record)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a12e1e7a5f74109f507f00cd19603d9b1</anchor>
      <arglist>(std::ostream &amp;destination, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a006ebee8bee8a86bf8b195a42bf780ae</anchor>
      <arglist>(std::ostream &amp;destination, int threshold, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a65f7afb296ece8024126ead591a77feb</anchor>
      <arglist>(const boost::shared_ptr&lt; LogDestination &gt; &amp;destination)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getPreamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a07a141da270ee8156acb45737dae86b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a382e1c4fa45c443fe907ca3c5bd4e573</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a80e998f8b3054dc0f90cd9f87e532618</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8b0610ca21a7dcad81a953640844875e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a5b324feea4c7ab90f3d2061ee26aab5c</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;filename, int filethresh=Log::INHERIT_THRESHOLD, int screenthresh=0, bool screenVerbose=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_dual_log.html</anchorfile>
      <anchor>a4c2cf71daa71ea924dbc43321e52cbd8</anchor>
      <arglist>(const std::string &amp;filename, int filethresh=Log::INHERIT_THRESHOLD, int screenthresh=0, bool screenVerbose=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a6f32de0e377f62bb323fca0087504eeb</anchor>
      <arglist>(bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a37ad42356831b38a1a8bcad193fe2a46</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Log &amp;</type>
      <name>getDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a14b8432754d9ee1a1af9ea092a158332</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aaa60fcad95f0ce7e9e01e4ec48575a5c</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>closeDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2725662306312839a60938864731d4e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>DEBUG</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acd7e6b84345ad03108095d7248116fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INFO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af3f69688ac95f40e2dd0b46ba01ea7de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>WARN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1444b33e122c3a2eb19a3e57922b69d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INHERIT_THRESHOLD</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5e7f680fbc09576a4db6c5df7d84632f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>FATAL</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a56997b47bc199dd91016f982485d0153</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af1d5655fa2b4cea7887a8856861f6fbb</anchor>
      <arglist>(int threshold, int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3b1b897af38e393d8f743aff1d6541c</anchor>
      <arglist>(int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static void</type>
      <name>setDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a78b8046c875e1e03aa3ab547d4578215</anchor>
      <arglist>(Log *deflog)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; threshold::Memory &gt;</type>
      <name>_thresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad206c417fb04942f48d2db6c72249285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt;</type>
      <name>_destinations</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a11acd5225a6963a56cac78e11588b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_preamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a105ba415a35949cec51fc6f316bb3714</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static Log *</type>
      <name>defaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad0847f994499165035e09e230390b72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static const std::string</type>
      <name>_sep</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a95a99c329e6ecee6821b80e2141cf311</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::FileDestination</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_file_destination.html</filename>
    <base>lsst::pex::logging::LogDestination</base>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a822a864291755cd6a4d2614667465beb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const boost::filesystem::path &amp;</type>
      <name>getPath</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a29e0e822e66be15cb5af543117b12520</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>ae66bc36ee98c3602dfa8b869d30baf31</anchor>
      <arglist>(const std::string &amp;filepath, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a00d858d2039d76f183c4e3102319bc3d</anchor>
      <arglist>(const char *filepath, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>afccbc86060ac66b649fe9eef7325858c</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a8460eea2d3dd8a85b5af31f807fc4971</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, bool verbose=false, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>ad5e6de14334d09f5e8414dcdb742252d</anchor>
      <arglist>(const std::string &amp;filepath, bool verbose=false, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FileDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a9703dfeb893097635681bc1764b4d27c</anchor>
      <arglist>(const char *filepath, bool verbose=false, int threshold=threshold::PASS_ALL, bool truncate=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>aa158d6bd46ae7f42a05a99286b4bf6cc</anchor>
      <arglist>(std::ostream *strm, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter, int threshold=threshold::PASS_ALL)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a4cd4101a59fb0b4f918da229cb06fb82</anchor>
      <arglist>(const LogDestination &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>ad400c8fef9a154e0dabd3d2490c9e907</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogDestination &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a1b813ac2adfdbcee2c1f652b834bbf58</anchor>
      <arglist>(const LogDestination &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a56c439ce63ba2d99559ae07d7df9744d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a96483ff0d5e4ee31644ee2e994a430d1</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a33b824e3a3df712280e0803f18f0c790</anchor>
      <arglist>(const LogRecord &amp;rec)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::filesystem::path</type>
      <name>_path</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_file_destination.html</anchorfile>
      <anchor>a7126c1d6e7aa12bd5220d8dc596b87cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_threshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a6e9c7dc512cb2cae00eca8310d0ff504</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::ostream *</type>
      <name>_strm</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>aae36e993a3bd481aaee3f21db207bd1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; LogFormatter &gt;</type>
      <name>_frmtr</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a8c14d16d464f3bfa50afab0fd478120e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::Log</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log.html</filename>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5ea246bd93a1f57b8a0bfc796ca102db</anchor>
      <arglist>(const int threshold=INFO, const std::string &amp;name=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abc7bc74e560cd042fa025db31e7d3c83</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO, bool defaultShowAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af49865ef88f9c3f6c6f8e1eff3db72d8</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;childName, int threshold=INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aba7f3c6f2461b131118d30863d45b230</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5a2b689e4ef4f473bbe33a1b393283a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Log &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a93f8685b2415aa3d8aaa3fc16dfc3a9b</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a53b2beea1c6b23045a394f2adc3515</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a0b35030b1193fc57729f1ed1f23322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af70a59bbe37df3b6cbea6c61347db864</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>sends</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2cb13a039c37f55534833ab6f8a208</anchor>
      <arglist>(int importance) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a445b2518f5c5b2783fe2e6926de66e0c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a889cff19f967c20e7b0ed84d8e420a2b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8d5502590612259ced05d1a7bfa28a40</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a59b7fe237c46de30076d88855f2a1f6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4db8595c790fcd491f60c3c5032ee166</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>afe1c0a37616d30b9a15e29562632d1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1c369d283ea7dc0335b54e064019da5d</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a226fa5caead5075f4ea252f3fa23b442</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>Log *</type>
      <name>createChildLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3032a9cc687af9dc962d43a35acfd82</anchor>
      <arglist>(const std::string &amp;childName, int threshold=INHERIT_THRESHOLD) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6e7aca8a63be0147180844a4251441b</anchor>
      <arglist>(int importance, const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af5e82e65d8b7d5fd8ebac286e7895053</anchor>
      <arglist>(int importance, const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acb2e325ea37fb993ef0c530d86bbba7a</anchor>
      <arglist>(int importance, const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acc0539b7de3738ea01ad78fdf67d0abb</anchor>
      <arglist>(int importance, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a25314b164eac96dcc85f7db549b97689</anchor>
      <arglist>(int importance, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4f2618cc9575ad5f43e215fde67e69c0</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2cb4167faa85207ec0e179c9e5097e91</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4e95be08f6ea47c96f1f2e8bdef2015b</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8a0efa3588c971e561af60a65a41fa9e</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9330a55bb22f22d311833a551932656c</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae8c958aeab853fba3038f4bd5bf9fc81</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5dda4dcd410d10bec971620cf34f93b3</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1a695b2d511d8f7775dfa15f7b8e57e5</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3c844f958960e331495c283cbf43f22c</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a27583bf3dddf2720135bd1eb49f1ef46</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac0edb0a5b545da2ab83c90b264efdac1</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a383296dfe1521df229ae2bc69016828b</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4d49a08d21672b02724920981d675e04</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a048ce3e1e432cb5acb4f5ddf9b26dc47</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fcdc8fbf5e3fda46ca8aff37afcc6c6</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2b94aedf6297fac70f744037d009446d</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6d35e4d8afa07d9d942bf4f8bd38145</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa5a1c03c0d56424d5d252839528ba389</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa8ea6716b8cc9164f787f5aaa0f7e485</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a6814ee85d0a1a0bd790c9c20650ba5df</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abdd252bc0329ee51fc6dfc483bfc5744</anchor>
      <arglist>(int importance, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debugf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac4c4b41daa6003606723f00c947b36e9</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>infof</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fb91f93ae359d51837f3fce07811c5e</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warnf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a744ca6d0c0db19e9130274549879b417</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatalf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3a00b106156319003053b3640a74fe05</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2b23e481173716a109ac828a6c6a19</anchor>
      <arglist>(const LogRecord &amp;record)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a12e1e7a5f74109f507f00cd19603d9b1</anchor>
      <arglist>(std::ostream &amp;destination, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a006ebee8bee8a86bf8b195a42bf780ae</anchor>
      <arglist>(std::ostream &amp;destination, int threshold, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a65f7afb296ece8024126ead591a77feb</anchor>
      <arglist>(const boost::shared_ptr&lt; LogDestination &gt; &amp;destination)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getPreamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a07a141da270ee8156acb45737dae86b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a382e1c4fa45c443fe907ca3c5bd4e573</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a80e998f8b3054dc0f90cd9f87e532618</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8b0610ca21a7dcad81a953640844875e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Log &amp;</type>
      <name>getDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a14b8432754d9ee1a1af9ea092a158332</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aaa60fcad95f0ce7e9e01e4ec48575a5c</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>closeDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2725662306312839a60938864731d4e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>DEBUG</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acd7e6b84345ad03108095d7248116fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INFO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af3f69688ac95f40e2dd0b46ba01ea7de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>WARN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1444b33e122c3a2eb19a3e57922b69d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INHERIT_THRESHOLD</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5e7f680fbc09576a4db6c5df7d84632f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>FATAL</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a56997b47bc199dd91016f982485d0153</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af1d5655fa2b4cea7887a8856861f6fbb</anchor>
      <arglist>(int threshold, int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3b1b897af38e393d8f743aff1d6541c</anchor>
      <arglist>(int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static void</type>
      <name>setDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a78b8046c875e1e03aa3ab547d4578215</anchor>
      <arglist>(Log *deflog)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; threshold::Memory &gt;</type>
      <name>_thresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad206c417fb04942f48d2db6c72249285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt;</type>
      <name>_destinations</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a11acd5225a6963a56cac78e11588b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_preamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a105ba415a35949cec51fc6f316bb3714</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static Log *</type>
      <name>defaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad0847f994499165035e09e230390b72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static const std::string</type>
      <name>_sep</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a95a99c329e6ecee6821b80e2141cf311</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogRec</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_rec.html</filename>
    <base>lsst::pex::logging::LogRecord</base>
    <member kind="enumeration">
      <name>Manip</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a3ba4ed82252771f9714ae104fdd9cd3a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>endr</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a3ba4ed82252771f9714ae104fdd9cd3aacaaa55f9c8833d3771133dcffd77d45a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRec</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a63099332d815ff16b65cc398695c3af5</anchor>
      <arglist>(Log &amp;log, int importance)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRec</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a9a28cacb4fcb6bc3360fef1dd5e8a790</anchor>
      <arglist>(const LogRec &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogRec</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a4e6a26586c4e3b0ca2c57f284ba7c0a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a4ecba750a4f52eaf2a2e1d2132017ac4</anchor>
      <arglist>(const LogRec &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>aa73a0f1d3a6c79a45778eec182758f1f</anchor>
      <arglist>(const std::string &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>aae8f4ec517c2349876e7dfc24ca7e8dd</anchor>
      <arglist>(const boost::format &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a477dcc4c6df958fab497c7a807d14475</anchor>
      <arglist>(const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>a8f856ac37a503af20271923286cebc37</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>LogRec &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_rec.html</anchorfile>
      <anchor>aeab8ab9ab0cda70eb81ffc6d8a95f0b0</anchor>
      <arglist>(Manip signal)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5bf104ad419254b08c6daa8997f0c44c</anchor>
      <arglist>(int threshold, int importance, bool showAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a081cba93af347a56b5b91ad0926b8408</anchor>
      <arglist>(int threshold, int importance, const lsst::daf::base::PropertySet &amp;preamble, bool showAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ad7d0d4ba277f1ebdc67c4ff6777195f5</anchor>
      <arglist>(const LogRecord &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>abbbc0308b79f378f89fd61788af87572</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogRecord &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a83b68a58199fceca377bcdadd2a78738</anchor>
      <arglist>(const LogRecord &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addComment</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5169294fe3e1a40091eb84948033f96f</anchor>
      <arglist>(const std::string &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addComment</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>aa3f170b86ee93d132def6fd4655cd1cc</anchor>
      <arglist>(const boost::format &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a87c701b7bd3068bb05f9e6b70d63a583</anchor>
      <arglist>(const RecordProperty&lt; T &gt; &amp;property)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21dfca69db732d9724d640dbf0f23a4a</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5a9fc5f2023d335b6b8a9d8bcee8794f</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a424cdc601c84b2caff1f9280aa37e14d</anchor>
      <arglist>(const lsst::daf::base::PropertySet::Ptr &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a81e03a64a5118f15cb719e92176709c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>lsst::daf::base::PropertySet &amp;</type>
      <name>getProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a194e7c8ecaefe37268e3e38dd8197f31</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a382b9e5fe6216b4605d4f14938a9acc0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>lsst::daf::base::PropertySet &amp;</type>
      <name>data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a732ba73ef2cd00a865e283ded5c0cdcb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>countParamNames</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a3315c2a6ee0becd87c34e160fa04cd73</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>countParamValues</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a291a577450ab3263596a9baaa7ddc44f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getImportance</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a0d9d980ce0a51c47f5d1ae049b4ff60a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ad56ae7e359122bfb24c24d3412dcd3de</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21343627a5249e1d6eba1e5f747ac5d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a9bbb790b68461a9e478bfa18e0a8b148</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTimestamp</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ac8a0f3faddc9cdf4b08fa783c3fe7647</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setDate</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21fecec5d2da052963b12ccb84489ad7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static long long</type>
      <name>utcnow</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a7f276a819ef2009943e0ec6b5ad71938</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>af112a7e5626ea10d8389847c62137919</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_init</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a13c74c4cc3381291fa1dda094b416556</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a1d023b171725995b2a0b51b731d825c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_showAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>af13e56a2406efc362b92c7269b002d1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_vol</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a62ff7602680233c71bdd682f63d505cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ac195b00fe23c6b0c6316565efc21dc7b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogClient</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_client.html</filename>
    <member kind="function">
      <type></type>
      <name>LogClient</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>af749375547fa771ed57e539e29b1948f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>a4af9136332eb0b6636ad8ac8ed24f701</anchor>
      <arglist>(const Log &amp;log)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const Log &amp;</type>
      <name>getLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>afde7b75a0a8b289ab2ffe6adc530dc13</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual Log &amp;</type>
      <name>getLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>af95871573300e9bcb0cc8e69aea3f315</anchor>
      <arglist>()=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogClientHelper</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</filename>
    <base>lsst::pex::logging::LogClient</base>
    <member kind="function">
      <type></type>
      <name>LogClientHelper</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</anchorfile>
      <anchor>aeee8a00338aea3357d4fccf9fa16d614</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogClientHelper</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</anchorfile>
      <anchor>ad85408018ffe57396565e29a4a4434fb</anchor>
      <arglist>(const string &amp;childName)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogClientHelper</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</anchorfile>
      <anchor>a391dea7d64cb5651e3873046a80e18a9</anchor>
      <arglist>(const Log &amp;log)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogClientHelper</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</anchorfile>
      <anchor>a743964ac768b0ad2cb5c64204d0915f8</anchor>
      <arglist>(const Log &amp;log, const string &amp;childName)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogClient</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>af749375547fa771ed57e539e29b1948f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>a4af9136332eb0b6636ad8ac8ed24f701</anchor>
      <arglist>(const Log &amp;log)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const Log &amp;</type>
      <name>getLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>afde7b75a0a8b289ab2ffe6adc530dc13</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual Log &amp;</type>
      <name>getLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client.html</anchorfile>
      <anchor>af95871573300e9bcb0cc8e69aea3f315</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Log</type>
      <name>_log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_client_helper.html</anchorfile>
      <anchor>aab4eec6de4bf816b85bfd6bfa9236372</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogDestination</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_destination.html</filename>
    <member kind="function">
      <type></type>
      <name>LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>aa158d6bd46ae7f42a05a99286b4bf6cc</anchor>
      <arglist>(std::ostream *strm, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter, int threshold=threshold::PASS_ALL)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a4cd4101a59fb0b4f918da229cb06fb82</anchor>
      <arglist>(const LogDestination &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>ad400c8fef9a154e0dabd3d2490c9e907</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogDestination &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a1b813ac2adfdbcee2c1f652b834bbf58</anchor>
      <arglist>(const LogDestination &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a56c439ce63ba2d99559ae07d7df9744d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a96483ff0d5e4ee31644ee2e994a430d1</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a33b824e3a3df712280e0803f18f0c790</anchor>
      <arglist>(const LogRecord &amp;rec)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_threshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a6e9c7dc512cb2cae00eca8310d0ff504</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::ostream *</type>
      <name>_strm</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>aae36e993a3bd481aaee3f21db207bd1b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; LogFormatter &gt;</type>
      <name>_frmtr</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_destination.html</anchorfile>
      <anchor>a8c14d16d464f3bfa50afab0fd478120e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogFormatter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</filename>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a102cd779e0a856e143316cde547b290f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a522baea38c124155d0011dc55c917e87</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>acc272a20bd77a80f5defc3e5b95da95d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a99763b3f0699201b1a60ca373620f6b8</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>aa1f404b464d49bf9c354dbb70c3e45d1</anchor>
      <arglist>(std::ostream *strm, const LogRecord &amp;rec)=0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BriefFormatter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</filename>
    <base>lsst::pex::logging::LogFormatter</base>
    <member kind="function">
      <type></type>
      <name>BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a223f09a42031e4dc9c31ac996a02277e</anchor>
      <arglist>(bool verbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>ad358567903393d6f3def6c884cfb632c</anchor>
      <arglist>(const BriefFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a2d55cc7e7c77c79284637e6a41787a91</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>BriefFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a98f8c7dbda6e319a273719d512e393a7</anchor>
      <arglist>(const BriefFormatter &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>af7eaa10fc88218f5ec79257a2cf0c6e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>aef46ae1c2072c7b3751b7bba12cc946e</anchor>
      <arglist>(bool printAll)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a0f1ec21afdc9bb3ef11ad3343886cc2c</anchor>
      <arglist>(std::ostream *strm, const LogRecord &amp;rec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a102cd779e0a856e143316cde547b290f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a522baea38c124155d0011dc55c917e87</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>acc272a20bd77a80f5defc3e5b95da95d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a99763b3f0699201b1a60ca373620f6b8</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::IndentedFormatter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</filename>
    <base>lsst::pex::logging::BriefFormatter</base>
    <member kind="function">
      <type></type>
      <name>IndentedFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</anchorfile>
      <anchor>afb954ae6cc22525f0857895da838963d</anchor>
      <arglist>(bool verbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>IndentedFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</anchorfile>
      <anchor>a2995bbc72ceda2f3435691419d4426dd</anchor>
      <arglist>(const IndentedFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~IndentedFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</anchorfile>
      <anchor>aeefc310a5c7c7339b03a0ef0908a7f84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>IndentedFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</anchorfile>
      <anchor>ae5d27f5b0deb43fa713f2a64647ee794</anchor>
      <arglist>(const IndentedFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_indented_formatter.html</anchorfile>
      <anchor>ad77f2c3db45939fcd0522235566b1f15</anchor>
      <arglist>(std::ostream *strm, const LogRecord &amp;rec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a223f09a42031e4dc9c31ac996a02277e</anchor>
      <arglist>(bool verbose=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>ad358567903393d6f3def6c884cfb632c</anchor>
      <arglist>(const BriefFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BriefFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a2d55cc7e7c77c79284637e6a41787a91</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>BriefFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>a98f8c7dbda6e319a273719d512e393a7</anchor>
      <arglist>(const BriefFormatter &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>af7eaa10fc88218f5ec79257a2cf0c6e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_brief_formatter.html</anchorfile>
      <anchor>aef46ae1c2072c7b3751b7bba12cc946e</anchor>
      <arglist>(bool printAll)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a102cd779e0a856e143316cde547b290f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a522baea38c124155d0011dc55c917e87</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>acc272a20bd77a80f5defc3e5b95da95d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a99763b3f0699201b1a60ca373620f6b8</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::NetLoggerFormatter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</filename>
    <base>lsst::pex::logging::LogFormatter</base>
    <member kind="function">
      <type></type>
      <name>NetLoggerFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>a3deeeac4a7d4a4b2b754d3849403ef5a</anchor>
      <arglist>(const std::string &amp;valueDelim=defaultValDelim)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NetLoggerFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>adb38b1d44bf31ab542c377a701cbec42</anchor>
      <arglist>(const NetLoggerFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~NetLoggerFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>a272017f26a2d0acb19063e2dee22f714</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>NetLoggerFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>a55a2657a3e331c8850085bd6fa6befeb</anchor>
      <arglist>(const NetLoggerFormatter &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getValueDelimiter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>ac5f3cab9d28bc79b0304f23ba06349f6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>a3fb55e5062a2ba68d31ab973fbc9b78a</anchor>
      <arglist>(std::ostream *strm, const LogRecord &amp;rec)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a102cd779e0a856e143316cde547b290f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a522baea38c124155d0011dc55c917e87</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogFormatter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>acc272a20bd77a80f5defc3e5b95da95d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogFormatter &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_formatter.html</anchorfile>
      <anchor>a99763b3f0699201b1a60ca373620f6b8</anchor>
      <arglist>(const LogFormatter &amp;that)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>defaultValDelim</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_net_logger_formatter.html</anchorfile>
      <anchor>ac2dbe8db22a04fe7917a1bba490a1d1c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::RecordProperty</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_record_property.html</filename>
    <templarg>T</templarg>
    <member kind="typedef">
      <type>T</type>
      <name>ValueType</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a0a7eb6220ac6788b714a6b4f9a33d629</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RecordProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a929f7478b20bd4b55a18d661a4cf4077</anchor>
      <arglist>(const std::string &amp;pname, const T &amp;pvalue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addTo</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a2ffd3892f9b5f55e61cfcd688aba23a1</anchor>
      <arglist>(lsst::daf::base::PropertySet &amp;set)</arglist>
    </member>
    <member kind="variable">
      <type>const std::string</type>
      <name>name</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>ae0974cca2b4ceae7f0b8d34090510fe1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const T &amp;</type>
      <name>value</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>aaa15eaacf5073b270504db30fd9109a9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::Prop</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_prop.html</filename>
    <templarg></templarg>
    <base>lsst::pex::logging::RecordProperty</base>
    <member kind="function">
      <type></type>
      <name>Prop</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_prop.html</anchorfile>
      <anchor>aecb3c1d4ebed7038239626d92e57dc9b</anchor>
      <arglist>(const std::string &amp;pname, const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>RecordProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a929f7478b20bd4b55a18d661a4cf4077</anchor>
      <arglist>(const std::string &amp;pname, const T &amp;pvalue)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addTo</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a2ffd3892f9b5f55e61cfcd688aba23a1</anchor>
      <arglist>(lsst::daf::base::PropertySet &amp;set)</arglist>
    </member>
    <member kind="typedef">
      <type>T</type>
      <name>ValueType</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>a0a7eb6220ac6788b714a6b4f9a33d629</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const std::string</type>
      <name>name</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>ae0974cca2b4ceae7f0b8d34090510fe1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const T &amp;</type>
      <name>value</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_record_property.html</anchorfile>
      <anchor>aaa15eaacf5073b270504db30fd9109a9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::LogRecord</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_log_record.html</filename>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5bf104ad419254b08c6daa8997f0c44c</anchor>
      <arglist>(int threshold, int importance, bool showAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a081cba93af347a56b5b91ad0926b8408</anchor>
      <arglist>(int threshold, int importance, const lsst::daf::base::PropertySet &amp;preamble, bool showAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ad7d0d4ba277f1ebdc67c4ff6777195f5</anchor>
      <arglist>(const LogRecord &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>abbbc0308b79f378f89fd61788af87572</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>LogRecord &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a83b68a58199fceca377bcdadd2a78738</anchor>
      <arglist>(const LogRecord &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addComment</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5169294fe3e1a40091eb84948033f96f</anchor>
      <arglist>(const std::string &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addComment</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>aa3f170b86ee93d132def6fd4655cd1cc</anchor>
      <arglist>(const boost::format &amp;comment)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a87c701b7bd3068bb05f9e6b70d63a583</anchor>
      <arglist>(const RecordProperty&lt; T &gt; &amp;property)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21dfca69db732d9724d640dbf0f23a4a</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a5a9fc5f2023d335b6b8a9d8bcee8794f</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a424cdc601c84b2caff1f9280aa37e14d</anchor>
      <arglist>(const lsst::daf::base::PropertySet::Ptr &amp;props)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a81e03a64a5118f15cb719e92176709c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>lsst::daf::base::PropertySet &amp;</type>
      <name>getProperties</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a194e7c8ecaefe37268e3e38dd8197f31</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a382b9e5fe6216b4605d4f14938a9acc0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>lsst::daf::base::PropertySet &amp;</type>
      <name>data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a732ba73ef2cd00a865e283ded5c0cdcb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>countParamNames</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a3315c2a6ee0becd87c34e160fa04cd73</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>countParamValues</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a291a577450ab3263596a9baaa7ddc44f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getImportance</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a0d9d980ce0a51c47f5d1ae049b4ff60a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ad56ae7e359122bfb24c24d3412dcd3de</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21343627a5249e1d6eba1e5f747ac5d9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a9bbb790b68461a9e478bfa18e0a8b148</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTimestamp</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ac8a0f3faddc9cdf4b08fa783c3fe7647</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setDate</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a21fecec5d2da052963b12ccb84489ad7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static long long</type>
      <name>utcnow</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a7f276a819ef2009943e0ec6b5ad71938</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>LogRecord</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>af112a7e5626ea10d8389847c62137919</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_init</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a13c74c4cc3381291fa1dda094b416556</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a1d023b171725995b2a0b51b731d825c2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_showAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>af13e56a2406efc362b92c7269b002d1f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>int</type>
      <name>_vol</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>a62ff7602680233c71bdd682f63d505cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_data</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log_record.html</anchorfile>
      <anchor>ac195b00fe23c6b0c6316565efc21dc7b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::PrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</filename>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>ab3e965fd6ba54378a3e69cfa2c8508bf</anchor>
      <arglist>(std::ostream *strm) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a6f2c8aa86e13e15db14637482df007c2</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a58daa4aaced025a997ea9dec92155566</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>aef64aa5abbb420479fbab499edaa6b53</anchor>
      <arglist>(const PrinterIter &amp;that) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a7da7d817585011e237c7366e20df96a0</anchor>
      <arglist>(const PrinterIter &amp;that) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>aaa9a2ebbc1f7214b9ad3f0bd73aeb73f</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>acc0bb7971a458d23123ce8888b1ddf39</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BaseTmplPrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</filename>
    <templarg>T</templarg>
    <base>lsst::pex::logging::PrinterIter</base>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a375fd42553220c3a0c2f1e838bf396ea</anchor>
      <arglist>(typename std::vector&lt; T &gt;::const_iterator listiter, typename std::vector&lt; T &gt;::const_iterator beginiter, typename std::vector&lt; T &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a1461d00f68dd4f55860763dafa5478a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ab5938986fdb0b7d8c26506a4a20a122d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7956291906c41c17185e514caa02ca45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a79ee6e6981fda703329ea58cd64e156b</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7a65027c9d8779b56407dd13b7dcef8c</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>aa4da717ef0b1896a4672cb05eedc006c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a60a1e9696400eec36244a36299efc82a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>adfbbf0b7162d667d7a09bb54b180ef9f</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a54449244f44047958b94e7865b1c12ae</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>ab3e965fd6ba54378a3e69cfa2c8508bf</anchor>
      <arglist>(std::ostream *strm) const =0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_it</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ad9879ebc2f7e0f02f4061d1c6b76dc69</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>abc5ceb86d34afa030a9d2b87198b032d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_end</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a24620042009eb7be95bb5f039c49119c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::TmplPrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_iter.html</filename>
    <templarg></templarg>
    <base>lsst::pex::logging::BaseTmplPrinterIter</base>
    <member kind="function">
      <type></type>
      <name>TmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_iter.html</anchorfile>
      <anchor>a128256cde130c25618d5c816ce3cd108</anchor>
      <arglist>(typename std::vector&lt; T &gt;::const_iterator listiter, typename std::vector&lt; T &gt;::const_iterator beginiter, typename std::vector&lt; T &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_iter.html</anchorfile>
      <anchor>a899294d4608fc9ac10219a5ce6b2b920</anchor>
      <arglist>(std::ostream *strm) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a375fd42553220c3a0c2f1e838bf396ea</anchor>
      <arglist>(typename std::vector&lt; T &gt;::const_iterator listiter, typename std::vector&lt; T &gt;::const_iterator beginiter, typename std::vector&lt; T &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a1461d00f68dd4f55860763dafa5478a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ab5938986fdb0b7d8c26506a4a20a122d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7956291906c41c17185e514caa02ca45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a79ee6e6981fda703329ea58cd64e156b</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7a65027c9d8779b56407dd13b7dcef8c</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>aa4da717ef0b1896a4672cb05eedc006c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a60a1e9696400eec36244a36299efc82a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>adfbbf0b7162d667d7a09bb54b180ef9f</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a54449244f44047958b94e7865b1c12ae</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_it</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ad9879ebc2f7e0f02f4061d1c6b76dc69</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>abc5ceb86d34afa030a9d2b87198b032d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;::const_iterator</type>
      <name>_end</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a24620042009eb7be95bb5f039c49119c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::WrappedPrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</filename>
    <base>lsst::pex::logging::PrinterIter</base>
    <member kind="function">
      <type></type>
      <name>WrappedPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>aaf5937d8acb33c5eb19988317723e481</anchor>
      <arglist>(boost::shared_ptr&lt; PrinterIter &gt; iter)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~WrappedPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>a41ab7c26aa7e7f1ede1a42d713a8671e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>aad006c8a1613d6aa4ddd6010003b9d7c</anchor>
      <arglist>(std::ostream *strm) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>aa7d4c40e4d8cce8796e1b996d35c7ef3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>a05e510dce09fc189464b93f85722d423</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>a920e5ada8b45fd5ec015779517a139e1</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>ac3c4000581eaa6f7eb82f98ada57a62f</anchor>
      <arglist>(const PrinterIter &amp;that) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>ac65825ea1b2ec3da08d1d97c5f2951da</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_wrapped_printer_iter.html</anchorfile>
      <anchor>ad07821dc5b5713ae08cc0a71936f5a28</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::PrinterList</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_printer_list.html</filename>
    <member kind="typedef">
      <type>WrappedPrinterIter</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a5c80d8b3c3dd3e9c56d8c0b203cab076</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a911a30edbee2e189a0c7698501a61692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a8f2fcdac0a982a326c30f6749a48dfe7</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a10f01b2af90d273d2db8a3684ebfc2e1</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>aac2a6ac3438f70d2c129e295e2148d0e</anchor>
      <arglist>() const =0</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BaseTmplPrinterList</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</filename>
    <templarg>T</templarg>
    <base>lsst::pex::logging::PrinterList</base>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a4629e2d065500e6b732d7fd5feb50ae7</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a62fabc94a29447bfc8f21838ec65ea86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a1fb0cb5aedfdda1ec14bd6560bd9ecb4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a911a30edbee2e189a0c7698501a61692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a8f2fcdac0a982a326c30f6749a48dfe7</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a10f01b2af90d273d2db8a3684ebfc2e1</anchor>
      <arglist>() const =0</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;</type>
      <name>_list</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a9fa711ddcf4414fa91146a256c8c42ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>WrappedPrinterIter</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a5c80d8b3c3dd3e9c56d8c0b203cab076</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::TmplPrinterList</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_list.html</filename>
    <templarg>T</templarg>
    <base>lsst::pex::logging::BaseTmplPrinterList</base>
    <member kind="function">
      <type></type>
      <name>TmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_list.html</anchorfile>
      <anchor>a28580c98399b736bf220702810b05074</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterList::iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_list.html</anchorfile>
      <anchor>a8587074b229c7e1c0efc13cd4d10f449</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterList::iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_tmpl_printer_list.html</anchorfile>
      <anchor>af638af2aa76e5036cee0a56c17eb7f73</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a4629e2d065500e6b732d7fd5feb50ae7</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a62fabc94a29447bfc8f21838ec65ea86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a1fb0cb5aedfdda1ec14bd6560bd9ecb4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a911a30edbee2e189a0c7698501a61692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="typedef">
      <type>WrappedPrinterIter</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a5c80d8b3c3dd3e9c56d8c0b203cab076</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; T &gt;</type>
      <name>_list</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a9fa711ddcf4414fa91146a256c8c42ef</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::DateTimePrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_date_time_printer_iter.html</filename>
    <base>BaseTmplPrinterIter&lt; lsst::daf::base::DateTime &gt;</base>
    <member kind="function">
      <type></type>
      <name>DateTimePrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_iter.html</anchorfile>
      <anchor>a485cfc9363a2076d2bfcdf8a75cf67de</anchor>
      <arglist>(std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator listiter, std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator beginiter, std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DateTimePrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_iter.html</anchorfile>
      <anchor>ac09cec13ef6f94d57c5c49540ec4bb92</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_iter.html</anchorfile>
      <anchor>ac4b1c3f4d882384c8dd97fd2422c86e1</anchor>
      <arglist>(std::ostream *strm) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a375fd42553220c3a0c2f1e838bf396ea</anchor>
      <arglist>(typename std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator listiter, typename std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator beginiter, typename std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a1461d00f68dd4f55860763dafa5478a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ab5938986fdb0b7d8c26506a4a20a122d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7956291906c41c17185e514caa02ca45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a79ee6e6981fda703329ea58cd64e156b</anchor>
      <arglist>(const PrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>adfbbf0b7162d667d7a09bb54b180ef9f</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7a65027c9d8779b56407dd13b7dcef8c</anchor>
      <arglist>(const PrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a54449244f44047958b94e7865b1c12ae</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>aa4da717ef0b1896a4672cb05eedc006c</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a60a1e9696400eec36244a36299efc82a</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator</type>
      <name>_it</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ad9879ebc2f7e0f02f4061d1c6b76dc69</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator</type>
      <name>_begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>abc5ceb86d34afa030a9d2b87198b032d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; lsst::daf::base::DateTime &gt;::const_iterator</type>
      <name>_end</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a24620042009eb7be95bb5f039c49119c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::DateTimePrinterList</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_date_time_printer_list.html</filename>
    <base>BaseTmplPrinterList&lt; lsst::daf::base::DateTime &gt;</base>
    <member kind="function">
      <type></type>
      <name>DateTimePrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_list.html</anchorfile>
      <anchor>a926b5f38891a1d036f2f83df995edfea</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DateTimePrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_list.html</anchorfile>
      <anchor>a8962d719daa8c55f9be51f57154a975c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_list.html</anchorfile>
      <anchor>acc1d56ef6ad369e1cfffa7190af579d2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_date_time_printer_list.html</anchorfile>
      <anchor>a436c031aebb0f9a4c63412df29530af8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a4629e2d065500e6b732d7fd5feb50ae7</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a62fabc94a29447bfc8f21838ec65ea86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a1fb0cb5aedfdda1ec14bd6560bd9ecb4</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a911a30edbee2e189a0c7698501a61692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="typedef">
      <type>WrappedPrinterIter</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a5c80d8b3c3dd3e9c56d8c0b203cab076</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; lsst::daf::base::DateTime &gt;</type>
      <name>_list</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a9fa711ddcf4414fa91146a256c8c42ef</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BoolPrinterIter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_bool_printer_iter.html</filename>
    <base>BaseTmplPrinterIter&lt; bool &gt;</base>
    <member kind="function">
      <type></type>
      <name>BoolPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_iter.html</anchorfile>
      <anchor>ab5b150337c4c4b17bbf03ed7e7d30d60</anchor>
      <arglist>(std::vector&lt; bool &gt;::const_iterator listiter, std::vector&lt; bool &gt;::const_iterator beginiter, std::vector&lt; bool &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BoolPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_iter.html</anchorfile>
      <anchor>a05dd98956f32d2006cb9f36ae6597eab</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_iter.html</anchorfile>
      <anchor>a537e46611a584df134a5d87d2b6106fe</anchor>
      <arglist>(std::ostream *strm) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a375fd42553220c3a0c2f1e838bf396ea</anchor>
      <arglist>(typename std::vector&lt; bool &gt;::const_iterator listiter, typename std::vector&lt; bool &gt;::const_iterator beginiter, typename std::vector&lt; bool &gt;::const_iterator enditer)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a1461d00f68dd4f55860763dafa5478a5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator++</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ab5938986fdb0b7d8c26506a4a20a122d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PrinterIter &amp;</type>
      <name>operator--</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7956291906c41c17185e514caa02ca45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a79ee6e6981fda703329ea58cd64e156b</anchor>
      <arglist>(const PrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator==</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>adfbbf0b7162d667d7a09bb54b180ef9f</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a7a65027c9d8779b56407dd13b7dcef8c</anchor>
      <arglist>(const PrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>operator!=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a54449244f44047958b94e7865b1c12ae</anchor>
      <arglist>(const BaseTmplPrinterIter &amp;that) const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notAtEnd</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>aa4da717ef0b1896a4672cb05eedc006c</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>notLTBegin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a60a1e9696400eec36244a36299efc82a</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual</type>
      <name>~PrinterIter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a217b9a4970966417982b9e489a36a095</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>operator*</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_iter.html</anchorfile>
      <anchor>a1b55accb2178f21ab99cdeea205a6fa6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; bool &gt;::const_iterator</type>
      <name>_it</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>ad9879ebc2f7e0f02f4061d1c6b76dc69</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; bool &gt;::const_iterator</type>
      <name>_begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>abc5ceb86d34afa030a9d2b87198b032d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; bool &gt;::const_iterator</type>
      <name>_end</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_iter.html</anchorfile>
      <anchor>a24620042009eb7be95bb5f039c49119c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::BoolPrinterList</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_bool_printer_list.html</filename>
    <base>BaseTmplPrinterList&lt; bool &gt;</base>
    <member kind="function">
      <type></type>
      <name>BoolPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_list.html</anchorfile>
      <anchor>aaba8f53bf0cfc44682b00f99d753792b</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BoolPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_list.html</anchorfile>
      <anchor>af7282e45570468b60adcd9687409edd6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_list.html</anchorfile>
      <anchor>a8034d5eb773b5e68e5fe5f508487b1b7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_bool_printer_list.html</anchorfile>
      <anchor>a3dbb526cd1b138283ea1a5c56ba2cf35</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a4629e2d065500e6b732d7fd5feb50ae7</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BaseTmplPrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a62fabc94a29447bfc8f21838ec65ea86</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a1fb0cb5aedfdda1ec14bd6560bd9ecb4</anchor>
      <arglist>() const</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PrinterList</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a911a30edbee2e189a0c7698501a61692</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="typedef">
      <type>WrappedPrinterIter</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_list.html</anchorfile>
      <anchor>a5c80d8b3c3dd3e9c56d8c0b203cab076</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::vector&lt; bool &gt;</type>
      <name>_list</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_base_tmpl_printer_list.html</anchorfile>
      <anchor>a9fa711ddcf4414fa91146a256c8c42ef</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::PrinterFactory</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_printer_factory.html</filename>
    <member kind="typedef">
      <type>PrinterList *(*</type>
      <name>factoryFuncPtr</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_factory.html</anchorfile>
      <anchor>a2caff25efac9de25248199da1a836561</anchor>
      <arglist>)(const lsst::daf::base::PropertySet &amp;, const std::string &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PrinterFactory</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_factory.html</anchorfile>
      <anchor>acc91f465bcc2f208c71070b221531000</anchor>
      <arglist>(bool loadDefaults=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_factory.html</anchorfile>
      <anchor>a8025120b52624d9b86b1da8dc2db59ee</anchor>
      <arglist>(const std::type_info &amp;tp, factoryFuncPtr func)</arglist>
    </member>
    <member kind="function">
      <type>PrinterList *</type>
      <name>makePrinter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_printer_factory.html</anchorfile>
      <anchor>a0bfae70be4647a3be3b6cee26d24c9d8</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::PropertyPrinter</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_property_printer.html</filename>
    <member kind="typedef">
      <type>PrinterList::iterator</type>
      <name>iterator</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>a2700caa929b2393cadf90db332af6926</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PropertyPrinter</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>afb8a9d00b336a7ebca293fc2df373650</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;prop, const std::string &amp;name, const PrinterFactory &amp;fact=defaultPrinterFactory)</arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>a7c3384171a9fa2d63d45d56a72f7a095</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>iterator</type>
      <name>last</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>a1b5f759a1638bf5c853e17622dd96646</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>a8cfb36b545d10eeb6b36aa7f0f0b9d10</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static PrinterFactory</type>
      <name>defaultPrinterFactory</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_property_printer.html</anchorfile>
      <anchor>a3e6028b3656e4abce86d73fd871453d2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::ScreenLog</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_screen_log.html</filename>
    <base>lsst::pex::logging::Log</base>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>ac0f8d075ad5c7462640813597cd58e05</anchor>
      <arglist>(bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a0b305695a70c376f150e88fb44dcf0ad</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>adea20f8a24b510a1b4166df5245dabda</anchor>
      <arglist>(const ScreenLog &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ScreenLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a01479c8d8b1c73db9372c401e5a28538</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ScreenLog &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a8afa41cad84fae5dd189a36339d4d7db</anchor>
      <arglist>(const ScreenLog &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getScreenThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>af79517d833d5bbe4c247851647711030</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>aeb13d6fedb0f2bf9ac845951b45c3dc8</anchor>
      <arglist>(int thresh)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setScreenVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a136124f09c4daf1db7da7a637731885d</anchor>
      <arglist>(bool printAll)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isScreenVerbose</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a8e19db8be9c05832e5f515be0b57ccf3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5ea246bd93a1f57b8a0bfc796ca102db</anchor>
      <arglist>(const int threshold=INFO, const std::string &amp;name=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abc7bc74e560cd042fa025db31e7d3c83</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO, bool defaultShowAll=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af49865ef88f9c3f6c6f8e1eff3db72d8</anchor>
      <arglist>(const Log &amp;parent, const std::string &amp;childName, int threshold=INHERIT_THRESHOLD)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aba7f3c6f2461b131118d30863d45b230</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5a2b689e4ef4f473bbe33a1b393283a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Log &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a93f8685b2415aa3d8aaa3fc16dfc3a9b</anchor>
      <arglist>(const Log &amp;that)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getName</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a53b2beea1c6b23045a394f2adc3515</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9a0b35030b1193fc57729f1ed1f23322</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af70a59bbe37df3b6cbea6c61347db864</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>sends</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2cb13a039c37f55534833ab6f8a208</anchor>
      <arglist>(int importance) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a445b2518f5c5b2783fe2e6926de66e0c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a889cff19f967c20e7b0ed84d8e420a2b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8d5502590612259ced05d1a7bfa28a40</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>willShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a59b7fe237c46de30076d88855f2a1f6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4db8595c790fcd491f60c3c5032ee166</anchor>
      <arglist>(bool yesno)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetShowAll</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>afe1c0a37616d30b9a15e29562632d1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1c369d283ea7dc0335b54e064019da5d</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setPreambleProperty</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a226fa5caead5075f4ea252f3fa23b442</anchor>
      <arglist>(const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>Log *</type>
      <name>createChildLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3032a9cc687af9dc962d43a35acfd82</anchor>
      <arglist>(const std::string &amp;childName, int threshold=INHERIT_THRESHOLD) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6e7aca8a63be0147180844a4251441b</anchor>
      <arglist>(int importance, const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af5e82e65d8b7d5fd8ebac286e7895053</anchor>
      <arglist>(int importance, const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acb2e325ea37fb993ef0c530d86bbba7a</anchor>
      <arglist>(int importance, const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acc0539b7de3738ea01ad78fdf67d0abb</anchor>
      <arglist>(int importance, const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>log</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a25314b164eac96dcc85f7db549b97689</anchor>
      <arglist>(int importance, const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4f2618cc9575ad5f43e215fde67e69c0</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2cb4167faa85207ec0e179c9e5097e91</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4e95be08f6ea47c96f1f2e8bdef2015b</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8a0efa3588c971e561af60a65a41fa9e</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>logdebug</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a9330a55bb22f22d311833a551932656c</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae8c958aeab853fba3038f4bd5bf9fc81</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5dda4dcd410d10bec971620cf34f93b3</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1a695b2d511d8f7775dfa15f7b8e57e5</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3c844f958960e331495c283cbf43f22c</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>info</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a27583bf3dddf2720135bd1eb49f1ef46</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac0edb0a5b545da2ab83c90b264efdac1</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a383296dfe1521df229ae2bc69016828b</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a4d49a08d21672b02724920981d675e04</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a048ce3e1e432cb5acb4f5ddf9b26dc47</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warn</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fcdc8fbf5e3fda46ca8aff37afcc6c6</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2b94aedf6297fac70f744037d009446d</anchor>
      <arglist>(const std::string &amp;message, const lsst::daf::base::PropertySet &amp;properties)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ab6d35e4d8afa07d9d942bf4f8bd38145</anchor>
      <arglist>(const std::string &amp;message, const std::string &amp;name, const T &amp;val)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa5a1c03c0d56424d5d252839528ba389</anchor>
      <arglist>(const std::string &amp;message, const RecordProperty&lt; T &gt; &amp;prop)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aa8ea6716b8cc9164f787f5aaa0f7e485</anchor>
      <arglist>(const std::string &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatal</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a6814ee85d0a1a0bd790c9c20650ba5df</anchor>
      <arglist>(const boost::format &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>abdd252bc0329ee51fc6dfc483bfc5744</anchor>
      <arglist>(int importance, const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>debugf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ac4c4b41daa6003606723f00c947b36e9</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>infof</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3fb91f93ae359d51837f3fce07811c5e</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>warnf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a744ca6d0c0db19e9130274549879b417</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fatalf</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a3a00b106156319003053b3640a74fe05</anchor>
      <arglist>(const char *fmt,...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ade2b23e481173716a109ac828a6c6a19</anchor>
      <arglist>(const LogRecord &amp;record)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a12e1e7a5f74109f507f00cd19603d9b1</anchor>
      <arglist>(std::ostream &amp;destination, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a006ebee8bee8a86bf8b195a42bf780ae</anchor>
      <arglist>(std::ostream &amp;destination, int threshold, const boost::shared_ptr&lt; LogFormatter &gt; &amp;formatter)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a65f7afb296ece8024126ead591a77feb</anchor>
      <arglist>(const boost::shared_ptr&lt; LogDestination &gt; &amp;destination)</arglist>
    </member>
    <member kind="function">
      <type>const lsst::daf::base::PropertySet &amp;</type>
      <name>getPreamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a07a141da270ee8156acb45737dae86b1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a382e1c4fa45c443fe907ca3c5bd4e573</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a80e998f8b3054dc0f90cd9f87e532618</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a8b0610ca21a7dcad81a953640844875e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a6f32de0e377f62bb323fca0087504eeb</anchor>
      <arglist>(bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_screen_log.html</anchorfile>
      <anchor>a37ad42356831b38a1a8bcad193fe2a46</anchor>
      <arglist>(const lsst::daf::base::PropertySet &amp;preamble, bool verbose=false, int threshold=Log::INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Log &amp;</type>
      <name>getDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a14b8432754d9ee1a1af9ea092a158332</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>createDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>aaa60fcad95f0ce7e9e01e4ec48575a5c</anchor>
      <arglist>(const std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt; &amp;destinations, const lsst::daf::base::PropertySet &amp;preamble, const std::string &amp;name=&quot;&quot;, const int threshold=INFO)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>closeDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a2725662306312839a60938864731d4e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>DEBUG</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>acd7e6b84345ad03108095d7248116fa5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INFO</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af3f69688ac95f40e2dd0b46ba01ea7de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>WARN</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a1444b33e122c3a2eb19a3e57922b69d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>INHERIT_THRESHOLD</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a5e7f680fbc09576a4db6c5df7d84632f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const int</type>
      <name>FATAL</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a56997b47bc199dd91016f982485d0153</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_send</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>af1d5655fa2b4cea7887a8856861f6fbb</anchor>
      <arglist>(int threshold, int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ae3b1b897af38e393d8f743aff1d6541c</anchor>
      <arglist>(int importance, const char *fmt, va_list ap)</arglist>
    </member>
    <member kind="function" protection="protected" static="yes">
      <type>static void</type>
      <name>setDefaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a78b8046c875e1e03aa3ab547d4578215</anchor>
      <arglist>(Log *deflog)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::shared_ptr&lt; threshold::Memory &gt;</type>
      <name>_thresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad206c417fb04942f48d2db6c72249285</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::list&lt; boost::shared_ptr&lt; LogDestination &gt; &gt;</type>
      <name>_destinations</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a11acd5225a6963a56cac78e11588b067</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>_preamble</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a105ba415a35949cec51fc6f316bb3714</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static Log *</type>
      <name>defaultLog</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>ad0847f994499165035e09e230390b72d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected" static="yes">
      <type>static const std::string</type>
      <name>_sep</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_log.html</anchorfile>
      <anchor>a95a99c329e6ecee6821b80e2141cf311</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::Trace</name>
    <filename>classlsst_1_1pex_1_1logging_1_1_trace.html</filename>
    <member kind="function">
      <type></type>
      <name>Trace</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a8dc2922988ffd889fbda5bcd1f97700a</anchor>
      <arglist>(const std::string &amp;name, const int verbosity, const std::string &amp;msg,...)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Trace</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>ad80b01a13c0a0adf071a450512418562</anchor>
      <arglist>(const std::string &amp;name, const int verbosity, const boost::format &amp;msg)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setDestination</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a3f72b554d615f3d346d7a5c56ebfd15a</anchor>
      <arglist>(std::ostream &amp;)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a3b984672778c6c06b9777ebaa9a760f0</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a875efe7662568f19da03fc9354cba890</anchor>
      <arglist>(const std::string &amp;name, const int verbosity)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>getVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a81f0c38937bdecfd09c5c3ae5e4f6d68</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>printVerbosity</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>a84b1d178df1176a90920190abb950174</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1_trace.html</anchorfile>
      <anchor>ac7799af61eaf5c1aa56b03dfdb7e5b94</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::logging::threshold</name>
    <filename>namespacelsst_1_1pex_1_1logging_1_1threshold.html</filename>
    <class kind="class">lsst::pex::logging::threshold::Family</class>
    <class kind="class">lsst::pex::logging::threshold::Memory</class>
    <member kind="typedef">
      <type>boost::tokenizer&lt; boost::char_separator&lt; char &gt; &gt;</type>
      <name>tokenizer</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a24bfa7847e251e7a0c22c54794450b2b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INHERIT</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a09d4aa93a28d4a4f7793c25c9daedc4aa92926abec3b2a9ae58ec4cf8e41f0e01</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>PASS_ALL</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1threshold.html</anchorfile>
      <anchor>a507c7a5ccb6060295c5131d5d6b614a2aa6531362f3b98bc34c64562034372a9e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::threshold::Family</name>
    <filename>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</filename>
    <member kind="function">
      <type></type>
      <name>Family</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>adc7de9c5d0463803709f441c478d46a3</anchor>
      <arglist>(int defaultThreshold=INHERIT)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Family</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a4279fe323985dfd66726cdb412fa9fdb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a8c4f63218e06194c99381d3a34730650</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>ad5140782218e30f4fcaa073a04eded8f</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a644310c50a2d6bfc331c43b0f6d1642d</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a0c6124600b0f13a3acfb2398c92c8397</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a6b8beab12f71b84c3dd41e52afcc013a</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printDescThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a43357cab7da18c4d9cc967775bf9230c</anchor>
      <arglist>(std::ostream &amp;out, const std::string &amp;prefix) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>deleteDescendants</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>ad35a653f7cdf59f3b46c115c2520614f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>Family *</type>
      <name>ensureDescendant</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>abfba5586479e710b0d9372fd2ec7263f</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>const Family *</type>
      <name>findDescendant</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a631a7fefc27ffd1404547bd33852c780</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end, bool orNearest=false) const </arglist>
    </member>
    <member kind="function" protection="protected">
      <type>Family *</type>
      <name>makeDescendants</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_family.html</anchorfile>
      <anchor>a5e77237f5ec6df3b7554824e21ceaeac</anchor>
      <arglist>(tokenizer::iterator toptoken, const tokenizer::iterator &amp;end)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::logging::threshold::Memory</name>
    <filename>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</filename>
    <member kind="function">
      <type></type>
      <name>Memory</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>ac27ebe287b05c3848edc68f42ca440f1</anchor>
      <arglist>(const std::string &amp;delims=&quot;.&quot;)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>a278d0827f6640a60cca3f94f458657fe</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setThresholdFor</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>a802dd0fe485118e0f3a788532971280b</anchor>
      <arglist>(const std::string &amp;name, int threshold)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getRootThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>a85f2372c83370b8755cd70f4ac82a73a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setRootThreshold</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>aac9952e5108ca7a885e7f037bba79e55</anchor>
      <arglist>(int threshold)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>forgetAllNames</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>a8430e0b920baa36e82b268a3277cb2a0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>printThresholds</name>
      <anchorfile>classlsst_1_1pex_1_1logging_1_1threshold_1_1_memory.html</anchorfile>
      <anchor>abcd1c1b8313c12b4e933006e3ce05718</anchor>
      <arglist>(std::ostream &amp;out)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::logging::version</name>
    <filename>namespacelsst_1_1pex_1_1logging_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a0ff25a71e192432712cd06800ea5fa15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a782657e44eb6894737bc7d94162a24ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a8b12ff26d0777768f9b3b8db8d6c066f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a84893280b661c0bddd9db7874478f7ec</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>ae000dea61435b3b6af5644d2242036b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a6196f068fc274f5cfa84779937e3656f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1logging_1_1version.html</anchorfile>
      <anchor>a93838034009c015e7b3d80689e3fbe08</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex/logging</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/pex/logging/</path>
    <filename>dir_f37416aa9cb3033d7d4fcca954c334bb.html</filename>
    <file>__init__.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex/logging</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/</path>
    <filename>dir_e974611b7236deb06bd08c858b907547.html</filename>
    <dir>include/lsst/pex/logging/threshold</dir>
    <file>BlockTimingLog.h</file>
    <file>Component.h</file>
    <file>Debug.h</file>
    <file>DualLog.h</file>
    <file>FileDestination.h</file>
    <file>Log.h</file>
    <file>LogClient.h</file>
    <file>LogDestination.h</file>
    <file>LogFormatter.h</file>
    <file>LogRecord.h</file>
    <file>PropertyPrinter.h</file>
    <file>ScreenLog.h</file>
    <file>Trace.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/pex</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/pex</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/lsst/pex/</path>
    <filename>dir_513028b95738c600a2c7d67ec6cba6e9.html</filename>
    <dir>python/lsst/pex/logging</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/</path>
    <filename>dir_da59fd778d3d02495d7b88077ed25f4c.html</filename>
    <dir>include/lsst/pex/logging</dir>
    <file>logging.h</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <dir>src/threshold</dir>
    <file>BlockTimingLog.cc</file>
    <file>Component.cc</file>
    <file>DualLog.cc</file>
    <file>FileDestination.cc</file>
    <file>Log.cc</file>
    <file>LogDestination.cc</file>
    <file>LogFormatter.cc</file>
    <file>LogRecord.cc</file>
    <file>PropertyPrinter.cc</file>
    <file>ScreenLog.cc</file>
  </compound>
  <compound kind="dir">
    <name>src/threshold</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/src/threshold/</path>
    <filename>dir_49b4607306081ef213c970820981a077.html</filename>
    <file>Memory.cc</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex/logging/threshold</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_logging-6.2.1.0+4/pex_logging-6.2.1.0/include/lsst/pex/logging/threshold/</path>
    <filename>dir_7a62f7bdb394b4ccf100e035ad75afe8.html</filename>
    <file>enum.h</file>
    <file>Memory.h</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>lsst::pex::logging:  writing messages to logs</title>
    <filename>index</filename>
    <docanchor file="index">secLogIntro</docanchor>
    <docanchor file="index">secLogFw</docanchor>
    <docanchor file="index">secLogHier</docanchor>
    <docanchor file="index">secLogRec</docanchor>
    <docanchor file="index">secLogFmt</docanchor>
    <docanchor file="index">secLogVerb</docanchor>
  </compound>
</tagfile>
