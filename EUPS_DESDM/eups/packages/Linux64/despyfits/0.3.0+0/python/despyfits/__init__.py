__author__  = "Felipe Menanteau, Eric Neilsen"
__version__ = '0.3.0'
version = __version__

"""
 A collection of FITS files-related Python functions useful for DESDM
 Python-based modules
"""

from . import fitsutils
from .fitsutils import *
