<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>mainpage.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/doc/</path>
    <filename>mainpage_8dox</filename>
  </compound>
  <compound kind="file">
    <name>exceptions.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/pex/</path>
    <filename>exceptions_8h</filename>
    <includes id="_exception_8h" name="Exception.h" local="yes" imported="no">lsst/pex/exceptions/Exception.h</includes>
    <includes id="_runtime_8h" name="Runtime.h" local="yes" imported="no">lsst/pex/exceptions/Runtime.h</includes>
  </compound>
  <compound kind="file">
    <name>Exception.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/pex/exceptions/</path>
    <filename>_exception_8h</filename>
    <class kind="struct">lsst::pex::exceptions::Tracepoint</class>
    <class kind="class">lsst::pex::exceptions::Exception</class>
    <namespace>lsst::pex</namespace>
    <namespace>lsst::pex::exceptions</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EXCEPT_HERE</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>ad40540101ee315936eb39b50d2b8ad06</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EXCEPT</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>ae48de0285726e06656de9ee35c1530af</anchor>
      <arglist>(type,...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EXCEPT_ADD</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>a537a9c16acf99d344429704051ddef9d</anchor>
      <arglist>(e, m)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EARGS_TYPED</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>a99e342850a1ad3a3f809067e054881f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EARGS_UNTYPED</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>a78bd59a4a3dd5d3db70de3c471e5d750</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>LSST_EXCEPTION_TYPE</name>
      <anchorfile>_exception_8h.html</anchorfile>
      <anchor>a361d0c8a1d5cedfe505b7d3bcf436473</anchor>
      <arglist>(t, b, c)</arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Tracepoint &gt;</type>
      <name>Traceback</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions.html</anchorfile>
      <anchor>ad85f1d7afac85a28d602710cd64c24f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions.html</anchorfile>
      <anchor>a82003c42bea870c00de06a56e0b674f9</anchor>
      <arglist>(std::ostream &amp;stream, Exception const &amp;e)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Runtime.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/pex/exceptions/</path>
    <filename>_runtime_8h</filename>
    <includes id="_exception_8h" name="Exception.h" local="yes" imported="no">lsst/pex/exceptions/Exception.h</includes>
    <class kind="class">lsst::pex::exceptions::LogicErrorException</class>
    <class kind="class">lsst::pex::exceptions::DomainErrorException</class>
    <class kind="class">lsst::pex::exceptions::InvalidParameterException</class>
    <class kind="class">lsst::pex::exceptions::LengthErrorException</class>
    <class kind="class">lsst::pex::exceptions::OutOfRangeException</class>
    <class kind="class">lsst::pex::exceptions::RuntimeErrorException</class>
    <class kind="class">lsst::pex::exceptions::RangeErrorException</class>
    <class kind="class">lsst::pex::exceptions::OverflowErrorException</class>
    <class kind="class">lsst::pex::exceptions::UnderflowErrorException</class>
    <class kind="class">lsst::pex::exceptions::NotFoundException</class>
    <class kind="class">lsst::pex::exceptions::MemoryException</class>
    <class kind="class">lsst::pex::exceptions::IoErrorException</class>
    <class kind="class">lsst::pex::exceptions::TimeoutException</class>
    <namespace>lsst::pex</namespace>
    <namespace>lsst::pex::exceptions</namespace>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/pex/</path>
    <filename>pex_2____init_____8py</filename>
    <namespace>lsst::pex</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pex.html</anchorfile>
      <anchor>a911b93fdf688c7be7c768463f2d8d041</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/pex/exceptions/</path>
    <filename>pex_2exceptions_2____init_____8py</filename>
    <namespace>lsst::pex::exceptions</namespace>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/pex/exceptions/</path>
    <filename>version_8py</filename>
    <namespace>lsst::pex::exceptions::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>ab65f5e0df3ffb52cabb601ffd8bdca94</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a296c1ceb8b1eed829afe08a65c7827e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a1b2ee8f9f50df36252254075413c6ee8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a1e44424e68c547dba23e1ac142cf2545</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>abd15bea1535cc053899b3f671c3f19dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a80484279769b8808d0232d5330cca444</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a58c4c67178319e0855fa1bcf50c2f5e4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Exception.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/src/</path>
    <filename>_exception_8cc</filename>
    <includes id="_exception_8h" name="Exception.h" local="yes" imported="no">lsst/pex/exceptions/Exception.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_exception_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex</name>
    <filename>namespacelsst_1_1pex.html</filename>
    <namespace>lsst::pex::exceptions</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pex.html</anchorfile>
      <anchor>a911b93fdf688c7be7c768463f2d8d041</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::exceptions</name>
    <filename>namespacelsst_1_1pex_1_1exceptions.html</filename>
    <namespace>lsst::pex::exceptions::version</namespace>
    <class kind="struct">lsst::pex::exceptions::Tracepoint</class>
    <class kind="class">lsst::pex::exceptions::Exception</class>
    <class kind="class">lsst::pex::exceptions::LogicErrorException</class>
    <class kind="class">lsst::pex::exceptions::DomainErrorException</class>
    <class kind="class">lsst::pex::exceptions::InvalidParameterException</class>
    <class kind="class">lsst::pex::exceptions::LengthErrorException</class>
    <class kind="class">lsst::pex::exceptions::OutOfRangeException</class>
    <class kind="class">lsst::pex::exceptions::RuntimeErrorException</class>
    <class kind="class">lsst::pex::exceptions::RangeErrorException</class>
    <class kind="class">lsst::pex::exceptions::OverflowErrorException</class>
    <class kind="class">lsst::pex::exceptions::UnderflowErrorException</class>
    <class kind="class">lsst::pex::exceptions::NotFoundException</class>
    <class kind="class">lsst::pex::exceptions::MemoryException</class>
    <class kind="class">lsst::pex::exceptions::IoErrorException</class>
    <class kind="class">lsst::pex::exceptions::TimeoutException</class>
    <member kind="typedef">
      <type>std::vector&lt; Tracepoint &gt;</type>
      <name>Traceback</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions.html</anchorfile>
      <anchor>ad85f1d7afac85a28d602710cd64c24f3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions.html</anchorfile>
      <anchor>a82003c42bea870c00de06a56e0b674f9</anchor>
      <arglist>(std::ostream &amp;stream, Exception const &amp;e)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lsst::pex::exceptions::Tracepoint</name>
    <filename>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</filename>
    <member kind="function">
      <type></type>
      <name>Tracepoint</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>a12f6733f5cee32298175e664f9fd7020</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Tracepoint</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>adfe2152fd258a5ee177366869e955558</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="variable">
      <type>char const *</type>
      <name>_file</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>adc7f0e03c34e1d93b5cd7a6811dba1e3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>_line</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>a5600e5b6062ac49ab4462634089ff438</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>char const *</type>
      <name>_func</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>a42680894c7adff6df4e36a06b89bd540</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>_msg</name>
      <anchorfile>structlsst_1_1pex_1_1exceptions_1_1_tracepoint.html</anchorfile>
      <anchor>a7a10630b16b2839daa3efed95f159de0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::Exception</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_exception.html</filename>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::LogicErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_logic_error_exception.html</filename>
    <base>lsst::pex::exceptions::Exception</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::DomainErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_domain_error_exception.html</filename>
    <base>lsst::pex::exceptions::LogicErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::InvalidParameterException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_invalid_parameter_exception.html</filename>
    <base>lsst::pex::exceptions::LogicErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::LengthErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_length_error_exception.html</filename>
    <base>lsst::pex::exceptions::LogicErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::OutOfRangeException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_out_of_range_exception.html</filename>
    <base>lsst::pex::exceptions::LogicErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::RuntimeErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_runtime_error_exception.html</filename>
    <base>lsst::pex::exceptions::Exception</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::RangeErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_range_error_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::OverflowErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_overflow_error_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::UnderflowErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_underflow_error_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::NotFoundException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_not_found_exception.html</filename>
    <base>lsst::pex::exceptions::Exception</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::MemoryException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_memory_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::IoErrorException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_io_error_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::exceptions::TimeoutException</name>
    <filename>classlsst_1_1pex_1_1exceptions_1_1_timeout_exception.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>ac611f3f7503ce9bb76d360537ded8769</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Exception</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a24e887eb96d05b719fc152e479ca08d8</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addMessage</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a0fa99a54813352fe9cace13f4abba2b3</anchor>
      <arglist>(char const *file, int line, char const *func, std::string const &amp;message)</arglist>
    </member>
    <member kind="function">
      <type>Traceback const &amp;</type>
      <name>getTraceback</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a6211b2cf696853486b60961066687fba</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>addToStream</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a66aaa02d4676004196e5ec53b6d01f58</anchor>
      <arglist>(std::ostream &amp;stream) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>what</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>acdaff6c5eb871be969e175d3fc3fcfb2</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>a41d3b3bec32f4f623bc94e19613ca3ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1exceptions_1_1_exception.html</anchorfile>
      <anchor>aac75676b4a7e47e80a14b521cf5c56df</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::exceptions::version</name>
    <filename>namespacelsst_1_1pex_1_1exceptions_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>ab65f5e0df3ffb52cabb601ffd8bdca94</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a296c1ceb8b1eed829afe08a65c7827e6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a1b2ee8f9f50df36252254075413c6ee8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a1e44424e68c547dba23e1ac142cf2545</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>abd15bea1535cc053899b3f671c3f19dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a80484279769b8808d0232d5330cca444</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1exceptions_1_1version.html</anchorfile>
      <anchor>a58c4c67178319e0855fa1bcf50c2f5e4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex/exceptions</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/pex/exceptions/</path>
    <filename>dir_33fab5f758c2221b27bad9a693139b46.html</filename>
    <file>__init__.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex/exceptions</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/pex/exceptions/</path>
    <filename>dir_fc079d756484b023a2db0705e5c981c1.html</filename>
    <file>Exception.h</file>
    <file>Runtime.h</file>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/pex</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/pex</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/lsst/pex/</path>
    <filename>dir_513028b95738c600a2c7d67ec6cba6e9.html</filename>
    <dir>python/lsst/pex/exceptions</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/include/lsst/pex/</path>
    <filename>dir_da59fd778d3d02495d7b88077ed25f4c.html</filename>
    <dir>include/lsst/pex/exceptions</dir>
    <file>exceptions.h</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_exceptions-6.2.0.0+3/pex_exceptions-6.2.0.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>Exception.cc</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>lsst::pex::exceptions;  LSST Exceptions</title>
    <filename>index</filename>
    <docanchor file="index">secExcIntro</docanchor>
    <docanchor file="index">secExcPython</docanchor>
  </compound>
</tagfile>
