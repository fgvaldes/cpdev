<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>base.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/include/lsst/</path>
    <filename>base_8h</filename>
    <member kind="define">
      <type>#define</type>
      <name>LSST_WHITESPACE</name>
      <anchorfile>base_8h.html</anchorfile>
      <anchor>aa4af8f55c4808713b052c7209c538531</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>PTR</name>
      <anchorfile>base_8h.html</anchorfile>
      <anchor>a0344343ad45c45b1bb5108bfe1b12acf</anchor>
      <arglist>(...)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>CONST_PTR</name>
      <anchorfile>base_8h.html</anchorfile>
      <anchor>a4b5b68b2a3843eefc5abfa7e9e23a66a</anchor>
      <arglist>(...)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>ModuleImporter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/include/lsst/base/</path>
    <filename>_module_importer_8h</filename>
    <class kind="class">lsst::base::ModuleImporter</class>
    <namespace>lsst</namespace>
    <namespace>lsst::base</namespace>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <namespace>lsst</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/lsst/base/</path>
    <filename>base_2____init_____8py</filename>
    <namespace>lsst::base</namespace>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/lsst/base/</path>
    <filename>version_8py</filename>
    <namespace>lsst::base::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a0430a2a7ed37b18ed91b6f67e07f0be8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a424ea14b6527472b86c73c7579f64c23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>aa700c58a2a9605f312e51f8892436a60</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>ae07cc6e13685eb60e9409daf783b7596</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>ae0efb798644f318e8d388f082339eed8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a840ee24f70a7a1f9e7aa034b06565574</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>abc4805405d0af9ee69eb8eeba9403b24</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>lsst64defs.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/</path>
    <filename>lsst64defs_8py</filename>
    <namespace>lsst64defs</namespace>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_GLOBAL</name>
      <anchorfile>namespacelsst64defs.html</anchorfile>
      <anchor>a937a9e7d760646e08c532d4a5ebf1ee6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_NOW</name>
      <anchorfile>namespacelsst64defs.html</anchorfile>
      <anchor>aa9c8525dbc1198ab8a9c7e397a896beb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>lsstDebug.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/</path>
    <filename>lsst_debug_8py</filename>
    <class kind="class">lsstDebug::Info</class>
    <namespace>lsstDebug</namespace>
    <member kind="variable">
      <type></type>
      <name>getInfo</name>
      <anchorfile>namespacelsst_debug.html</anchorfile>
      <anchor>a8aff38cccd562455f8d1d7fad8fc2a67</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>lsstimport.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/</path>
    <filename>lsstimport_8py</filename>
    <namespace>lsstimport</namespace>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_GLOBAL</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a1f60d95b497561378fa24412fd7cd0ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_NOW</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a65566742089e8a1832010f4fe2a3eea0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dlflags</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a08864fc5697e6757ec871016b8d541ec</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst</name>
    <filename>namespacelsst.html</filename>
    <namespace>lsst::base</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst64defs</name>
    <filename>namespacelsst64defs.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_GLOBAL</name>
      <anchorfile>namespacelsst64defs.html</anchorfile>
      <anchor>a937a9e7d760646e08c532d4a5ebf1ee6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_NOW</name>
      <anchorfile>namespacelsst64defs.html</anchorfile>
      <anchor>aa9c8525dbc1198ab8a9c7e397a896beb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::base</name>
    <filename>namespacelsst_1_1base.html</filename>
    <namespace>lsst::base::version</namespace>
    <class kind="class">lsst::base::ModuleImporter</class>
  </compound>
  <compound kind="class">
    <name>lsst::base::ModuleImporter</name>
    <filename>classlsst_1_1base_1_1_module_importer.html</filename>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>import</name>
      <anchorfile>classlsst_1_1base_1_1_module_importer.html</anchorfile>
      <anchor>af52c92751e130e3acc002368c745c888</anchor>
      <arglist>(std::string const &amp;name)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>ModuleImporter</name>
      <anchorfile>classlsst_1_1base_1_1_module_importer.html</anchorfile>
      <anchor>ae543c487e8540ac1dc2b58e471a37a57</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual bool</type>
      <name>_import</name>
      <anchorfile>classlsst_1_1base_1_1_module_importer.html</anchorfile>
      <anchor>a1729f393961ea7d003c803e8547e962d</anchor>
      <arglist>(std::string const &amp;name) const =0</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual</type>
      <name>~ModuleImporter</name>
      <anchorfile>classlsst_1_1base_1_1_module_importer.html</anchorfile>
      <anchor>aaff9a94272d36011617d836446bc8945</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend void</type>
      <name>installPythonModuleImporter</name>
      <anchorfile>classlsst_1_1base_1_1_module_importer.html</anchorfile>
      <anchor>a7eae13e1183f7916851c9684333f57b9</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::base::version</name>
    <filename>namespacelsst_1_1base_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a0430a2a7ed37b18ed91b6f67e07f0be8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a424ea14b6527472b86c73c7579f64c23</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>aa700c58a2a9605f312e51f8892436a60</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>ae07cc6e13685eb60e9409daf783b7596</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>ae0efb798644f318e8d388f082339eed8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>a840ee24f70a7a1f9e7aa034b06565574</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1base_1_1version.html</anchorfile>
      <anchor>abc4805405d0af9ee69eb8eeba9403b24</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsstDebug</name>
    <filename>namespacelsst_debug.html</filename>
    <class kind="class">lsstDebug::Info</class>
    <member kind="variable">
      <type></type>
      <name>getInfo</name>
      <anchorfile>namespacelsst_debug.html</anchorfile>
      <anchor>a8aff38cccd562455f8d1d7fad8fc2a67</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsstDebug::Info</name>
    <filename>classlsst_debug_1_1_info.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_debug_1_1_info.html</anchorfile>
      <anchor>a1a86587bfd77a303c6dad14f28c38713</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getattr__</name>
      <anchorfile>classlsst_debug_1_1_info.html</anchorfile>
      <anchor>a9606fdfcc20574caaa34df4a858f0b8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setattr__</name>
      <anchorfile>classlsst_debug_1_1_info.html</anchorfile>
      <anchor>ab2a5035abb9663e9c8c856ee54de40c5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsstimport</name>
    <filename>namespacelsstimport.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_GLOBAL</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a1f60d95b497561378fa24412fd7cd0ee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>RTLD_NOW</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a65566742089e8a1832010f4fe2a3eea0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dlflags</name>
      <anchorfile>namespacelsstimport.html</anchorfile>
      <anchor>a08864fc5697e6757ec871016b8d541ec</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>python/lsst/base</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/lsst/base/</path>
    <filename>dir_bbe979f08639e03139f102453e7d88a9.html</filename>
    <file>__init__.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/base</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/include/lsst/base/</path>
    <filename>dir_3d5b8a05b96a6ffcea599c638b7af6fe.html</filename>
    <file>ModuleImporter.h</file>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/base</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/base</dir>
    <file>base.h</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/base-7.1.1.0+1/base-7.1.1.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
    <file>lsst64defs.py</file>
    <file>lsstDebug.py</file>
    <file>lsstimport.py</file>
  </compound>
</tagfile>
