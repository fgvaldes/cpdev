/* wcsconfig.h.  Generated from wcsconfig.h.in by configure.  */
/*============================================================================
*
* wcsconfig.h is generated from wcsconfig.h.in by 'configure'.  It contains
* C preprocessor macro definitions for compiling WCSLIB 4.14
*
* Author: Mark Calabretta, Australia Telescope National Facility, CSIRO.
* http://www.atnf.csiro.au/people/Mark.Calabretta
* $Id: wcsconfig.h.in,v 4.14 2012/07/13 10:02:44 cal103 Exp $
*===========================================================================*/

/* WCSLIB library version number. */
#define WCSLIB_VERSION 4.14

/* Define to 1 if sincos() is available. */
#define HAVE_SINCOS 1

/* 64-bit integer data type. */
#define WCSLIB_INT64 long long int
