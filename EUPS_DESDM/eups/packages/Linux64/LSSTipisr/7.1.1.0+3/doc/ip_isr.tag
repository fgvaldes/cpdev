<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>isr.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/include/lsst/ip/</path>
    <filename>isr_8h</filename>
    <class kind="class">lsst::ip::isr::LookupTableMultiplicative</class>
    <class kind="class">lsst::ip::isr::LookupTableReplace</class>
    <class kind="class">lsst::ip::isr::CountMaskedPixels</class>
    <class kind="class">lsst::ip::isr::UnmaskedNanCounter</class>
    <namespace>lsst::ip</namespace>
    <namespace>lsst::ip::isr</namespace>
    <member kind="enumeration">
      <name>StageId</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_LINid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a5f1eb52d28b7ea7073a8cb60a34e225f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_OSCANid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88ab27197726f464a245428ec0902f6f768</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_TRIMid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a39d4cacab5a47f7c5b22a1d24b10dffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BIASid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a33b8f3b5f31e294d4ac68a3f519696b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_DFLATid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a09aae0d1529bea528a4fa979a9db176c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_ILLUMid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a188f797bf3290add5a75cf0660c5f58c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BADPid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88ae4ad4b4761e40b05d927f2d998ce1ab5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_SATid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a0da3f09388890fffb3e56f3b4e07f409</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_FRINid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a585225eeb0985bbc8ed8efead469e154</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_DARKid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88aaf78539b0270f908c15ddbe719abac95</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_PUPILid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88af25b2d15b8d653f423089a43c7a756bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_CRREJid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a5c83e301543f106b19841706ffa1c1b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BACKSUBid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a8169100e2270db325804cf642b8acbf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a835b15d3fe4ce42fead20bcf324eca3e</anchor>
      <arglist>(boost::shared_ptr&lt; lsst::afw::math::Function1&lt; FunctionT &gt; &gt; &amp;overscanFunction, lsst::afw::image::MaskedImage&lt; ImagePixelT &gt; const &amp;overscan, double ssize=1., int sigma=1)</arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_LIN</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a75c54a3acf16ca60aa666a462404991c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_OSCAN</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ac1eb0ebab12c423bd415ab0954b5a65b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_TRIM</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a125aa45e902e3894fe7731c14f1884b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BIAS</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a3bdc8e213fbaf70c9a5b3d410c076d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_DFLAT</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a3a323c2aea91e51044d727e4aa5d5970</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_ILLUM</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a446258c2e1f897136d84b89d350ea601</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BADP</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a93c7fac45b7bca4d723af31cc714adc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_SAT</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ae05a440e3b44057b2cae9c77fa6ae7b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_FRING</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a98dc1e6c9997f788e0cce1940300f8ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_DARK</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ab9e67e07c9abc20486f4549e654ef8a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_PUPIL</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ab2dad39c0ffd73aa95db912b45d5f187</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_CRREJ</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a1b33ec1029a5eecca4011624a15bff37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BACKSUB</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a584116f463a27e0e2823347953f1586b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/</path>
    <filename>ip_2____init_____8py</filename>
    <namespace>lsst::ip</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1ip.html</anchorfile>
      <anchor>a249a970fe304b96ec8a9409939454e3f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>ip_2isr_2____init_____8py</filename>
    <namespace>lsst::ip::isr</namespace>
  </compound>
  <compound kind="file">
    <name>assembleCcdTask.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>assemble_ccd_task_8py</filename>
    <class kind="class">lsst::ip::isr::assembleCcdTask::AssembleCcdConfig</class>
    <class kind="class">lsst::ip::isr::assembleCcdTask::AssembleCcdTask</class>
    <namespace>lsst::ip::isr::assembleCcdTask</namespace>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1assemble_ccd_task.html</anchorfile>
      <anchor>adfcd4886c44ca0499281cb9610071c8e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>fringe.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>fringe_8py</filename>
    <class kind="class">lsst::ip::isr::fringe::FringeStatisticsConfig</class>
    <class kind="class">lsst::ip::isr::fringe::FringeConfig</class>
    <class kind="class">lsst::ip::isr::fringe::FringeTask</class>
    <namespace>lsst::ip::isr::fringe</namespace>
    <member kind="function">
      <type>def</type>
      <name>getFrame</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>aa09c10baab03fd8d137e0c811ceddaf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>measure</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>a173094cca7e30be34132eb68bbe4eac1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stdev</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>ade74c9882f6116cf870de548eb504bf0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>isr.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>isr_8py</filename>
    <namespace>lsst::ip::isr::isr</namespace>
    <member kind="function">
      <type>def</type>
      <name>createPsf</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a0d5d6a8017268c5361489239546fcdc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calcEffectiveGain</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a32ad249b8ec54d1315e5162ca2877f71</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>transposeMaskedImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a23312c9f3d6993f661ed20be24200d98</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calculateSdqaCcdRatings</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a9536e45a0b3d602f202730a660bb0253</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calculateSdqaAmpRatings</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ac5a6b8d78415ddcac6828b397d84d20f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interpolateDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a42ac5b88fc6dc2f41b0a49f4bf944e10</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>defectListFromFootprintList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>aff10b5e72c8bef356b4e7120ccff0bbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>transposeDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a0d92c79b375f6acf7ce4d20ee4a68f55</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>maskPixelsFromDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>aac0f8953dcd2592e0b85343bd1e1c418</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDefectListFromMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a27aa9baad1e72279cbe0c36c987bfdc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeThresholdMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a13e601726d644306e5dba13be72e6738</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interpolateFromMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a20e1eee9d8fb4fd92c15092c80f6e20a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>saturationCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ac9983f648f3db437114563514099db59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>biasCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a71a911c0d4afa8852350f6ce48954a8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>darkCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a1db342075ff92bbbc385771399211626</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>updateVariance</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ad6845b893bbdc4b72c5cfacb727151d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>flatCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ab0132e8964c80de4abacf9e1e1ee82b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>illuminationCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a83454e0fce9e924e98e8b24c7f498d59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>trimAmp</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ae4596969b0c0e37717a4edf985d0efc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>overscanCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a51bced3d0675be4594a830bbafa23b1c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>isrTask.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>isr_task_8py</filename>
    <class kind="class">lsst::ip::isr::isrTask::IsrTaskConfig</class>
    <class kind="class">lsst::ip::isr::isrTask::IsrTask</class>
    <namespace>lsst::ip::isr::isrTask</namespace>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>version_8py</filename>
    <namespace>lsst::ip::isr::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a5aa12113bd1c6d0f6b790729d1788b93</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a6ea879f312dc2f6d1120072491844886</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>acb6d0dfdf56715ac28909aaa9c6db4b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>ac4277177f8d3e1c0b9994716097e5846</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>ac1ea0a8d6aece00692bebd21bfbdca87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>af94afadee2f58fc2145cfe3f3c1e400a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a52efacbfd6718b1e169ced13787c69bb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Isr.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/src/</path>
    <filename>_isr_8cc</filename>
    <includes id="isr_8h" name="isr.h" local="yes" imported="no">lsst/ip/isr.h</includes>
    <namespace>lsst::ip</namespace>
    <namespace>lsst::ip::isr</namespace>
    <member kind="function">
      <type>void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a7861576ae0ccf279388fea739f2e18ef</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; FunctionT &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; ImagePixelT &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>between</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>afa41dacbfb489ee7cad58a4d212c139a</anchor>
      <arglist>(std::string &amp;s, char ldelim, char rdelim)</arglist>
    </member>
    <member kind="function">
      <type>template void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a07b401a3e88a07223aa01249007d6b1c</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; double &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; float &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
    <member kind="function">
      <type>template void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a425f15e4a1a7ba90f60607bc9a78b2fa</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; double &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; double &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::afw::table::detail::SchemaImpl</name>
    <filename>namespacelsst_1_1afw_1_1table_1_1detail_1_1_schema_impl.html</filename>
  </compound>
  <compound kind="namespace">
    <name>lsst::afw::table::detail::SchemaMapperImpl</name>
    <filename>namespacelsst_1_1afw_1_1table_1_1detail_1_1_schema_mapper_impl.html</filename>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip</name>
    <filename>namespacelsst_1_1ip.html</filename>
    <namespace>lsst::ip::isr</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1ip.html</anchorfile>
      <anchor>a249a970fe304b96ec8a9409939454e3f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr</name>
    <filename>namespacelsst_1_1ip_1_1isr.html</filename>
    <namespace>lsst::ip::isr::assembleCcdTask</namespace>
    <namespace>lsst::ip::isr::fringe</namespace>
    <namespace>lsst::ip::isr::isr</namespace>
    <namespace>lsst::ip::isr::isrTask</namespace>
    <namespace>lsst::ip::isr::version</namespace>
    <class kind="class">lsst::ip::isr::LookupTableMultiplicative</class>
    <class kind="class">lsst::ip::isr::LookupTableReplace</class>
    <class kind="class">lsst::ip::isr::CountMaskedPixels</class>
    <class kind="class">lsst::ip::isr::UnmaskedNanCounter</class>
    <member kind="enumeration">
      <name>StageId</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_LINid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a5f1eb52d28b7ea7073a8cb60a34e225f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_OSCANid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88ab27197726f464a245428ec0902f6f768</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_TRIMid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a39d4cacab5a47f7c5b22a1d24b10dffb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BIASid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a33b8f3b5f31e294d4ac68a3f519696b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_DFLATid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a09aae0d1529bea528a4fa979a9db176c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_ILLUMid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a188f797bf3290add5a75cf0660c5f58c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BADPid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88ae4ad4b4761e40b05d927f2d998ce1ab5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_SATid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a0da3f09388890fffb3e56f3b4e07f409</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_FRINid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a585225eeb0985bbc8ed8efead469e154</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_DARKid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88aaf78539b0270f908c15ddbe719abac95</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_PUPILid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88af25b2d15b8d653f423089a43c7a756bc</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_CRREJid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a5c83e301543f106b19841706ffa1c1b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>ISR_BACKSUBid</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a827b2e82c5e0f24bd855d3fff1cc4a88a8169100e2270db325804cf642b8acbf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a835b15d3fe4ce42fead20bcf324eca3e</anchor>
      <arglist>(boost::shared_ptr&lt; lsst::afw::math::Function1&lt; FunctionT &gt; &gt; &amp;overscanFunction, lsst::afw::image::MaskedImage&lt; ImagePixelT &gt; const &amp;overscan, double ssize=1., int sigma=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a7861576ae0ccf279388fea739f2e18ef</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; FunctionT &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; ImagePixelT &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>between</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>afa41dacbfb489ee7cad58a4d212c139a</anchor>
      <arglist>(std::string &amp;s, char ldelim, char rdelim)</arglist>
    </member>
    <member kind="function">
      <type>template void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a07b401a3e88a07223aa01249007d6b1c</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; double &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; float &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
    <member kind="function">
      <type>template void</type>
      <name>fitOverscanImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a425f15e4a1a7ba90f60607bc9a78b2fa</anchor>
      <arglist>(boost::shared_ptr&lt; afw::math::Function1&lt; double &gt; &gt; &amp;overscanFunction, afw::image::MaskedImage&lt; double &gt; const &amp;overscan, double ssize, int sigma)</arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_LIN</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a75c54a3acf16ca60aa666a462404991c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_OSCAN</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ac1eb0ebab12c423bd415ab0954b5a65b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_TRIM</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a125aa45e902e3894fe7731c14f1884b5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BIAS</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a3bdc8e213fbaf70c9a5b3d410c076d2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_DFLAT</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a3a323c2aea91e51044d727e4aa5d5970</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_ILLUM</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a446258c2e1f897136d84b89d350ea601</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BADP</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a93c7fac45b7bca4d723af31cc714adc8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_SAT</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ae05a440e3b44057b2cae9c77fa6ae7b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_FRING</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a98dc1e6c9997f788e0cce1940300f8ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_DARK</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ab9e67e07c9abc20486f4549e654ef8a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_PUPIL</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>ab2dad39c0ffd73aa95db912b45d5f187</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_CRREJ</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a1b33ec1029a5eecca4011624a15bff37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string const &amp;</type>
      <name>ISR_BACKSUB</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr.html</anchorfile>
      <anchor>a584116f463a27e0e2823347953f1586b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::LookupTableMultiplicative</name>
    <filename>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; ImageT &gt;::x_iterator</type>
      <name>x_iterator</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>aa22a0a0398f4ccffb041bede7114bcf6</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; ImageT &gt;::Pixel</type>
      <name>PixelT</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>a1ebd231aaf522db6440c44f133639957</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LookupTableMultiplicative</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>aa88144fc85b78e2ca5a3729630b6adf9</anchor>
      <arglist>(std::vector&lt; double &gt; table)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LookupTableMultiplicative</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>a8d3b5284af8a2b3d82f55c885c5bc162</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>apply</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>a50f77d45db10ec64c7f970acbbeaf87b</anchor>
      <arglist>(lsst::afw::image::MaskedImage&lt; ImageT &gt; &amp;image, float gain=1.0)</arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; double &gt;</type>
      <name>getTable</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_multiplicative.html</anchorfile>
      <anchor>ab31fa56eba50a29329f5848fbf039e3f</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::LookupTableReplace</name>
    <filename>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; ImageT &gt;::x_iterator</type>
      <name>x_iterator</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>aefb1de5eaca48449afaec280b751b8b2</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; ImageT &gt;::Pixel</type>
      <name>PixelT</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>a79e00a36d8672eeb2a89e0b22322614a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LookupTableReplace</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>adb72660ef1427b0a7f57da18d0923504</anchor>
      <arglist>(std::vector&lt; double &gt; table)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~LookupTableReplace</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>a284d3f70c3502aefcc82ae1c9e954058</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>apply</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>aaf5abd8f41edf3daddaf1011962397b8</anchor>
      <arglist>(lsst::afw::image::MaskedImage&lt; ImageT &gt; &amp;image, float gain=1.0) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; double &gt;</type>
      <name>getTable</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_lookup_table_replace.html</anchorfile>
      <anchor>a5ae46c65af310581698dd9ea47dcc5b4</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::CountMaskedPixels</name>
    <filename>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</filename>
    <templarg></templarg>
    <templarg></templarg>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; ImageT &gt;::x_iterator</type>
      <name>x_iterator</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>a78ec96ea1fd110fbe069124b2ed32582</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>CountMaskedPixels</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>a3bd4b4b96606a0f07a52d4c5b2c42f0f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~CountMaskedPixels</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>aae1518f7b954e22bf830eef82043ed6a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>a459424bc1c76d5b6641f4ad75ba7b44d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>apply</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>a0133c7a373027ae75085f92b3e736dcf</anchor>
      <arglist>(lsst::afw::image::MaskedImage&lt; ImageT &gt; const &amp;image, MaskT bitmask)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getCount</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_count_masked_pixels.html</anchorfile>
      <anchor>a94640b822237f4b6d1c6f4315f38bb30</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::UnmaskedNanCounter</name>
    <filename>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</filename>
    <templarg></templarg>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; UnmaskedNanCounter &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>a38ca102413a4b57d00c509bf5f2dd057</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>lsst::afw::image::MaskedImage&lt; PixelT &gt;::x_iterator</type>
      <name>x_iterator</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>abd45ddd8b90915612c9d7fcdf8f3e3f5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>UnmaskedNanCounter</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>adcd8b70f4e0511f3d4906e32d3db3c6d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~UnmaskedNanCounter</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>ad2a72f371fb59eae3ee7d83d20a057df</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>ac3d102e6ec8a27b2d9cae1b8e79386a1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>apply</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>aa0371687bf6f8d314466226ef2511ba4</anchor>
      <arglist>(lsst::afw::image::MaskedImage&lt; PixelT &gt; const &amp;mi)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getNpix</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1_unmasked_nan_counter.html</anchorfile>
      <anchor>ade858e3a5891ca6ee5d0b1c66142c730</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr::assembleCcdTask</name>
    <filename>namespacelsst_1_1ip_1_1isr_1_1assemble_ccd_task.html</filename>
    <class kind="class">lsst::ip::isr::assembleCcdTask::AssembleCcdConfig</class>
    <class kind="class">lsst::ip::isr::assembleCcdTask::AssembleCcdTask</class>
    <member kind="variable">
      <type>list</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1assemble_ccd_task.html</anchorfile>
      <anchor>adfcd4886c44ca0499281cb9610071c8e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::assembleCcdTask::AssembleCcdConfig</name>
    <filename>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_config.html</filename>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>setGain</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_config.html</anchorfile>
      <anchor>a1b88904b41cd18c9a153ee90e1ac3d15</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doRenorm</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_config.html</anchorfile>
      <anchor>a74c29a4f9d2bf6473fa46d1939fa36a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doTrim</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_config.html</anchorfile>
      <anchor>a51c4b13dcace0e48f721b5ec5c4f3557</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>keysToRemove</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_config.html</anchorfile>
      <anchor>aba61cca840eae9420ec7cd0610c5c9c7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::assembleCcdTask::AssembleCcdTask</name>
    <filename>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>a2ce1a7fcd66bf572e465b107ea4b2378</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assembleCcd</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>a4197c3ee61b1e17ebc06c27ec7b99aed</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>assembleAmpList</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>a13a96b70c0e3aa74099a61dd58f93528</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>postprocessExposure</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>ab4fdba3d9cc6e3ab4aaf1a5c2d7c6a30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setWcs</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>ac318e6c2bbfd47e05ac6a91aae9c2682</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>setGain</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>a82634cd522820c47d0cc91c2852983da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>allKeysToRemove</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>afb7775fc26fed5fb5b258ab8599eca66</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ConfigClass</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1assemble_ccd_task_1_1_assemble_ccd_task.html</anchorfile>
      <anchor>a21aa719562a3f251066a76e1b7e42cdd</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr::fringe</name>
    <filename>namespacelsst_1_1ip_1_1isr_1_1fringe.html</filename>
    <class kind="class">lsst::ip::isr::fringe::FringeStatisticsConfig</class>
    <class kind="class">lsst::ip::isr::fringe::FringeConfig</class>
    <class kind="class">lsst::ip::isr::fringe::FringeTask</class>
    <member kind="function">
      <type>def</type>
      <name>getFrame</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>aa09c10baab03fd8d137e0c811ceddaf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>measure</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>a173094cca7e30be34132eb68bbe4eac1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>stdev</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1fringe.html</anchorfile>
      <anchor>ade74c9882f6116cf870de548eb504bf0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::fringe::FringeStatisticsConfig</name>
    <filename>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_statistics_config.html</filename>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>badMaskPlanes</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_statistics_config.html</anchorfile>
      <anchor>a2c7bac9395a594e06511917ca18896cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>stat</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_statistics_config.html</anchorfile>
      <anchor>a0f8bfd5b469afb8bb7c805b70ab32ccf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>clip</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_statistics_config.html</anchorfile>
      <anchor>ad96a4510136c68104b709438c94e3717</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>iterations</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_statistics_config.html</anchorfile>
      <anchor>a9e511f432b996c155ed72f4e0fcbac63</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::fringe::FringeConfig</name>
    <filename>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</filename>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>filters</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a2de4e47107d80bc32989cc521dc9579e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>num</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a397d15500a951442bc46ff4f4c8a2aff</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>small</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a58d0f53c8599966f9a0f35b66bc23149</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>large</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>aebcf903b45ad76a6f0ad039be6a2388b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>iterations</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a36b72f2c41f3492ee8a3eb83e5296109</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>clip</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a2950bd9ca001388647ff71335875ee60</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>stats</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a93981c2d1a6c5ec97e47c84f0c0462c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>pedestal</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_config.html</anchorfile>
      <anchor>a57e9746ded458d5217edc255b61d5682</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::fringe::FringeTask</name>
    <filename>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</filename>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a299a66ae900120832b0c76f0cfc66aca</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>checkFilter</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a1c50d3972fd861088e1aabf988c2f187</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>readFringes</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a9d2f561b887200302e56c71e159355fb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>generatePositions</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a0cac80cf8f1b65bfe74649b4c3944869</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>measureExposure</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a633febf9ee3902bb921daf9bc37e54a2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>solve</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>ad04097c20f06acf45357eddd92e2e988</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>subtract</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a3dbd78cd245f05dfcbbd85d4b98a1b97</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ConfigClass</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1fringe_1_1_fringe_task.html</anchorfile>
      <anchor>a08071142638e1e3456d813499d66069f</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr::isr</name>
    <filename>namespacelsst_1_1ip_1_1isr_1_1isr.html</filename>
    <member kind="function">
      <type>def</type>
      <name>createPsf</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a0d5d6a8017268c5361489239546fcdc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calcEffectiveGain</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a32ad249b8ec54d1315e5162ca2877f71</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>transposeMaskedImage</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a23312c9f3d6993f661ed20be24200d98</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calculateSdqaCcdRatings</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a9536e45a0b3d602f202730a660bb0253</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>calculateSdqaAmpRatings</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ac5a6b8d78415ddcac6828b397d84d20f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interpolateDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a42ac5b88fc6dc2f41b0a49f4bf944e10</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>defectListFromFootprintList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>aff10b5e72c8bef356b4e7120ccff0bbb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>transposeDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a0d92c79b375f6acf7ce4d20ee4a68f55</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>maskPixelsFromDefectList</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>aac0f8953dcd2592e0b85343bd1e1c418</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDefectListFromMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a27aa9baad1e72279cbe0c36c987bfdc6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>makeThresholdMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a13e601726d644306e5dba13be72e6738</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>interpolateFromMask</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a20e1eee9d8fb4fd92c15092c80f6e20a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>saturationCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ac9983f648f3db437114563514099db59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>biasCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a71a911c0d4afa8852350f6ce48954a8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>darkCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a1db342075ff92bbbc385771399211626</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>updateVariance</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ad6845b893bbdc4b72c5cfacb727151d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>flatCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ab0132e8964c80de4abacf9e1e1ee82b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>illuminationCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a83454e0fce9e924e98e8b24c7f498d59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>trimAmp</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>ae4596969b0c0e37717a4edf985d0efc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>overscanCorrection</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1isr.html</anchorfile>
      <anchor>a51bced3d0675be4594a830bbafa23b1c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr::isrTask</name>
    <filename>namespacelsst_1_1ip_1_1isr_1_1isr_task.html</filename>
    <class kind="class">lsst::ip::isr::isrTask::IsrTaskConfig</class>
    <class kind="class">lsst::ip::isr::isrTask::IsrTask</class>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::isrTask::IsrTaskConfig</name>
    <filename>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</filename>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doBias</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>aa79173bf6a78a6b8cba8799908fd9509</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doDark</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>ac43b57a4e27d8fe2d99d5aa6099b6bc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doFlat</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a3bd0451adf9c76a4fc859524cd1bafeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doFringe</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a3a8753dadfb2ab9448df9281dfe17a2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doWrite</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a5af75366f1e3a9f34b882a16d75195af</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>assembleCcd</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a6d94b74c4537822b63b4931f2b42266c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>fringeAfterFlat</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a2379e0af94fbfc8bc6d90db68c3e1919</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>fringe</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a71912d92cb377a3c5d011352b2509561</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>fwhm</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a4d8b1f0f0e11ae202ae79a2b015edfae</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>saturatedMaskName</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a8103f2346bd2a60f6899115016a8e2dc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>flatScalingType</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>aa039855118f20f97a5ebe67e8aa8961a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>flatUserScale</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>af547c4174af40fc8cd6731075e25764f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>overscanFitType</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>ae7512c6fb0259a15e288e03a8195c5b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>overscanPolyOrder</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>aed4e7dccbfc2a173721cf30c2315b17f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>growSaturationFootprintSize</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a85d3af8926eb31000c935e08cc85c563</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>fluxMag0T1</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>aa9f8e6f6485f4cdf532ed298e2694eee</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>setGainAssembledCcd</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>ac9d0a754147dbc4d0c2886559c5de01d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>keysToRemoveFromAssembledCcd</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a504fd07ff77e0e146253183c6b01bdc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>tuple</type>
      <name>doAssembleDetrends</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task_config.html</anchorfile>
      <anchor>a750a57ac1eef249cf310310df7eda881</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::ip::isr::isrTask::IsrTask</name>
    <filename>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a2854c33ab33677c6c19427c52409bb48</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>run</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>aab476cefa23d730451f39119e04875d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>checkIsAmp</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>aebc837b76dbc135aac88fdbfa1a1781c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>convertIntToFloat</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a1fec5cfe0890a017aa796c0104eff39f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>biasCorrection</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>aa6ccdf9dcf1735c5ed90c2c23e496725</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>darkCorrection</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>ab41dc49d2b1df5388fe3f653bfadcfd6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>updateVariance</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a8f5afe71d7d8b7bc824fd15f63257b8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>flatCorrection</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>ae6918c99805e1f902687842a7b09cf56</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDetrend</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a3a72ac6fab6e4f6f046fe8461de413f4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>saturationDetection</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a853d9470afa9e178fb42bb050e6fc3a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>saturationInterpolation</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a7d6b3e4ec6233d1da18a514be8d82f63</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>maskAndInterpDefect</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>ac938896ee62ee77619f07fb85de47350</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>maskAndInterpNan</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a5ae0dffccdb1be2188a1538baed45412</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>overscanCorrection</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a5e5c48656c428d20fb981a6858ee98cb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>transposeForInterpolation</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a075b6ffcee3e9aa2002c1bd8d1074bb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type></type>
      <name>ConfigClass</name>
      <anchorfile>classlsst_1_1ip_1_1isr_1_1isr_task_1_1_isr_task.html</anchorfile>
      <anchor>a4855a6fe925b7d5a00adb92c963f5111</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::ip::isr::version</name>
    <filename>namespacelsst_1_1ip_1_1isr_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a5aa12113bd1c6d0f6b790729d1788b93</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a6ea879f312dc2f6d1120072491844886</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>acb6d0dfdf56715ac28909aaa9c6db4b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>ac4277177f8d3e1c0b9994716097e5846</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>ac1ea0a8d6aece00692bebd21bfbdca87</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>af94afadee2f58fc2145cfe3f3c1e400a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1ip_1_1isr_1_1version.html</anchorfile>
      <anchor>a52efacbfd6718b1e169ced13787c69bb</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/ip</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/</path>
    <filename>dir_5a316154dea37ff4b035482c8de60969.html</filename>
    <dir>python/lsst/ip/isr</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/ip</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/include/lsst/ip/</path>
    <filename>dir_4cf0246c09f2a1ecbfbddac74a3e2521.html</filename>
    <file>isr.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/ip/isr</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/ip/isr/</path>
    <filename>dir_e1713a6c112c2c45ed4cedcf9b86bdd0.html</filename>
    <file>__init__.py</file>
    <file>assembleCcdTask.py</file>
    <file>fringe.py</file>
    <file>isr.py</file>
    <file>isrTask.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/ip</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/ip</dir>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/ip_isr-7.1.1.0+3/ip_isr-7.1.1.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>Isr.cc</file>
  </compound>
</tagfile>
