<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>base.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/</path>
    <filename>base_8h</filename>
    <includes id="_citizen_8h" name="Citizen.h" local="yes" imported="no">lsst/daf/base/Citizen.h</includes>
    <includes id="_date_time_8h" name="DateTime.h" local="yes" imported="no">lsst/daf/base/DateTime.h</includes>
    <includes id="_persistable_8h" name="Persistable.h" local="yes" imported="no">lsst/daf/base/Persistable.h</includes>
    <includes id="_property_set_8h" name="PropertySet.h" local="yes" imported="no">lsst/daf/base/PropertySet.h</includes>
    <includes id="_property_list_8h" name="PropertyList.h" local="yes" imported="no">lsst/daf/base/PropertyList.h</includes>
  </compound>
  <compound kind="file">
    <name>Citizen.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>_citizen_8h</filename>
    <class kind="class">lsst::daf::base::Citizen</class>
    <class kind="class">lsst::daf::base::PersistentCitizenScope</class>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::base</namespace>
  </compound>
  <compound kind="file">
    <name>DateTime.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>_date_time_8h</filename>
    <class kind="class">lsst::daf::base::DateTime</class>
    <namespace>boost</namespace>
    <namespace>boost::serialization</namespace>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::base</namespace>
  </compound>
  <compound kind="file">
    <name>Persistable.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>_persistable_8h</filename>
    <class kind="class">lsst::daf::base::Persistable</class>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::base</namespace>
    <namespace>lsst::daf::persistence</namespace>
    <namespace>boost</namespace>
    <namespace>boost::serialization</namespace>
    <member kind="define">
      <type>#define</type>
      <name>LSST_PERSIST_FORMATTER</name>
      <anchorfile>_persistable_8h.html</anchorfile>
      <anchor>af8b18e97e4c5b2ad16620df4a0a30b2f</anchor>
      <arglist>(formatter...)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>delegateSerialize</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>a43ecbaa829108901dace40c962c21937</anchor>
      <arglist>(Archive &amp;ar, unsigned int const version, lsst::daf::base::Persistable *persistable)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PropertyList.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>_property_list_8h</filename>
    <includes id="_property_set_8h" name="PropertySet.h" local="yes" imported="no">lsst/daf/base/PropertySet.h</includes>
    <class kind="class">lsst::daf::base::PropertyList</class>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::persistence</namespace>
    <namespace>lsst::daf::base</namespace>
  </compound>
  <compound kind="file">
    <name>PropertySet.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>_property_set_8h</filename>
    <includes id="_citizen_8h" name="Citizen.h" local="yes" imported="no">lsst/daf/base/Citizen.h</includes>
    <includes id="_persistable_8h" name="Persistable.h" local="yes" imported="no">lsst/daf/base/Persistable.h</includes>
    <class kind="class">lsst::daf::base::PropertySet</class>
    <class kind="class">lsst::daf::base::TypeMismatchException</class>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::persistence</namespace>
    <namespace>lsst::daf::base</namespace>
    <member kind="function">
      <type>void</type>
      <name>PropertySet::add&lt; PropertySet::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1base.html</anchorfile>
      <anchor>a18cf998d5e2e0ae2f0c167cc4a4358e6</anchor>
      <arglist>(std::string const &amp;name, Ptr const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PropertySet::add&lt; PropertySet::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1base.html</anchorfile>
      <anchor>a3629f1d95e4014c1edee79bf623c304b</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; Ptr &gt; const &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/</path>
    <filename>daf_2____init_____8py</filename>
    <namespace>lsst::daf</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1daf.html</anchorfile>
      <anchor>a090ed648173783b4b948e8dbc4010d0b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/base/</path>
    <filename>daf_2base_2____init_____8py</filename>
    <namespace>lsst::daf::base</namespace>
  </compound>
  <compound kind="file">
    <name>citizen.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/base/</path>
    <filename>citizen_8py</filename>
    <namespace>lsst::daf::base::citizen</namespace>
    <member kind="function">
      <type>def</type>
      <name>setCallbacks</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1citizen.html</anchorfile>
      <anchor>aada1d24a6a638ef4411ae8954bb095ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mortal</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1citizen.html</anchorfile>
      <anchor>ab3e9ea7e1e74d55e2cc11a8ed236104d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/base/</path>
    <filename>version_8py</filename>
    <namespace>lsst::daf::base::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a12216a0aea2dfd08a2b851de630b5502</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ab2917769106a2b1fa43474fd8b44a610</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ad70861a960b5750472d120600475dd31</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a9d636a2284d94efc0346aa9f25e9c64e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a4c8f7ab1a9c0c29be01e762b3fa41196</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a7c93d1d3bde18f2fc9527c4c099f2b38</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ac8540b76998b0c2889ac522158c3b7b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Citizen.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>_citizen_8cc</filename>
    <includes id="_citizen_8h" name="Citizen.h" local="yes" imported="no">lsst/daf/base/Citizen.h</includes>
    <class kind="class">CitizenInit</class>
    <member kind="function">
      <type>dafBase::Citizen::memId</type>
      <name>defaultNewCallback</name>
      <anchorfile>_citizen_8cc.html</anchorfile>
      <anchor>abc204ea8381f58327dfa0a69d14a140c</anchor>
      <arglist>(dafBase::Citizen::memId const cid)</arglist>
    </member>
    <member kind="function">
      <type>dafBase::Citizen::memId</type>
      <name>defaultDeleteCallback</name>
      <anchorfile>_citizen_8cc.html</anchorfile>
      <anchor>ae72410f5d20e48f02d78f97d550a4b72</anchor>
      <arglist>(dafBase::Citizen const *ptr)</arglist>
    </member>
    <member kind="function">
      <type>dafBase::Citizen::memId</type>
      <name>defaultCorruptionCallback</name>
      <anchorfile>_citizen_8cc.html</anchorfile>
      <anchor>a84134c1d60087931c2b914a9c2288c6b</anchor>
      <arglist>(dafBase::Citizen const *ptr)</arglist>
    </member>
    <member kind="variable">
      <type>CitizenInit</type>
      <name>one</name>
      <anchorfile>_citizen_8cc.html</anchorfile>
      <anchor>aac11928485c9aca7ff2365ad6f794f0c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DateTime.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>_date_time_8cc</filename>
    <includes id="_date_time_8h" name="DateTime.h" local="yes" imported="no">lsst/daf/base/DateTime.h</includes>
  </compound>
  <compound kind="file">
    <name>Persistable.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>_persistable_8cc</filename>
    <includes id="_persistable_8h" name="Persistable.h" local="yes" imported="no">lsst/daf/base/Persistable.h</includes>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::base</namespace>
  </compound>
  <compound kind="file">
    <name>PropertyList.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>_property_list_8cc</filename>
    <includes id="_property_list_8h" name="PropertyList.h" local="yes" imported="no">lsst/daf/base/PropertyList.h</includes>
    <includes id="_date_time_8h" name="DateTime.h" local="yes" imported="no">lsst/daf/base/DateTime.h</includes>
    <namespace>lsst::daf</namespace>
    <namespace>lsst::daf::base</namespace>
    <member kind="define">
      <type>#define</type>
      <name>INSTANTIATE</name>
      <anchorfile>_property_list_8cc.html</anchorfile>
      <anchor>a2f648363284552bcbc15f7356520106e</anchor>
      <arglist>(t)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PropertySet.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>_property_set_8cc</filename>
    <includes id="_property_set_8h" name="PropertySet.h" local="yes" imported="no">lsst/daf/base/PropertySet.h</includes>
    <includes id="_date_time_8h" name="DateTime.h" local="yes" imported="no">lsst/daf/base/DateTime.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>INSTANTIATE</name>
      <anchorfile>_property_set_8cc.html</anchorfile>
      <anchor>a2f648363284552bcbc15f7356520106e</anchor>
      <arglist>(t)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dafBase::PropertySet::add&lt; dafBase::PropertySet::Ptr &gt;</name>
      <anchorfile>_property_set_8cc.html</anchorfile>
      <anchor>adfb326cf982d007dbd5c2589c8cfb02c</anchor>
      <arglist>(std::string const &amp;name, Ptr const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>dafBase::PropertySet::add&lt; dafBase::PropertySet::Ptr &gt;</name>
      <anchorfile>_property_set_8cc.html</anchorfile>
      <anchor>a14b59620b34990ff459bc80e652c21b7</anchor>
      <arglist>(std::string const &amp;name, vector&lt; Ptr &gt; const &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CitizenInit</name>
    <filename>class_citizen_init.html</filename>
    <member kind="function">
      <type></type>
      <name>CitizenInit</name>
      <anchorfile>class_citizen_init.html</anchorfile>
      <anchor>aab70aafd676bce8d091fb577eb28f95f</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>boost</name>
    <filename>namespaceboost.html</filename>
    <namespace>boost::serialization</namespace>
  </compound>
  <compound kind="namespace">
    <name>boost::serialization</name>
    <filename>namespaceboost_1_1serialization.html</filename>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf</name>
    <filename>namespacelsst_1_1daf.html</filename>
    <namespace>lsst::daf::base</namespace>
    <namespace>lsst::daf::persistence</namespace>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1daf.html</anchorfile>
      <anchor>a090ed648173783b4b948e8dbc4010d0b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::base</name>
    <filename>namespacelsst_1_1daf_1_1base.html</filename>
    <namespace>lsst::daf::base::citizen</namespace>
    <namespace>lsst::daf::base::version</namespace>
    <class kind="class">lsst::daf::base::Citizen</class>
    <class kind="class">lsst::daf::base::PersistentCitizenScope</class>
    <class kind="class">lsst::daf::base::DateTime</class>
    <class kind="class">lsst::daf::base::Persistable</class>
    <class kind="class">lsst::daf::base::PropertyList</class>
    <class kind="class">lsst::daf::base::PropertySet</class>
    <class kind="class">lsst::daf::base::TypeMismatchException</class>
    <member kind="function">
      <type>void</type>
      <name>PropertySet::add&lt; PropertySet::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1base.html</anchorfile>
      <anchor>a18cf998d5e2e0ae2f0c167cc4a4358e6</anchor>
      <arglist>(std::string const &amp;name, Ptr const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>PropertySet::add&lt; PropertySet::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1base.html</anchorfile>
      <anchor>a3629f1d95e4014c1edee79bf623c304b</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; Ptr &gt; const &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::Citizen</name>
    <filename>classlsst_1_1daf_1_1base_1_1_citizen.html</filename>
    <member kind="enumvalue">
      <name>magicSentinel</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa7cae8a5128f585d5235f597b0688755abc0462db499d6483b4efc7cad5a60c76</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long</type>
      <name>memId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab8fb6fab88116695ea61c6dff1c41a5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a077e8ead49f6cae5ecbf0e19bbb2a914</anchor>
      <arglist>)(const memId cid)</arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a29a93b421e7a748de6eb250bf8a16d99</anchor>
      <arglist>)(const Citizen *ptr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a5e45083f72154b6b0e9e05aef3050a3a</anchor>
      <arglist>(const std::type_info &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a989ed2a2668a9e73cbb1ba1673fd5424</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa57c4af31185de9121685a21e4312e18</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Citizen &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a54c0ab33a317803a4752b92fd4fdaf28</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>repr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6b3051d5dcd2a7692b4d663dc18a7268</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a3a23b55c5a05821c35bcd1755c5014c6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>memId</type>
      <name>getId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a242b51d329007b8695f144c5a5c556f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>hasBeenCorrupted</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa33c60cf54bc7261f591ddb6b6bf1e75</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>getNextMemId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a99bc987832370ba15fd12b33ab8162ed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>init</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab3db6d88ff76207207f94df57aaa05f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aedf53c4a26cb3ac4b381d431c7b939fc</anchor>
      <arglist>(int, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ace380c15bb61bc22467708f71f34b40a</anchor>
      <arglist>(std::ostream &amp;stream, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const std::vector&lt; const Citizen * &gt; *</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a064ba850b1d7ad4ac897025ce133622d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setNewCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>acdeed07dab21d09bad8ba4b4fc200192</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setDeleteCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6db30f6a86d8f9db26c6e10f8feb5ff7</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>PersistentCitizenScope</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a3c8b0fa24fdb69fef1538224d7140bc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memNewCallback</type>
      <name>setNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a0c6863610ddaad17698135997a511633</anchor>
      <arglist>(memNewCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setDeleteCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a20d574f2a7e7aa132fae29fde6055774</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setCorruptionCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a92c06d65b54cb1d12d81eaf330294840</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::PersistentCitizenScope</name>
    <filename>classlsst_1_1daf_1_1base_1_1_persistent_citizen_scope.html</filename>
    <member kind="function">
      <type></type>
      <name>PersistentCitizenScope</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistent_citizen_scope.html</anchorfile>
      <anchor>a908f1c2d200d4b577aecf9e211125bd4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~PersistentCitizenScope</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistent_citizen_scope.html</anchorfile>
      <anchor>a06a1228ec7e5dfbd85e0893f7e2f25fa</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::DateTime</name>
    <filename>classlsst_1_1daf_1_1base_1_1_date_time.html</filename>
    <member kind="enumeration">
      <name>Timescale</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a4afa527df52836c7d5e15257cbac652a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TAI</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a4afa527df52836c7d5e15257cbac652aa2f2cfafbacfc1565bd266a9c2c1002c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UTC</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a4afa527df52836c7d5e15257cbac652aad2e555d24069a0f0ff2341a52f657714</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>TT</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a4afa527df52836c7d5e15257cbac652aac164f6a6f237ef54541dd72184560e47</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <name>DateSystem</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>abb9afc3c0c8b4060256e80d2dab2c8d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>JD</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>abb9afc3c0c8b4060256e80d2dab2c8d7abfd35318d1872db34f820c6a2395cedf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>MJD</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>abb9afc3c0c8b4060256e80d2dab2c8d7abbfb6326e997647e92029bbeb05bf5c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>EPOCH</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>abb9afc3c0c8b4060256e80d2dab2c8d7a35fec840742ba8db94fbd5745cc8befa</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DateTime</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a198ebf4193bc763113c55fa19c7f835e</anchor>
      <arglist>(long long nsecs=0LL, Timescale scale=TAI)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DateTime</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a0aa2944c88ca1fc119d616a0539f361b</anchor>
      <arglist>(double date, DateSystem system=MJD, Timescale scale=TAI)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DateTime</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>ac4d69f912ac8a0c79d167aee3f80c525</anchor>
      <arglist>(int year, int month, int day, int hr, int min, int sec, Timescale scale=TAI)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DateTime</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a12cf4d8b2fb0524ecd4237eef2a03686</anchor>
      <arglist>(std::string const &amp;iso8601)</arglist>
    </member>
    <member kind="function">
      <type>long long</type>
      <name>nsecs</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a9305808944a765835fac48417bd010de</anchor>
      <arglist>(Timescale scale=TAI) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>mjd</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>ac2a56a0f720d9091a7e9b1ce3208131a</anchor>
      <arglist>(Timescale scale=TAI) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a431c8fd836931518720d0eabf1a07927</anchor>
      <arglist>(DateSystem system=MJD, Timescale scale=TAI) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>af5ac2b47a2fab7850980b650ab2652b9</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>struct tm</type>
      <name>gmtime</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a20795f010bdf4c1d8881d6ef66c40a99</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>struct timespec</type>
      <name>timespec</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a1b45e435ea37b0a809b7ba58a4300f66</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>struct timeval</type>
      <name>timeval</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>aadbb2005189083303d5a9769e40e41c7</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static DateTime</type>
      <name>now</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>a52551934bbd12bde412e5b81fd9e1602</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>initializeLeapSeconds</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>aaaaef4f3fe48445c96a17007df5b08db</anchor>
      <arglist>(std::string const &amp;leapString)</arglist>
    </member>
    <member kind="friend">
      <type>friend class</type>
      <name>boost::serialization::access</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_date_time.html</anchorfile>
      <anchor>ac98d07dd8f7b70e16ccb9a01abf56b9c</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::Persistable</name>
    <filename>classlsst_1_1daf_1_1base_1_1_persistable.html</filename>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Persistable &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aa2696e131f20579ac52a9d15cb5776e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>a391f72f548131adc6e6fe223f2eb9e94</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aff869948a83d96699a4d5a9d1c2539c5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>serialize</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aef69adcf0ecffc7aece0f9733ced96ab</anchor>
      <arglist>(Archive &amp;, unsigned int const)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::PropertyList</name>
    <filename>classlsst_1_1daf_1_1base_1_1_property_list.html</filename>
    <base>lsst::daf::base::PropertySet</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertyList &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>abf577e70597e27534899e3d5c33d5a22</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertyList const  &gt;</type>
      <name>ConstPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>af40a69dcbf3ad6b290b5410a69fd6da9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertySet &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>aa5e5ef5b25f4fffc090a25482809adc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertySet const  &gt;</type>
      <name>ConstPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a37cd5b01b01af996b13499467b1ec365</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Persistable &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aa2696e131f20579ac52a9d15cb5776e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>magicSentinel</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa7cae8a5128f585d5235f597b0688755abc0462db499d6483b4efc7cad5a60c76</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long</type>
      <name>memId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab8fb6fab88116695ea61c6dff1c41a5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a077e8ead49f6cae5ecbf0e19bbb2a914</anchor>
      <arglist>)(const memId cid)</arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a29a93b421e7a748de6eb250bf8a16d99</anchor>
      <arglist>)(const Citizen *ptr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PropertyList</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>af23ec29db00ba05cd307dd606eaf4268</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PropertyList</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a67cdd50af2efb9c6fc0a0c67a2800402</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PropertySet::Ptr</type>
      <name>deepCopy</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>ae08ba1962ce00cdb41e3a49d021440ad</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a49cd5257a9351a6dd41b2846516ed504</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a767a76e63ed962989a137940eec3e6b3</anchor>
      <arglist>(std::string const &amp;name, T const &amp;defaultValue) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; T &gt;</type>
      <name>getArray</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aa440f8e827f5337eb39e53a13331ef50</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>getComment</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a64230da0ec60a546a2f25be8969a36a2</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>getOrderedNames</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aee0170254e0eff5a2b8ef539dd394167</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>std::list&lt; std::string &gt;::const_iterator</type>
      <name>begin</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a09f5bea5ff04bc433b863e2954185bcb</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>std::list&lt; std::string &gt;::const_iterator</type>
      <name>end</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a5c642730800bdb8f0520650c6d86a4f0</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a64d4bf010f4814173227cc5f8d92c663</anchor>
      <arglist>(bool topLevelOnly=false, std::string const &amp;indent=&quot;&quot;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>ab21a28bab1ba62a5f1ecf098d57a76c7</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aeb3dc8ab7db4563f69e82a8d970bbf5c</anchor>
      <arglist>(std::string const &amp;name, PropertySet::Ptr const &amp;value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a5201267542a3a58fb578f1599bf16c1d</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a99e9e733a836493840e00318b46b4ad1</anchor>
      <arglist>(std::string const &amp;name, char const *value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a8881cb856922d33f062c5561b7a9a09b</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a781df26b0410a38c5817b75889dd273f</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a02988bb48a4ade81762e360887d9dc5b</anchor>
      <arglist>(std::string const &amp;name, char const *value, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a119191a8670c5bd318488214e8cc7d56</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>ada4b05f6f43a1d9d36edbed10087dcb8</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>af05df6d7cf67c565b8bfa11d2911266f</anchor>
      <arglist>(std::string const &amp;name, char const *value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a81ffc9d08c6b05095c4655472c9a1667</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a1606741e14260cd1d2b5d3a579e6c8b3</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aa5ee49e63cece12e4a8354123cc6d52c</anchor>
      <arglist>(std::string const &amp;name, char const *value, std::string const &amp;comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a5db9c5141a35918f02268536009df09b</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a60d9e2d1c211ad0a32a98fd605bc1a81</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a5e567a53ed62bfd5c99836fe8e5929fd</anchor>
      <arglist>(std::string const &amp;name, char const *value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a600aec2f71db06d5c8d076a3281519ce</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aa9391c48a683f533ce4cf50839e47394</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>ab7289083ccaa9084abe8eb9fd38c3e2d</anchor>
      <arglist>(std::string const &amp;name, char const *value, char const *comment, bool inPlace=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>copy</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>acfa1343b3279f379fb099e52e2680241</anchor>
      <arglist>(std::string const &amp;dest, PropertySet::ConstPtr source, std::string const &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>combine</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>aa68eb4b7d9cbafdb24564a086770e09e</anchor>
      <arglist>(PropertySet::ConstPtr source)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>copy</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>af6a2983cc6b82cf8f927024ccde64a07</anchor>
      <arglist>(std::string const &amp;dest, PropertySet::ConstPtr source, std::string const &amp;name, bool inPlace)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>combine</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a473022d75bc4bf84206bbac850ba430a</anchor>
      <arglist>(PropertySet::ConstPtr source, bool inPlace)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>remove</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_list.html</anchorfile>
      <anchor>a79f1ca3f4cd757e151ea4c2be40edb33</anchor>
      <arglist>(std::string const &amp;name)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PropertySet</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a8627fa3263fd5badddbf719c7f90fee0</anchor>
      <arglist>(bool flat=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PropertySet</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a102f1aab603c3af1514b37ad7573c536</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>nameCount</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a47e556628d062b6ed20017c1233c551b</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>names</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a82782adc11a89aec7c9bbe46101c93eb</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>paramNames</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>abe9879aeaeb053380484d6c2ebfd97c6</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>propertySetNames</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a2da5de5eaac136470599143205bedf84</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a743ebc5d903029533144defc130c4f2e</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isArray</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a3abab085911e315ef8dc5f4e2cfcc5d8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPropertySetPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ac0d29e128c43a89673e1bd63e38419f4</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a1bca4c52acac16820dbc528f68074cc8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::type_info const &amp;</type>
      <name>typeOf</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ad3a980e3d77b53d2d22f430cae538ba8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>abe9d337893eb893d3956b18a650faa07</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a2971e3ef1a9a57a5663f1feae568b832</anchor>
      <arglist>(std::string const &amp;name, T const &amp;defaultValue) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; T &gt;</type>
      <name>getArray</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a5eeb3f31abc944d410e9d200d8b46b4a</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getAsBool</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>af763adba53833fa57f8e392e34636829</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getAsInt</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a139cf1f317bb9e31d5918a9e3c4cf12d</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int64_t</type>
      <name>getAsInt64</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ad6baaee12a0ed770bb6a715bb305343a</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getAsDouble</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a4d2ef1656d8449659869f5335b022b06</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getAsString</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a5de937d0154a1c453b550fa79fe5a6b0</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>PropertySet::Ptr</type>
      <name>getAsPropertySetPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ade48ada2381d8cc2345ee82dceeaacb4</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Persistable::Ptr</type>
      <name>getAsPersistablePtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a9c17557a872cf6c3a95383adad1c27fb</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a71f9fb8a19d07335f8f853a85e673034</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>af2c15af693c300146a51147e500e0789</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ac4a3b2670e1fe44dc2ce0b4372ac7be7</anchor>
      <arglist>(std::string const &amp;name, char const *value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ae4209d29aabff0337be45ff94754aa0b</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a39b2bac569e5b79f8f9bfb71c0ae4514</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ab09c682c8fad10d1c874a226771f5b8a</anchor>
      <arglist>(std::string const &amp;name, char const *value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>a391f72f548131adc6e6fe223f2eb9e94</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aff869948a83d96699a4d5a9d1c2539c5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>serialize</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aef69adcf0ecffc7aece0f9733ced96ab</anchor>
      <arglist>(Archive &amp;, unsigned int const)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a5e45083f72154b6b0e9e05aef3050a3a</anchor>
      <arglist>(const std::type_info &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a989ed2a2668a9e73cbb1ba1673fd5424</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa57c4af31185de9121685a21e4312e18</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Citizen &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a54c0ab33a317803a4752b92fd4fdaf28</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>repr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6b3051d5dcd2a7692b4d663dc18a7268</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a3a23b55c5a05821c35bcd1755c5014c6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>memId</type>
      <name>getId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a242b51d329007b8695f144c5a5c556f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>hasBeenCorrupted</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa33c60cf54bc7261f591ddb6b6bf1e75</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>getNextMemId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a99bc987832370ba15fd12b33ab8162ed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>init</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab3db6d88ff76207207f94df57aaa05f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aedf53c4a26cb3ac4b381d431c7b939fc</anchor>
      <arglist>(int, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ace380c15bb61bc22467708f71f34b40a</anchor>
      <arglist>(std::ostream &amp;stream, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const std::vector&lt; const Citizen * &gt; *</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a064ba850b1d7ad4ac897025ce133622d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setNewCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>acdeed07dab21d09bad8ba4b4fc200192</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setDeleteCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6db30f6a86d8f9db26c6e10f8feb5ff7</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memNewCallback</type>
      <name>setNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a0c6863610ddaad17698135997a511633</anchor>
      <arglist>(memNewCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setDeleteCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a20d574f2a7e7aa132fae29fde6055774</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setCorruptionCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a92c06d65b54cb1d12d81eaf330294840</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>aa45513d99aa9d5ddc9e30ce7cdf06c9c</anchor>
      <arglist>(std::string const &amp;name, boost::shared_ptr&lt; std::vector&lt; boost::any &gt; &gt; vp)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual std::string</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a8bc7a87e0cec43559c4cdf13f64482ee</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::PropertySet</name>
    <filename>classlsst_1_1daf_1_1base_1_1_property_set.html</filename>
    <base>lsst::daf::base::Persistable</base>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertySet &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>aa5e5ef5b25f4fffc090a25482809adc7</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PropertySet const  &gt;</type>
      <name>ConstPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a37cd5b01b01af996b13499467b1ec365</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Persistable &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aa2696e131f20579ac52a9d15cb5776e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>magicSentinel</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa7cae8a5128f585d5235f597b0688755abc0462db499d6483b4efc7cad5a60c76</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>unsigned long</type>
      <name>memId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab8fb6fab88116695ea61c6dff1c41a5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a077e8ead49f6cae5ecbf0e19bbb2a914</anchor>
      <arglist>)(const memId cid)</arglist>
    </member>
    <member kind="typedef">
      <type>memId(*</type>
      <name>memCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a29a93b421e7a748de6eb250bf8a16d99</anchor>
      <arglist>)(const Citizen *ptr)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PropertySet</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a8627fa3263fd5badddbf719c7f90fee0</anchor>
      <arglist>(bool flat=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PropertySet</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a102f1aab603c3af1514b37ad7573c536</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Ptr</type>
      <name>deepCopy</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a19a99e37390831de9263566f17c2c2c0</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>nameCount</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a47e556628d062b6ed20017c1233c551b</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>names</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a82782adc11a89aec7c9bbe46101c93eb</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>paramNames</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>abe9879aeaeb053380484d6c2ebfd97c6</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>propertySetNames</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a2da5de5eaac136470599143205bedf84</anchor>
      <arglist>(bool topLevelOnly=true) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a743ebc5d903029533144defc130c4f2e</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isArray</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a3abab085911e315ef8dc5f4e2cfcc5d8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPropertySetPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ac0d29e128c43a89673e1bd63e38419f4</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a1bca4c52acac16820dbc528f68074cc8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::type_info const &amp;</type>
      <name>typeOf</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ad3a980e3d77b53d2d22f430cae538ba8</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>abe9d337893eb893d3956b18a650faa07</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a2971e3ef1a9a57a5663f1feae568b832</anchor>
      <arglist>(std::string const &amp;name, T const &amp;defaultValue) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; T &gt;</type>
      <name>getArray</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a5eeb3f31abc944d410e9d200d8b46b4a</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getAsBool</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>af763adba53833fa57f8e392e34636829</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getAsInt</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a139cf1f317bb9e31d5918a9e3c4cf12d</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int64_t</type>
      <name>getAsInt64</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ad6baaee12a0ed770bb6a715bb305343a</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getAsDouble</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a4d2ef1656d8449659869f5335b022b06</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getAsString</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a5de937d0154a1c453b550fa79fe5a6b0</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>PropertySet::Ptr</type>
      <name>getAsPropertySetPtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ade48ada2381d8cc2345ee82dceeaacb4</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Persistable::Ptr</type>
      <name>getAsPersistablePtr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a9c17557a872cf6c3a95383adad1c27fb</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a76333a90d6e82e1954942103fb28e6f1</anchor>
      <arglist>(bool topLevelOnly=false, std::string const &amp;indent=&quot;&quot;) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a71f9fb8a19d07335f8f853a85e673034</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>af2c15af693c300146a51147e500e0789</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ac4a3b2670e1fe44dc2ce0b4372ac7be7</anchor>
      <arglist>(std::string const &amp;name, char const *value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ae4209d29aabff0337be45ff94754aa0b</anchor>
      <arglist>(std::string const &amp;name, T const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a39b2bac569e5b79f8f9bfb71c0ae4514</anchor>
      <arglist>(std::string const &amp;name, std::vector&lt; T &gt; const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ab09c682c8fad10d1c874a226771f5b8a</anchor>
      <arglist>(std::string const &amp;name, char const *value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>copy</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>ada7b08632bb092e8fcb0e45958c55a2c</anchor>
      <arglist>(std::string const &amp;dest, ConstPtr source, std::string const &amp;name)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>combine</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>aa6cc5bd59b79dda999113feb1f60cbc6</anchor>
      <arglist>(ConstPtr source)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>remove</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a4f5a11cebd5ab4e664b4715093459e9a</anchor>
      <arglist>(std::string const &amp;name)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>a391f72f548131adc6e6fe223f2eb9e94</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Persistable</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aff869948a83d96699a4d5a9d1c2539c5</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>serialize</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_persistable.html</anchorfile>
      <anchor>aef69adcf0ecffc7aece0f9733ced96ab</anchor>
      <arglist>(Archive &amp;, unsigned int const)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a5e45083f72154b6b0e9e05aef3050a3a</anchor>
      <arglist>(const std::type_info &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a989ed2a2668a9e73cbb1ba1673fd5424</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Citizen</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa57c4af31185de9121685a21e4312e18</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Citizen &amp;</type>
      <name>operator=</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a54c0ab33a317803a4752b92fd4fdaf28</anchor>
      <arglist>(Citizen const &amp;)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>repr</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6b3051d5dcd2a7692b4d663dc18a7268</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>markPersistent</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a3a23b55c5a05821c35bcd1755c5014c6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>memId</type>
      <name>getId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a242b51d329007b8695f144c5a5c556f2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_set</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>af1398e7530d6848ded081656fb7f740c</anchor>
      <arglist>(std::string const &amp;name, boost::shared_ptr&lt; std::vector&lt; boost::any &gt; &gt; vp)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>_add</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>aa45513d99aa9d5ddc9e30ce7cdf06c9c</anchor>
      <arglist>(std::string const &amp;name, boost::shared_ptr&lt; std::vector&lt; boost::any &gt; &gt; vp)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual std::string</type>
      <name>_format</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_property_set.html</anchorfile>
      <anchor>a8bc7a87e0cec43559c4cdf13f64482ee</anchor>
      <arglist>(std::string const &amp;name) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>hasBeenCorrupted</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aa33c60cf54bc7261f591ddb6b6bf1e75</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>getNextMemId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a99bc987832370ba15fd12b33ab8162ed</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>init</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ab3db6d88ff76207207f94df57aaa05f0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>aedf53c4a26cb3ac4b381d431c7b939fc</anchor>
      <arglist>(int, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>ace380c15bb61bc22467708f71f34b40a</anchor>
      <arglist>(std::ostream &amp;stream, memId startingMemId=0)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static const std::vector&lt; const Citizen * &gt; *</type>
      <name>census</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a064ba850b1d7ad4ac897025ce133622d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setNewCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>acdeed07dab21d09bad8ba4b4fc200192</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memId</type>
      <name>setDeleteCallbackId</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a6db30f6a86d8f9db26c6e10f8feb5ff7</anchor>
      <arglist>(memId id)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memNewCallback</type>
      <name>setNewCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a0c6863610ddaad17698135997a511633</anchor>
      <arglist>(memNewCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setDeleteCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a20d574f2a7e7aa132fae29fde6055774</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static memCallback</type>
      <name>setCorruptionCallback</name>
      <anchorfile>classlsst_1_1daf_1_1base_1_1_citizen.html</anchorfile>
      <anchor>a92c06d65b54cb1d12d81eaf330294840</anchor>
      <arglist>(memCallback func)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::base::TypeMismatchException</name>
    <filename>classlsst_1_1daf_1_1base_1_1_type_mismatch_exception.html</filename>
    <base>lsst::pex::exceptions::LogicErrorException</base>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::base::citizen</name>
    <filename>namespacelsst_1_1daf_1_1base_1_1citizen.html</filename>
    <member kind="function">
      <type>def</type>
      <name>setCallbacks</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1citizen.html</anchorfile>
      <anchor>aada1d24a6a638ef4411ae8954bb095ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>mortal</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1citizen.html</anchorfile>
      <anchor>ab3e9ea7e1e74d55e2cc11a8ed236104d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::base::version</name>
    <filename>namespacelsst_1_1daf_1_1base_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a12216a0aea2dfd08a2b851de630b5502</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ab2917769106a2b1fa43474fd8b44a610</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ad70861a960b5750472d120600475dd31</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a9d636a2284d94efc0346aa9f25e9c64e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a4c8f7ab1a9c0c29be01e762b3fa41196</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>a7c93d1d3bde18f2fc9527c4c099f2b38</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1daf_1_1base_1_1version.html</anchorfile>
      <anchor>ac8540b76998b0c2889ac522158c3b7b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence</name>
    <filename>namespacelsst_1_1daf_1_1persistence.html</filename>
    <member kind="function">
      <type>void</type>
      <name>delegateSerialize</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>a43ecbaa829108901dace40c962c21937</anchor>
      <arglist>(Archive &amp;ar, unsigned int const version, lsst::daf::base::Persistable *persistable)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>python/lsst/daf/base</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/base/</path>
    <filename>dir_b6caa57ef59be2cb0e78c5bb2ae4740f.html</filename>
    <file>__init__.py</file>
    <file>citizen.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/daf/base</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/base/</path>
    <filename>dir_e4c0cd0d6612584ed8c39b6f5c3db73d.html</filename>
    <file>Citizen.h</file>
    <file>DateTime.h</file>
    <file>Persistable.h</file>
    <file>PropertyList.h</file>
    <file>PropertySet.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/daf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/daf/</path>
    <filename>dir_a5af4eb02f8a2eeb55a6bfc74ef0b012.html</filename>
    <dir>python/lsst/daf/base</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/daf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/daf/</path>
    <filename>dir_956e4256259974f8f135b20840595bd9.html</filename>
    <dir>include/lsst/daf/base</dir>
    <file>base.h</file>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/daf</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/daf</dir>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_base-7.1.1.0+1/daf_base-7.1.1.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>Citizen.cc</file>
    <file>DateTime.cc</file>
    <file>Persistable.cc</file>
    <file>PropertyList.cc</file>
    <file>PropertySet.cc</file>
  </compound>
</tagfile>
