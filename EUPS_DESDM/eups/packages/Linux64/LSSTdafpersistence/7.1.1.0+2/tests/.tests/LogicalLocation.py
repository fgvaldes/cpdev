tests/LogicalLocation.py

     daf.persistence.LogicalLocation DEBUG: Input string: %(foo)xx
     daf.persistence.LogicalLocation DEBUG: Key: foo
     daf.persistence.LogicalLocation DEBUG: Map Val: bar
     daf.persistence.LogicalLocation DEBUG: Result: barxx
     daf.persistence.LogicalLocation DEBUG: Input string: %(x)foo
     daf.persistence.LogicalLocation DEBUG: Key: x
     daf.persistence.LogicalLocation DEBUG: Map Val: 3
     daf.persistence.LogicalLocation DEBUG: Result: 3foo
     daf.persistence.LogicalLocation DEBUG: Input string: yy-1548456656(x)yy
     daf.persistence.LogicalLocation DEBUG: Key: x
     daf.persistence.LogicalLocation DEBUG: Map Val: 3
     daf.persistence.LogicalLocation DEBUG: Result: yy0003yy
     daf.persistence.LogicalLocation DEBUG: Input string: %(foo)%(x)%(y)
     daf.persistence.LogicalLocation DEBUG: Key: foo
     daf.persistence.LogicalLocation DEBUG: Map Val: bar
     daf.persistence.LogicalLocation DEBUG: Key: x
     daf.persistence.LogicalLocation DEBUG: Map Val: 3
     daf.persistence.LogicalLocation DEBUG: Key: y
     daf.persistence.LogicalLocation DEBUG: Map Val: 2009
     daf.persistence.LogicalLocation DEBUG: Result: bar32009
     daf.persistence.LogicalLocation DEBUG: Input string: %(foo)-1548456656(y)
     daf.persistence.LogicalLocation DEBUG: Key: foo
     daf.persistence.LogicalLocation DEBUG: Map Val: baz
     daf.persistence.LogicalLocation DEBUG: Key: y
     daf.persistence.LogicalLocation DEBUG: Map Val: 2009
     daf.persistence.LogicalLocation DEBUG: Result: baz2009
.
----------------------------------------------------------------------
Ran 1 test in 0.004s

OK
