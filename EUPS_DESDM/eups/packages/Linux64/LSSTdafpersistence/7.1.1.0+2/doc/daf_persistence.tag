<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>persistence.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/</path>
    <filename>persistence_8h</filename>
    <includes id="_boost_storage_8h" name="BoostStorage.h" local="no" imported="no">lsst/daf/persistence/BoostStorage.h</includes>
    <includes id="_db_auth_8h" name="DbAuth.h" local="no" imported="no">lsst/daf/persistence/DbAuth.h</includes>
    <includes id="_db_storage_8h" name="DbStorage.h" local="no" imported="no">lsst/daf/persistence/DbStorage.h</includes>
    <includes id="_db_tsv_storage_8h" name="DbTsvStorage.h" local="no" imported="no">lsst/daf/persistence/DbTsvStorage.h</includes>
    <includes id="_fits_storage_8h" name="FitsStorage.h" local="no" imported="no">lsst/daf/persistence/FitsStorage.h</includes>
    <includes id="_formatter_8h" name="Formatter.h" local="no" imported="no">lsst/daf/persistence/Formatter.h</includes>
    <includes id="_formatter_impl_8h" name="FormatterImpl.h" local="no" imported="no">lsst/daf/persistence/FormatterImpl.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="no" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <includes id="_persistence_8h" name="Persistence.h" local="no" imported="no">lsst/daf/persistence/Persistence.h</includes>
    <includes id="_xml_storage_8h" name="XmlStorage.h" local="no" imported="no">lsst/daf/persistence/XmlStorage.h</includes>
  </compound>
  <compound kind="file">
    <name>BoostStorage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_boost_storage_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::BoostStorage</class>
  </compound>
  <compound kind="file">
    <name>DbAuth.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_db_auth_8h</filename>
    <class kind="class">lsst::daf::persistence::DbAuth</class>
  </compound>
  <compound kind="file">
    <name>DbStorage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_db_storage_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::DbStorage</class>
  </compound>
  <compound kind="file">
    <name>DbStorageImpl.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_db_storage_impl_8h</filename>
    <class kind="class">lsst::daf::persistence::BoundVar</class>
    <class kind="class">lsst::daf::persistence::DbStorageImpl</class>
    <member kind="function">
      <type>void</type>
      <name>DbStorageImpl::setColumn&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>ac60279724e371e1045a153312537be27</anchor>
      <arglist>(std::string const &amp;columnName, std::string const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>DbStorageImpl::setColumn&lt; dafBase::DateTime &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>a43c7bb69be2355bbb49a0418f1b8db36</anchor>
      <arglist>(std::string const &amp;columnName, dafBase::DateTime const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>DbStorageImpl::outParam&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>abfed281ea132d2f1b53557a739dd642f</anchor>
      <arglist>(std::string const &amp;columnName, std::string *location, bool isExpr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>DbStorageImpl::outParam&lt; dafBase::DateTime &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>ac29ffaa9e42ec30a0e87dda8df583ef5</anchor>
      <arglist>(std::string const &amp;columnName, dafBase::DateTime *location, bool isExpr)</arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>DbStorageImpl::getColumnByPos&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>a71b2c5282613a108528f5e0d82a76220</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function">
      <type>dafBase::DateTime const &amp;</type>
      <name>DbStorageImpl::getColumnByPos&lt; dafBase::DateTime &gt;</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence.html</anchorfile>
      <anchor>aa3ab3c243f574baf58dd554bbc87348e</anchor>
      <arglist>(int pos)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbStorageLocation.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_db_storage_location_8h</filename>
    <includes id="_db_auth_8h" name="DbAuth.h" local="yes" imported="no">lsst/daf/persistence/DbAuth.h</includes>
    <class kind="class">lsst::daf::persistence::DbStorageLocation</class>
  </compound>
  <compound kind="file">
    <name>DbTsvStorage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_db_tsv_storage_8h</filename>
    <includes id="_db_storage_8h" name="DbStorage.h" local="yes" imported="no">lsst/daf/persistence/DbStorage.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <class kind="class">lsst::daf::persistence::DbTsvStorage</class>
  </compound>
  <compound kind="file">
    <name>FitsStorage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_fits_storage_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::FitsStorage</class>
  </compound>
  <compound kind="file">
    <name>Formatter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_formatter_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::Formatter</class>
    <class kind="class">lsst::daf::persistence::FormatterRegistration</class>
  </compound>
  <compound kind="file">
    <name>FormatterImpl.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_formatter_impl_8h</filename>
  </compound>
  <compound kind="file">
    <name>FormatterRegistry.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_formatter_registry_8h</filename>
    <includes id="_formatter_8h" name="Formatter.h" local="yes" imported="no">lsst/daf/persistence/Formatter.h</includes>
    <class kind="class">lsst::daf::persistence::FormatterRegistry</class>
  </compound>
  <compound kind="file">
    <name>LogicalLocation.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_logical_location_8h</filename>
    <class kind="class">lsst::daf::persistence::LogicalLocation</class>
  </compound>
  <compound kind="file">
    <name>Persistence.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_persistence_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::Persistence</class>
  </compound>
  <compound kind="file">
    <name>PropertySetFormatter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>include_2lsst_2daf_2persistence_2_property_set_formatter_8cc</filename>
  </compound>
  <compound kind="file">
    <name>PropertySetFormatter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>src_2_property_set_formatter_8cc</filename>
    <includes id="_property_set_formatter_8h" name="PropertySetFormatter.h" local="yes" imported="no">lsst/daf/persistence/PropertySetFormatter.h</includes>
    <includes id="_formatter_impl_8h" name="FormatterImpl.h" local="yes" imported="no">lsst/daf/persistence/FormatterImpl.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <includes id="_boost_storage_8h" name="BoostStorage.h" local="yes" imported="no">lsst/daf/persistence/BoostStorage.h</includes>
    <includes id="_db_storage_8h" name="DbStorage.h" local="yes" imported="no">lsst/daf/persistence/DbStorage.h</includes>
    <includes id="_xml_storage_8h" name="XmlStorage.h" local="yes" imported="no">lsst/daf/persistence/XmlStorage.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>src_2_property_set_formatter_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>EXEC_TRACE</name>
      <anchorfile>src_2_property_set_formatter_8cc.html</anchorfile>
      <anchor>af94301043c14564ca8b47e81f01004b0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PropertySetFormatter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_property_set_formatter_8h</filename>
    <includes id="_formatter_8h" name="Formatter.h" local="no" imported="no">lsst/daf/persistence/Formatter.h</includes>
    <includes id="_storage_8h" name="Storage.h" local="no" imported="no">lsst/daf/persistence/Storage.h</includes>
    <includes id="include_2lsst_2daf_2persistence_2_property_set_formatter_8cc" name="PropertySetFormatter.cc" local="yes" imported="no">PropertySetFormatter.cc</includes>
    <class kind="class">lsst::daf::persistence::PropertySetFormatter</class>
  </compound>
  <compound kind="file">
    <name>Storage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_storage_8h</filename>
    <class kind="class">lsst::daf::persistence::Storage</class>
  </compound>
  <compound kind="file">
    <name>StorageRegistry.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_storage_registry_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::StorageRegistry</class>
  </compound>
  <compound kind="file">
    <name>XmlStorage.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>_xml_storage_8h</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <class kind="class">lsst::daf::persistence::XmlStorage</class>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/</path>
    <filename>daf_2____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1daf.html</anchorfile>
      <anchor>a090ed648173783b4b948e8dbc4010d0b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>daf_2persistence_2____init_____8py</filename>
  </compound>
  <compound kind="file">
    <name>butler.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>butler_8py</filename>
    <class kind="class">lsst::daf::persistence::butler::Butler</class>
    <namespace>lsst::daf::persistence::butler</namespace>
  </compound>
  <compound kind="file">
    <name>butlerFactory.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>butler_factory_8py</filename>
    <class kind="class">lsst::daf::persistence::butlerFactory::ButlerFactory</class>
    <namespace>lsst::daf::persistence::butlerFactory</namespace>
  </compound>
  <compound kind="file">
    <name>butlerLocation.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>butler_location_8py</filename>
    <class kind="class">lsst::daf::persistence::butlerLocation::ButlerLocation</class>
    <namespace>lsst::daf::persistence::butlerLocation</namespace>
  </compound>
  <compound kind="file">
    <name>butlerSubset.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>butler_subset_8py</filename>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerSubset</class>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerSubsetIterator</class>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerDataRef</class>
    <namespace>lsst::daf::persistence::butlerSubset</namespace>
  </compound>
  <compound kind="file">
    <name>mapper.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>mapper_8py</filename>
    <class kind="class">lsst::daf::persistence::mapper::Mapper</class>
    <namespace>lsst::daf::persistence::mapper</namespace>
  </compound>
  <compound kind="file">
    <name>readProxy.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>read_proxy_8py</filename>
    <class kind="class">lsst::daf::persistence::readProxy::ReadProxy</class>
    <namespace>lsst::daf::persistence::readProxy</namespace>
    <member kind="function">
      <type>def</type>
      <name>__subject__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>af95e46a32899cef332637efc706be2d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>get_callback</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>ac16c08f0ede43fef2bca8a5b8ad658c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>set_callback</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>ab4813ef01c046f99ef53eb1e24d206b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>get_cache</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>af22cdf0ad019921719248ba62bc6015d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>set_cache</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>a3e73f42b051f0991d08ca5aee851f3a6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>version_8py</filename>
    <namespace>lsst::daf::persistence::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>afcd10f700e0c2a5be5e5e85a178c3292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a52ee33a80ad0cb0fb9375195afaff1f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a257375b7ef6c2c7d96f17c649e01599a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>ac9ba93015de53ae850807390b3937c37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a4306565e67a01d827a9c7362cc3e1560</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>abf53907879899a81c03a07c45db2141b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>ae34ecfc865605e5e514e793a6700a5af</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>BoostStorage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_boost_storage_8cc</filename>
    <includes id="_boost_storage_8h" name="BoostStorage.h" local="yes" imported="no">lsst/daf/persistence/BoostStorage.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_boost_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbAuth.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_db_auth_8cc</filename>
    <includes id="_db_auth_8h" name="DbAuth.h" local="yes" imported="no">lsst/daf/persistence/DbAuth.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_db_auth_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbStorage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_db_storage_8cc</filename>
    <includes id="_db_storage_8h" name="DbStorage.h" local="yes" imported="no">lsst/daf/persistence/DbStorage.h</includes>
    <includes id="_db_storage_impl_8h" name="DbStorageImpl.h" local="yes" imported="no">lsst/daf/persistence/DbStorageImpl.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_db_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbStorageImpl.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_db_storage_impl_8cc</filename>
    <includes id="_db_storage_impl_8h" name="DbStorageImpl.h" local="yes" imported="no">lsst/daf/persistence/DbStorageImpl.h</includes>
    <includes id="_db_storage_location_8h" name="DbStorageLocation.h" local="yes" imported="no">lsst/daf/persistence/DbStorageLocation.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <class kind="struct">lsst::daf::persistence::IntegerTypeTraits</class>
    <class kind="struct">lsst::daf::persistence::BoundVarTraits</class>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_db_storage_impl_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbStorageLocation.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_db_storage_location_8cc</filename>
    <includes id="_db_storage_location_8h" name="DbStorageLocation.h" local="yes" imported="no">lsst/daf/persistence/DbStorageLocation.h</includes>
    <includes id="_db_auth_8h" name="DbAuth.h" local="yes" imported="no">lsst/daf/persistence/DbAuth.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_db_storage_location_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DbTsvStorage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_db_tsv_storage_8cc</filename>
    <includes id="_db_tsv_storage_8h" name="DbTsvStorage.h" local="yes" imported="no">lsst/daf/persistence/DbTsvStorage.h</includes>
    <includes id="_db_storage_location_8h" name="DbStorageLocation.h" local="yes" imported="no">lsst/daf/persistence/DbStorageLocation.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_db_tsv_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>FitsStorage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_fits_storage_8cc</filename>
    <includes id="_fits_storage_8h" name="FitsStorage.h" local="yes" imported="no">lsst/daf/persistence/FitsStorage.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_fits_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Formatter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_formatter_8cc</filename>
    <includes id="_formatter_8h" name="Formatter.h" local="yes" imported="no">lsst/daf/persistence/Formatter.h</includes>
    <includes id="_formatter_registry_8h" name="FormatterRegistry.h" local="yes" imported="no">lsst/daf/persistence/FormatterRegistry.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_formatter_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>FormatterRegistry.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_formatter_registry_8cc</filename>
    <includes id="_formatter_registry_8h" name="FormatterRegistry.h" local="yes" imported="no">lsst/daf/persistence/FormatterRegistry.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_formatter_registry_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>LogicalLocation.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_logical_location_8cc</filename>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_logical_location_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Persistence.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_persistence_8cc</filename>
    <includes id="_persistence_8h" name="Persistence.h" local="yes" imported="no">lsst/daf/persistence/Persistence.h</includes>
    <includes id="_formatter_8h" name="Formatter.h" local="yes" imported="no">lsst/daf/persistence/Formatter.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_persistence_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>Storage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_storage_8cc</filename>
    <includes id="_storage_8h" name="Storage.h" local="yes" imported="no">lsst/daf/persistence/Storage.h</includes>
    <includes id="_storage_registry_8h" name="StorageRegistry.h" local="yes" imported="no">lsst/daf/persistence/StorageRegistry.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>StorageRegistry.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_storage_registry_8cc</filename>
    <includes id="_storage_registry_8h" name="StorageRegistry.h" local="yes" imported="no">lsst/daf/persistence/StorageRegistry.h</includes>
    <includes id="_boost_storage_8h" name="BoostStorage.h" local="yes" imported="no">lsst/daf/persistence/BoostStorage.h</includes>
    <includes id="_db_storage_8h" name="DbStorage.h" local="yes" imported="no">lsst/daf/persistence/DbStorage.h</includes>
    <includes id="_db_tsv_storage_8h" name="DbTsvStorage.h" local="yes" imported="no">lsst/daf/persistence/DbTsvStorage.h</includes>
    <includes id="_fits_storage_8h" name="FitsStorage.h" local="yes" imported="no">lsst/daf/persistence/FitsStorage.h</includes>
    <includes id="_xml_storage_8h" name="XmlStorage.h" local="yes" imported="no">lsst/daf/persistence/XmlStorage.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_storage_registry_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>XmlStorage.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>_xml_storage_8cc</filename>
    <includes id="_xml_storage_8h" name="XmlStorage.h" local="yes" imported="no">lsst/daf/persistence/XmlStorage.h</includes>
    <includes id="_logical_location_8h" name="LogicalLocation.h" local="yes" imported="no">lsst/daf/persistence/LogicalLocation.h</includes>
    <member kind="define">
      <type>#define</type>
      <name>__attribute__</name>
      <anchorfile>_xml_storage_8cc.html</anchorfile>
      <anchor>a9d373a9b65ff25b2db84c07394e1c212</anchor>
      <arglist>(x)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::BoostStorage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</filename>
    <base>lsst::daf::persistence::Storage</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; BoostStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a3133bf18e165b4d4d283fab8915ea5bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BoostStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a8afee9979264c0560e520c9eabb18c44</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~BoostStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a835f1de7d379cf5f023740e7be6d3923</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a4002de9944d6e74b0227ff11b417c270</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>aeba041c02fbcef35079b0923ff5e16e7</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a1904c5412eeff02f60565532d14795c8</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>af3f244bb7241ab9bc9f4b72d9401239a</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>aead7f3ab66259b91ba3e8d932df9530f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::archive::text_oarchive &amp;</type>
      <name>getOArchive</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>a30fb0829b4c770b745e9a4648922f5d6</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::archive::text_iarchive &amp;</type>
      <name>getIArchive</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_boost_storage.html</anchorfile>
      <anchor>ab8e08991d20f581606664f6c3543ae63</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ab7c2b054da1988d5d89a7a54b6683fc0</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>verifyPathName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a5f8176279f74c260d3f3064889f07611</anchor>
      <arglist>(std::string const &amp;pathName)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::DbAuth</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</filename>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</anchorfile>
      <anchor>a8d2330685090174653ab3de6435ecc51</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>available</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</anchorfile>
      <anchor>a25072cf12f62089cc97228dee09e4de4</anchor>
      <arglist>(std::string const &amp;host, std::string const &amp;port)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>authString</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</anchorfile>
      <anchor>ad181b8b09ca792cb03115b713fa6dfde</anchor>
      <arglist>(std::string const &amp;host, std::string const &amp;port)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>username</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</anchorfile>
      <anchor>a8ad0357be5c4e7d82d33947632b92298</anchor>
      <arglist>(std::string const &amp;host, std::string const &amp;port)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>password</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_auth.html</anchorfile>
      <anchor>a16f56964a57616b4ec622bdcc65684dc</anchor>
      <arglist>(std::string const &amp;host, std::string const &amp;port)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::DbStorage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</filename>
    <base>lsst::daf::persistence::Storage</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; DbStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a23cbd3c35a93898ae46925577fd9f7b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>acab72d2f0e41149817dbdb9f6e1d072e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>af2e430adf20715dd1bd3e9f97130c302</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>abd4a867c036c2d3fd51223100545f144</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a1a4d9de576770b662db3d82af174a6c3</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a34ea3064f5ec4d5adf4950f67519508d</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a6b0cef9dfe141479c9f42478ce9b2891</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a46c9eb3eef2b44a03be3ccb85e326435</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>createTableFromTemplate</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ab976067e94d9e3266aacc23509dda665</anchor>
      <arglist>(std::string const &amp;tableName, std::string const &amp;templateName, bool mayAlreadyExist=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>dropTable</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a577d8cd1fa6402053bd340eb4ef689cc</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>truncateTable</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>afadb1fb9fbe5b4d8674e2fe7e797a0b0</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>executeSql</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a40008fc064c12c719536ef897e0d292a</anchor>
      <arglist>(std::string const &amp;sqlStatement)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableForInsert</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>adc13de5f5c94c43392ce93ce7f391371</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a4ea1d787e29d1deec6e7a97a9cf8231d</anchor>
      <arglist>(std::string const &amp;columnName, T const &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColumnToNull</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ae982cd1030870929925c8c3724e301d2</anchor>
      <arglist>(std::string const &amp;columnName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>insertRow</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a63e449f4ba581ababc0ea55da92211a4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableForQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>aaba67cc168679b40c6edfa6c454930fd</anchor>
      <arglist>(std::string const &amp;tableName, bool isExpr=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableListForQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a74202046b2e90a18c34986481ddd5b20</anchor>
      <arglist>(std::vector&lt; std::string &gt; const &amp;tableNameList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>outColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a3f1473f7fc5bead41907b1580bf24e3a</anchor>
      <arglist>(std::string const &amp;columnName, bool isExpr=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>outParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a104ab4023653bd0ea69e6de32aa71610</anchor>
      <arglist>(std::string const &amp;columnName, T *location, bool isExpr=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>condParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ac66086776aa878003ce6ca31c2f0f2da</anchor>
      <arglist>(std::string const &amp;paramName, T const &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>orderBy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a99f0022a13cc11c921250987942263a1</anchor>
      <arglist>(std::string const &amp;expression)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>groupBy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a9b0017daf238017e9e252d9c5f77ddf7</anchor>
      <arglist>(std::string const &amp;expression)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setQueryWhere</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a7d7b1da51310179d5347f649e0a63692</anchor>
      <arglist>(std::string const &amp;whereClause)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>query</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a33fbdbe7a9776763474da80adb592578</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>next</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>af4517b8cb034f81224c1522739eeee1d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>T const &amp;</type>
      <name>getColumnByPos</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ad473b1e2bd746991459efa942225a640</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>columnIsNull</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a72805ade1dd153146b6f6ff0e8581851</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>finishQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a95ae2b6ab9b534ba92b20c677b379fad</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a85713d409b20bcc439fe1ed87073a4bd</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ab7c2b054da1988d5d89a7a54b6683fc0</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>verifyPathName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a5f8176279f74c260d3f3064889f07611</anchor>
      <arglist>(std::string const &amp;pathName)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::BoundVar</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function">
      <type></type>
      <name>BoundVar</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a2f3cbf1ecbac4d70df7397a58d89a920</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BoundVar</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>aadd197f03e5734fcd160cefd8696b869</anchor>
      <arglist>(void *location)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BoundVar</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a8cb4aeb731c71682b6eb616e29811c1d</anchor>
      <arglist>(BoundVar const &amp;src)</arglist>
    </member>
    <member kind="variable">
      <type>enum_field_types</type>
      <name>_type</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a6a14a608a630e798d80492d0446c505f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>_isNull</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a4ac5a50b1b8b89a19be0b352b6a264fd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>_isUnsigned</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>aefc032627305492bd7adbefcb8c5e280</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>unsigned long</type>
      <name>_length</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a0796abfac923e34ff010c667ea7d8ab6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>void *</type>
      <name>_data</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_bound_var.html</anchorfile>
      <anchor>a43f385713dcef5c6dbdbbbca89f5f140</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::DbStorageImpl</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DbStorageImpl</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>a7472a7cfb0cf6dab11362c06184d06cf</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>a0c822172150f6358851d292554517aeb</anchor>
      <arglist>(std::string const &amp;columnName, std::string const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>a5fa82ea6d9379160a804169ee8a63b0c</anchor>
      <arglist>(std::string const &amp;columnName, dafBase::DateTime const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>outParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>ab1bf7c2bbc830a30c2619b2b48da8fc4</anchor>
      <arglist>(std::string const &amp;columnName, std::string *location, bool isExpr)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>outParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>aa45a395e7670325f187a29c67cfd6e44</anchor>
      <arglist>(std::string const &amp;columnName, dafBase::DateTime *location, bool isExpr)</arglist>
    </member>
    <member kind="friend" protection="private">
      <type>friend class</type>
      <name>DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_impl.html</anchorfile>
      <anchor>a673ee87bb886f427ba94fa5239ae50c0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::DbStorageLocation</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; DbStorageLocation &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>abc170f09791cb6eb09962cca50c8cf37</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DbStorageLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>ae7e87c302bbb434b1b1ffda3b2215662</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DbStorageLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a815d542a7aa8ad47e6c96f111b71ffaf</anchor>
      <arglist>(std::string const &amp;url)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DbStorageLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a9bbdcd9e668c478c1105b9fe12782ee1</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a72f270beba233f4e131e4f6e950096ca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>getConnString</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a5a2f32990f211eece885614c51a071ec</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getDbType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a6660a481ee7396710e21b53b25a0a2b0</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getHostname</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a4f5bf7c6ff8d4b378cf2890825fb3cd9</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getPort</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a68295a4849211dee5c3bc075c2ad9509</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getUsername</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>ad802f89b5f5d7a3022922868afac7793</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getPassword</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a0e7bb3d3a794ceb0b118850afb49dfca</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getDbName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage_location.html</anchorfile>
      <anchor>a174d8db86d83e8d52675cd16a57b9b62</anchor>
      <arglist>(void) const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::DbTsvStorage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</filename>
    <base>lsst::daf::persistence::DbStorage</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; DbTsvStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>aadba4cbd8999302e5a252cc7ddfbbe9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; DbStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a23cbd3c35a93898ae46925577fd9f7b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DbTsvStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a62008db186bb10c552e50aee4d7563cc</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DbTsvStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>ae292ddcc57aac2ca3c24bb5b7d6350c4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a631b622dbc196f8bef74e9be07f72dff</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>ae45360549bdabac777f8a4d671426cdf</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a3c7255976623b82b0f84e7820df2c924</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a885aab929c2114640cf53373be4a8452</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>af2cdd40011de3e703e039ba5f5c7c333</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>createTableFromTemplate</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>aede6502d33a76825e61c3f8c50439053</anchor>
      <arglist>(std::string const &amp;tableName, std::string const &amp;templateName, bool mayAlreadyExist=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>dropTable</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a8184b7e5812e59b487969b1dddb6405c</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>truncateTable</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>abb97c3f49f41ec37f18e270e55ad0b85</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableForInsert</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a49046e4db7a09bd51b42d7a9d8906490</anchor>
      <arglist>(std::string const &amp;tableName)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a1df312d81e59f7fa87b5f1c513d9f0f7</anchor>
      <arglist>(std::string const &amp;columnName, T const &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setColumnToNull</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a7c448f52ef3f465044d010e6c99e21ef</anchor>
      <arglist>(std::string const &amp;columnName)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>insertRow</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>ac646a66be5e87f83dec1681574685042</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>outParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a637bcf46f033332d07600014237f597c</anchor>
      <arglist>(std::string const &amp;columnName, T *location, bool isExpr=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>condParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a177af9e9cf8fa5983b8a2744c8156b7a</anchor>
      <arglist>(std::string const &amp;paramName, T const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>T const &amp;</type>
      <name>getColumnByPos</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a358e922597451650c5313cbc98aafc3e</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a0000c6353f182833303e6a960b0d6c96</anchor>
      <arglist>(std::string const &amp;columnName, char const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a48bb1eda8a43a7f7e88299db9387f8d3</anchor>
      <arglist>(std::string const &amp;columnName, double const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>af8f05907e49aab50a556fca4bbf96274</anchor>
      <arglist>(std::string const &amp;columnName, float const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_tsv_storage.html</anchorfile>
      <anchor>a0677ba13c4d44361fa76e1db30c43da2</anchor>
      <arglist>(std::string const &amp;columnName, DateTime const &amp;value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>acab72d2f0e41149817dbdb9f6e1d072e</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>af2e430adf20715dd1bd3e9f97130c302</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>executeSql</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a40008fc064c12c719536ef897e0d292a</anchor>
      <arglist>(std::string const &amp;sqlStatement)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a4ea1d787e29d1deec6e7a97a9cf8231d</anchor>
      <arglist>(std::string const &amp;columnName, T const &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableForQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>aaba67cc168679b40c6edfa6c454930fd</anchor>
      <arglist>(std::string const &amp;tableName, bool isExpr=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setTableListForQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a74202046b2e90a18c34986481ddd5b20</anchor>
      <arglist>(std::vector&lt; std::string &gt; const &amp;tableNameList)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>outColumn</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a3f1473f7fc5bead41907b1580bf24e3a</anchor>
      <arglist>(std::string const &amp;columnName, bool isExpr=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>outParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a104ab4023653bd0ea69e6de32aa71610</anchor>
      <arglist>(std::string const &amp;columnName, T *location, bool isExpr=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>condParam</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ac66086776aa878003ce6ca31c2f0f2da</anchor>
      <arglist>(std::string const &amp;paramName, T const &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>orderBy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a99f0022a13cc11c921250987942263a1</anchor>
      <arglist>(std::string const &amp;expression)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>groupBy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a9b0017daf238017e9e252d9c5f77ddf7</anchor>
      <arglist>(std::string const &amp;expression)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setQueryWhere</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a7d7b1da51310179d5347f649e0a63692</anchor>
      <arglist>(std::string const &amp;whereClause)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>query</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a33fbdbe7a9776763474da80adb592578</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>next</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>af4517b8cb034f81224c1522739eeee1d</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function">
      <type>T const &amp;</type>
      <name>getColumnByPos</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>ad473b1e2bd746991459efa942225a640</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>columnIsNull</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a72805ade1dd153146b6f6ff0e8581851</anchor>
      <arglist>(int pos)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>finishQuery</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a95ae2b6ab9b534ba92b20c677b379fad</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>DbStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_db_storage.html</anchorfile>
      <anchor>a85713d409b20bcc439fe1ed87073a4bd</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::FitsStorage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</filename>
    <base>lsst::daf::persistence::Storage</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; FitsStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>aac239ca00056b454368ba8604ebafb94</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FitsStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>a4bcf14c5d611df8e7ebdab13e841e4df</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~FitsStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>ab039b6c00ebc9ef133303849c3a79005</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>a41bcc74e0ebc9e458cd702a0b3e32722</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>a5dea70ff61a93b8ec34698b0af6c3423</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>ab3a30b9e0bd47c54bcedab49b0241286</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>a8145211eff3459bb782d9d05b454320f</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>aa7bd50affe1634ea63e77f0f583c814c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string const &amp;</type>
      <name>getPath</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>a19a51b909c638ae6ce6c526d40604a58</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>getHdu</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_fits_storage.html</anchorfile>
      <anchor>ae37a5af0973083d31b31a5ea381efdba</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ab7c2b054da1988d5d89a7a54b6683fc0</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>verifyPathName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a5f8176279f74c260d3f3064889f07611</anchor>
      <arglist>(std::string const &amp;pathName)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::Formatter</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_formatter.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Formatter &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a013289a110de3a03d867f0c0e8a98a07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Ptr(*</type>
      <name>FactoryPtr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>aeedf26a988c5a3224ce5af0ff133b85a</anchor>
      <arglist>)(lsst::pex::policy::Policy::Ptr)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Formatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a6d7cb77b1b8b69e6291a5cd9bd8bffbd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a174784d7f85c7958946ec36e6abd460c</anchor>
      <arglist>(lsst::daf::base::Persistable const *persistable, Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual lsst::daf::base::Persistable *</type>
      <name>read</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a40d5ec4d5620ab1af91d784a57294f4d</anchor>
      <arglist>(Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>update</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a3c0cc0746018827c0b3acaec9acfb324</anchor>
      <arglist>(lsst::daf::base::Persistable *persistable, Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a35ffec7d02f510957e8ba2bf6afcc047</anchor>
      <arglist>(std::string const &amp;persistableType, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>affcfb3a9c2297472eade53d14b412cc8</anchor>
      <arglist>(std::type_info const &amp;persistableType, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Formatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a07ce82f4828cba0c48e3db009564acaf</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::FormatterRegistration</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_formatter_registration.html</filename>
    <member kind="function">
      <type></type>
      <name>FormatterRegistration</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter_registration.html</anchorfile>
      <anchor>a9016b779ba31dea8bba2dd7e0d1f5e2c</anchor>
      <arglist>(std::string const &amp;persistableName, std::type_info const &amp;persistableType, Formatter::FactoryPtr factory)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::FormatterRegistry</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_formatter_registry.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function">
      <type>void</type>
      <name>registerFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter_registry.html</anchorfile>
      <anchor>a7c690d74e1003673e011d43512644bb0</anchor>
      <arglist>(std::string const &amp;persistableName, std::type_info const &amp;persistableType, Formatter::FactoryPtr factory)</arglist>
    </member>
    <member kind="function">
      <type>Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter_registry.html</anchorfile>
      <anchor>a15e4894b979c3ce881a96228e6b18c6e</anchor>
      <arglist>(std::type_info const &amp;persistableType, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function">
      <type>Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter_registry.html</anchorfile>
      <anchor>ac3e15f48efa6cd34710619b98f4fdf77</anchor>
      <arglist>(std::string const &amp;persistableName, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static FormatterRegistry &amp;</type>
      <name>getInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter_registry.html</anchorfile>
      <anchor>a723e2e6eb22d32ed97d810a470804e67</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::LogicalLocation</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_logical_location.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; LogicalLocation &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_logical_location.html</anchorfile>
      <anchor>a8dd57e2f071783cbb2ef9e15e85f9620</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>LogicalLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_logical_location.html</anchorfile>
      <anchor>a90f8e844745fa727abf059aec3943b56</anchor>
      <arglist>(std::string const &amp;locString, boost::shared_ptr&lt; dafBase::PropertySet const  &gt; additionalData=boost::shared_ptr&lt; dafBase::PropertySet const  &gt;())</arglist>
    </member>
    <member kind="function">
      <type>std::string const &amp;</type>
      <name>locString</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_logical_location.html</anchorfile>
      <anchor>ae0a834272db7361cf02e30a053a34325</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>setLocationMap</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_logical_location.html</anchorfile>
      <anchor>ab65cee1f923c09b319bfa0204f645d2c</anchor>
      <arglist>(boost::shared_ptr&lt; dafBase::PropertySet &gt; map)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::Persistence</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_persistence.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Persistence &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>ae2baa1267caec6cfc30b7ace8bc87018</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Persistence</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a1bc60e407cd029e00fdb46ae08837427</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Storage::Ptr</type>
      <name>getPersistStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a2af0af2e074c7acfc0ff5c047e6e44d8</anchor>
      <arglist>(std::string const &amp;storageType, LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual Storage::Ptr</type>
      <name>getRetrieveStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>aa67d5670a83d4bfae4aa6637591de1db</anchor>
      <arglist>(std::string const &amp;storageType, LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>persist</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a4e297b123ffe9146c35cb9fa20efd5d9</anchor>
      <arglist>(lsst::daf::base::Persistable const &amp;persistable, Storage::List const &amp;storageList, lsst::daf::base::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::daf::base::Persistable::Ptr</type>
      <name>retrieve</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a2bd2c0a4d2f74e540a62c82f61ebd059</anchor>
      <arglist>(std::string const &amp;persistableType, Storage::List const &amp;storageList, lsst::daf::base::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::daf::base::Persistable *</type>
      <name>unsafeRetrieve</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a12e5cdeaee5a6893699a2f32454e5973</anchor>
      <arglist>(std::string const &amp;persistableType, Storage::List const &amp;storageList, lsst::daf::base::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function">
      <type>lsst::pex::policy::Policy::Ptr</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a19c7ec07bac4ccb28e4dee77011ad17f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>getPersistence</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_persistence.html</anchorfile>
      <anchor>a9de7b50ef1bbf313f463bc9ffa9e214a</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::PropertySetFormatter</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</filename>
    <base>lsst::daf::persistence::Formatter</base>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PropertySetFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</anchorfile>
      <anchor>a3baf7f09286fa4c3a5801b1646b34af4</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</anchorfile>
      <anchor>a40ba82c998bb347c567138ab6d577509</anchor>
      <arglist>(dafBase::Persistable const *persistable, dafPersist::Storage::Ptr storage, dafBase::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual dafBase::Persistable *</type>
      <name>read</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</anchorfile>
      <anchor>a0e5dcd23f4e2a26eaf60fcef75a332e9</anchor>
      <arglist>(dafPersist::Storage::Ptr storage, dafBase::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>update</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</anchorfile>
      <anchor>a8890b845e89ce1931362b3a937d4382c</anchor>
      <arglist>(dafBase::Persistable *persistable, dafPersist::Storage::Ptr storage, dafBase::PropertySet::Ptr additionalData)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Formatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a6d7cb77b1b8b69e6291a5cd9bd8bffbd</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a174784d7f85c7958946ec36e6abd460c</anchor>
      <arglist>(lsst::daf::base::Persistable const *persistable, Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual lsst::daf::base::Persistable *</type>
      <name>read</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a40d5ec4d5620ab1af91d784a57294f4d</anchor>
      <arglist>(Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>update</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a3c0cc0746018827c0b3acaec9acfb324</anchor>
      <arglist>(lsst::daf::base::Persistable *persistable, Storage::Ptr storage, lsst::daf::base::PropertySet::Ptr additionalData)=0</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>delegateSerialize</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_property_set_formatter.html</anchorfile>
      <anchor>a0549c715094dfa36b5e36bb0679946c4</anchor>
      <arglist>(Archive &amp;ar, unsigned int const version, dafBase::Persistable *persistable)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a35ffec7d02f510957e8ba2bf6afcc047</anchor>
      <arglist>(std::string const &amp;persistableType, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Formatter::Ptr</type>
      <name>lookupFormatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>affcfb3a9c2297472eade53d14b412cc8</anchor>
      <arglist>(std::type_info const &amp;persistableType, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Formatter &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a013289a110de3a03d867f0c0e8a98a07</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Ptr(*</type>
      <name>FactoryPtr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>aeedf26a988c5a3224ce5af0ff133b85a</anchor>
      <arglist>)(lsst::pex::policy::Policy::Ptr)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Formatter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_formatter.html</anchorfile>
      <anchor>a07ce82f4828cba0c48e3db009564acaf</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::Storage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_storage.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a3194fdc3736b98137629f64601e39465</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a9df6504e477b9707b84ff42d71ca31bb</anchor>
      <arglist>(LogicalLocation const &amp;location)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a77e5f66cc828d138b91088d28a55eadc</anchor>
      <arglist>(LogicalLocation const &amp;location)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a17d9131ad6af5ad33243f67d2dd5b981</anchor>
      <arglist>(void)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ae208336921e5d1f3aafda894f62c4a39</anchor>
      <arglist>(void)=0</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ab7c2b054da1988d5d89a7a54b6683fc0</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>verifyPathName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a5f8176279f74c260d3f3064889f07611</anchor>
      <arglist>(std::string const &amp;pathName)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::StorageRegistry</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_storage_registry.html</filename>
    <member kind="function">
      <type>Storage::Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage_registry.html</anchorfile>
      <anchor>ad9f84d4c86cc195138b7ff37f5825772</anchor>
      <arglist>(std::string const &amp;name)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static StorageRegistry &amp;</type>
      <name>getRegistry</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage_registry.html</anchorfile>
      <anchor>a5f2f32d34a74f633f0e637c47d731514</anchor>
      <arglist>(void)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::XmlStorage</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</filename>
    <base>lsst::daf::persistence::Storage</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; XmlStorage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a49d6f4f71d642826b90adf8cc67a13bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Storage &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a2d5e367fc152a7e2d958722cdd6f6a9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>List</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a983c26c3d9f2a82bd1381b2050f86fda</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>XmlStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a40174e6fc4a11f9ef1bc7d8a5b84f65c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~XmlStorage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a76afa34ebde68692dd0300450fe7e3fb</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPolicy</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a31fce4439c4643b25d1731889d9aa2f9</anchor>
      <arglist>(lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setPersistLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>affd4b24ce2082c8794086569d43890f8</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>setRetrieveLocation</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>afb1e8564f490e497931287c48687a170</anchor>
      <arglist>(LogicalLocation const &amp;location)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>startTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a5b8fa43e8607917f633b706cc22a5780</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>endTransaction</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>aa3728d2de92fe5e26768f094e1a8987c</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::archive::xml_oarchive &amp;</type>
      <name>getOArchive</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>ad415517f1ba7839a17812c06f3086c97</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::archive::xml_iarchive &amp;</type>
      <name>getIArchive</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_xml_storage.html</anchorfile>
      <anchor>a40e9a4c4ea3dcbf15638fb14dfdaa6a9</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a79843c10c7d9be7bf5624e2ff5e83d91</anchor>
      <arglist>(void)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Ptr</type>
      <name>createInstance</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>abc38947a53dd7f14dff4cc2bce6ab377</anchor>
      <arglist>(std::string const &amp;name, LogicalLocation const &amp;location, bool persist, lsst::pex::policy::Policy::Ptr policy)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Storage</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>ab7c2b054da1988d5d89a7a54b6683fc0</anchor>
      <arglist>(std::type_info const &amp;type)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>verifyPathName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1_storage.html</anchorfile>
      <anchor>a5f8176279f74c260d3f3064889f07611</anchor>
      <arglist>(std::string const &amp;pathName)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lsst::daf::persistence::IntegerTypeTraits</name>
    <filename>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</filename>
    <templarg>N</templarg>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</anchorfile>
      <anchor>a79df6a8a784543240a96961abebb7c7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</anchorfile>
      <anchor>a3fc002ee37ddaeb5a4c45e8adb683eb1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</anchorfile>
      <anchor>a02da639777ad929a2e1fea99c2273fb7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</anchorfile>
      <anchor>a4ee932814a739abc19638b354d143d50</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_integer_type_traits.html</anchorfile>
      <anchor>a0267b8b106a5df47dd5b0b9a4c8fdee4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>lsst::daf::persistence::BoundVarTraits</name>
    <filename>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</filename>
    <templarg></templarg>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a0a22ab903b396222799f303ea4f81c79</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a5d36d991616b1a7edb0ef57c7b785066</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>aa7e382cfae2db66750c61cf7d4e7693f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>adf007ff4f7ed39d7653e2a274839bb2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>ade4ab14ab99bded410f0f67c2c1d055b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>aaaa5026115e722b3e30156aa29626cf7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a2b12dc6c8a0ad74b080451923fa57892</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a7c24c7be4a95adb3a5175a7eae53b2a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a0b5f889b7d9a8890cefa96f751bf8379</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>abf1a50bda70d08fbb974e0ce10559c7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a6af670503b98cf1ac3c29942f73e8d00</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a8f30dd7fc919ec43acce6a903c978878</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a8c3a40460ba6847afe862c1bb120eae5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>aa03d51caeaa901b0fdb1ad68ea9749da</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a061753f00ca78540f9538a92cfdc185a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a84d47058681f1b9e8daade656cc7fa60</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>ab1aa272004a8f15f4bce004901f06f22</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>ac3e82d4667805fc0208a3fd1bdf72e36</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>aaa4c31126c927792d4d65043f222d06b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a5ab844e7dac21b0bd0d71a13c8f25a9a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a95b128f43426e61c9224b1cf34b3ac0a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static enum_field_types</type>
      <name>mysqlType</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>accca2c215071cb02d95e203ce56a516a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static bool</type>
      <name>isUnsigned</name>
      <anchorfile>structlsst_1_1daf_1_1persistence_1_1_bound_var_traits.html</anchorfile>
      <anchor>a1cdcc2ac8b5c328e2345daf9aeefe354</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::butler</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1butler.html</filename>
    <class kind="class">lsst::daf::persistence::butler::Butler</class>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butler::Butler</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a5bd2c9785c8497be6931f72d10ff075b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getKeys</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a9969184323450bf461478da53bf7e029</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>queryMetadata</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a5f2e147ed7842726c41440579431d2b4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>datasetExists</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a5b5af526b1503f7bd851e52cc1d77b7b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a664b468aaaa2f0a39a1c37bb4ad6298c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>put</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>ab6f7065ae31b25786953338ce113c74c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>subset</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>ad34f9811a17774d296c26fbeead80abd</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>dataRef</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a714c24bd062f86905084858fe6ce80e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__reduce__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>adcd264416872051eafb05226c02b92d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" static="yes">
      <type>def</type>
      <name>getMapperClass</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a21b63f1ed7384ab95a163d8ec7187a82</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>mapper</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>ac48e58b6c43c7c461ec446bfad6f617f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>persistence</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a647bc3745a9bd7f55e8f03892f8daeef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>log</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_1_1_butler.html</anchorfile>
      <anchor>a12e1b4adfa76383b53e8cce2209288b3</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::butlerFactory</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1butler_factory.html</filename>
    <class kind="class">lsst::daf::persistence::butlerFactory::ButlerFactory</class>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butlerFactory::ButlerFactory</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_factory_1_1_butler_factory.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_factory_1_1_butler_factory.html</anchorfile>
      <anchor>ad03ae29869db25a9a26c90467f86787a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>create</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_factory_1_1_butler_factory.html</anchorfile>
      <anchor>aa44c127dcc2b9011a84cb52fd09e2ec3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>mapper</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_factory_1_1_butler_factory.html</anchorfile>
      <anchor>afaef4f0d72b7c07cd4c83bc8b91ef4f5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::butlerLocation</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1butler_location.html</filename>
    <class kind="class">lsst::daf::persistence::butlerLocation::ButlerLocation</class>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butlerLocation::ButlerLocation</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>ac37d4d5851a430fe5abeb5fbeb847f14</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__str__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>a02fe5789ac87badb14294df28c44fdef</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getPythonType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>af9194cae8896b095f6ddd383788e218a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getCppType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>afa238ea85cf0ad1be696040fe53c6e05</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getStorageName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>a9f8aff4b1c29ed96aa1ba95854ebd9ab</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getLocations</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>ae3d1948d2be5b3a779f5b3cab003b86d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getAdditionalData</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>af6b223e7ff9349544fe93252f13dbfb4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>pythonType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>a67d37536e2b2761f8ff766e7ea65f329</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>cppType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>a47021ff6e64a30770889d94a4c0b422b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>storageName</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>aec54bfe4addc535e041cc946d864d1d4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>locationList</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>aa20abeb88c3c8d91be38b42f62e7315d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>additionalData</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_location_1_1_butler_location.html</anchorfile>
      <anchor>a2d777f07d6a8c70dd374a60fb65c4694</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::butlerSubset</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1butler_subset.html</filename>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerSubset</class>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerSubsetIterator</class>
    <class kind="class">lsst::daf::persistence::butlerSubset::ButlerDataRef</class>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butlerSubset::ButlerSubset</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>ab5b4ab00de47f6b6993bb2437e9f06e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__len__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>adab784bed47dddd38ed03fd37ed38784</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>acea42d76246835809cf15c78e417e524</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>butler</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>a9a9a45e357f04b91f358674d74db40b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>datasetType</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>a314807b69cba2af4080aa98e92270bdf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>level</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>a1bd7cd21a3bebe2e209b6cd9c4652883</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dataId</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>a76e6dcc353ccc4a1dfb74baec31e1618</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>cache</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset.html</anchorfile>
      <anchor>ab055ef70c018037ebc0002f7e0fdcec8</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butlerSubset::ButlerSubsetIterator</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</anchorfile>
      <anchor>a6a72cf23d2a8be01ae97f993d9521d87</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__iter__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</anchorfile>
      <anchor>ac1f037e1fae455c2f35576987391e39b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>next</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</anchorfile>
      <anchor>a783e7bf1cfb806be4ca8cc4dd8e91c8e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>butlerSubset</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</anchorfile>
      <anchor>a7749deea7d03fff36d9af14ed7a8e37b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>iter</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_subset_iterator.html</anchorfile>
      <anchor>a1b5d876be2f67956d5761b358fd0aa34</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::butlerSubset::ButlerDataRef</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a4fba944102b0606ffeb40f2e2a3cb961</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>get</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>af4131c54d8b0b36304430b81199b96ac</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>put</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a6fc905bd549b94c3bce244de29219c5c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>subLevels</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>aec34ea4145a699a12d9de47a282959c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>subItems</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a4fffe3d15a7dc58aed8a018a112508e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>datasetExists</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a1446ecbb81c35d62ccf2182d3ab84ead</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getButler</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a655fc2ea68538a3812651ff19c11d547</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>butlerSubset</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>a78a6bd1b825e968934ae97b0bb033f4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>dataId</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1butler_subset_1_1_butler_data_ref.html</anchorfile>
      <anchor>af9f0aa4fd94c933fae3f096688ef21a9</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::mapper</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1mapper.html</filename>
    <class kind="class">lsst::daf::persistence::mapper::Mapper</class>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::mapper::Mapper</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__new__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>aa0ae62711672a82ce0decaac5ba29ffe</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>afb2595337a2e2d931bb502e8e558ffe5</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getstate__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>ae793423deaf0d4eb2c67096907eea9be</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setstate__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a064a1e42f076c16f137657b4478523c8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>keys</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a5f50f7fd3d8244a7be30f45c83f15b40</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>queryMetadata</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a088f26bd30c99129fc7862f99d65871e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>getDatasetTypes</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a4189be25070811e3117e5fa6e6da263f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>map</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>ad1598c179b20f856880854ca5a3a08c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>canStandardize</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a5efccfaa55f896a020f7e4dfe00232b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>standardize</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a66e9369fe7e17004c0b79ef60d308b64</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>validate</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>ab5777fde2bd3635318ca39015eafd752</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>backup</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1mapper_1_1_mapper.html</anchorfile>
      <anchor>a5bde97e63582ccd62875ef68691fa00e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::readProxy</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</filename>
    <class kind="class">lsst::daf::persistence::readProxy::ReadProxy</class>
    <member kind="function">
      <type>def</type>
      <name>__subject__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>af95e46a32899cef332637efc706be2d6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>get_callback</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>ac16c08f0ede43fef2bca8a5b8ad658c7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>set_callback</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>ab4813ef01c046f99ef53eb1e24d206b1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>get_cache</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>af22cdf0ad019921719248ba62bc6015d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type></type>
      <name>set_cache</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1read_proxy.html</anchorfile>
      <anchor>a3e73f42b051f0991d08ca5aee851f3a6</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::daf::persistence::readProxy::ReadProxy</name>
    <filename>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</filename>
    <member kind="function">
      <type>def</type>
      <name>__init__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a777709a0ca9c25763ef5623b07a83460</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getattribute__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a2aae24a144c32c4e7a317908efe1887c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setattr__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a538453d47966077b6c6213431450f523</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delattr__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>ac9518f24e2c7a446826e2429906e8d5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__nonzero__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a5e33c83130ac16952899fc3208342d04</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getitem__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>aade0047c3c89236a9da6a0c8e545925d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setitem__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a7de747579d932a6f9c681d8a14fbeaa0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delitem__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a710828bddf0257ace1b81f7b7f4c0629</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__getslice__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a6697df5b372b84f01b8827564e41997a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__setslice__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a53e77c89fc9889d5a27c64de6421da1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__delslice__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a9f7cfdb35ef0a5a273aea3e96bc20d1a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__contains__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a556292efc66bc5a810554c2cbce76eb0</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__rdivmod__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a7434ee1b74a25ffbd0204338267da114</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__pow__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>ae6372c33a036fac0a6bac67ff8087488</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__ipow__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>a5c16c0eaed559d923edd9b04ce18b207</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>def</type>
      <name>__rpow__</name>
      <anchorfile>classlsst_1_1daf_1_1persistence_1_1read_proxy_1_1_read_proxy.html</anchorfile>
      <anchor>abe3a790d07a7cab89d090ac58bda1f8e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::daf::persistence::version</name>
    <filename>namespacelsst_1_1daf_1_1persistence_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>afcd10f700e0c2a5be5e5e85a178c3292</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a52ee33a80ad0cb0fb9375195afaff1f6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a257375b7ef6c2c7d96f17c649e01599a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>ac9ba93015de53ae850807390b3937c37</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>a4306565e67a01d827a9c7362cc3e1560</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>abf53907879899a81c03a07c45db2141b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1daf_1_1persistence_1_1version.html</anchorfile>
      <anchor>ae34ecfc865605e5e514e793a6700a5af</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>python/lsst/daf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/</path>
    <filename>dir_a5af4eb02f8a2eeb55a6bfc74ef0b012.html</filename>
    <dir>python/lsst/daf/persistence</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/daf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/</path>
    <filename>dir_956e4256259974f8f135b20840595bd9.html</filename>
    <dir>include/lsst/daf/persistence</dir>
    <file>persistence.h</file>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/daf</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/daf</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst/daf/persistence</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/lsst/daf/persistence/</path>
    <filename>dir_1b5efe42333a8a1dd777cd96b2917a6a.html</filename>
    <file>__init__.py</file>
    <file>butler.py</file>
    <file>butlerFactory.py</file>
    <file>butlerLocation.py</file>
    <file>butlerSubset.py</file>
    <file>mapper.py</file>
    <file>readProxy.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/daf/persistence</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/include/lsst/daf/persistence/</path>
    <filename>dir_53531d80938fcd4b56d3daf2adaac869.html</filename>
    <file>BoostStorage.h</file>
    <file>DbAuth.h</file>
    <file>DbStorage.h</file>
    <file>DbStorageImpl.h</file>
    <file>DbStorageLocation.h</file>
    <file>DbTsvStorage.h</file>
    <file>FitsStorage.h</file>
    <file>Formatter.h</file>
    <file>FormatterImpl.h</file>
    <file>FormatterRegistry.h</file>
    <file>LogicalLocation.h</file>
    <file>Persistence.h</file>
    <file>PropertySetFormatter.cc</file>
    <file>PropertySetFormatter.h</file>
    <file>Storage.h</file>
    <file>StorageRegistry.h</file>
    <file>XmlStorage.h</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/daf_persistence-7.1.1.0+2/daf_persistence-7.1.1.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <file>BoostStorage.cc</file>
    <file>DbAuth.cc</file>
    <file>DbStorage.cc</file>
    <file>DbStorageImpl.cc</file>
    <file>DbStorageLocation.cc</file>
    <file>DbTsvStorage.cc</file>
    <file>FitsStorage.cc</file>
    <file>Formatter.cc</file>
    <file>FormatterRegistry.cc</file>
    <file>LogicalLocation.cc</file>
    <file>Persistence.cc</file>
    <file>PropertySetFormatter.cc</file>
    <file>Storage.cc</file>
    <file>StorageRegistry.cc</file>
    <file>XmlStorage.cc</file>
  </compound>
</tagfile>
