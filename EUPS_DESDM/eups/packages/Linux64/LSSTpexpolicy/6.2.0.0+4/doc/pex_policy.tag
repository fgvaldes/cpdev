<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="file">
    <name>dictionaries.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/doc/</path>
    <filename>dictionaries_8dox</filename>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>overview.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/doc/</path>
    <filename>overview_8dox</filename>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>paf.dox</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/doc/</path>
    <filename>paf_8dox</filename>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>policy.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/</path>
    <filename>policy_8h</filename>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_dictionary_8h" name="Dictionary.h" local="yes" imported="no">lsst/pex/policy/Dictionary.h</includes>
    <includes id="_policy_writer_8h" name="PolicyWriter.h" local="yes" imported="no">lsst/pex/policy/PolicyWriter.h</includes>
    <includes id="_policy_string_8h" name="PolicyString.h" local="yes" imported="no">lsst/pex/policy/PolicyString.h</includes>
    <includes id="_default_policy_file_8h" name="DefaultPolicyFile.h" local="yes" imported="no">lsst/pex/policy/DefaultPolicyFile.h</includes>
    <includes id="_p_a_f_writer_8h" name="PAFWriter.h" local="yes" imported="no">lsst/pex/policy/paf/PAFWriter.h</includes>
    <includes id="_urn_policy_file_8h" name="UrnPolicyFile.h" local="yes" imported="no">lsst/pex/policy/UrnPolicyFile.h</includes>
    <includes id="_policy_stream_destination_8h" name="PolicyStreamDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyStreamDestination.h</includes>
    <includes id="_policy_string_destination_8h" name="PolicyStringDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyStringDestination.h</includes>
  </compound>
  <compound kind="file">
    <name>DefaultPolicyFile.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_default_policy_file_8h</filename>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <class kind="class">lsst::pex::policy::DefaultPolicyFile</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>Dictionary.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_dictionary_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
  </compound>
  <compound kind="file">
    <name>exceptions.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>exceptions_8h</filename>
    <class kind="class">lsst::pex::policy::BadNameError</class>
    <class kind="class">lsst::pex::policy::DictionaryError</class>
    <class kind="class">lsst::pex::policy::NameNotFound</class>
    <class kind="class">lsst::pex::policy::TypeError</class>
    <namespace>lsst::pex::policy</namespace>
    <member kind="define">
      <type>#define</type>
      <name>POL_EARGS_TYPED</name>
      <anchorfile>exceptions_8h.html</anchorfile>
      <anchor>a536c9cfa5082354ca91897435f39b7f8</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>POL_EARGS_UNTYPED</name>
      <anchorfile>exceptions_8h.html</anchorfile>
      <anchor>ae9feea3625314d40b6ce17fec954fa95</anchor>
      <arglist></arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>POL_EXCEPT_VIRTFUNCS</name>
      <anchorfile>exceptions_8h.html</anchorfile>
      <anchor>a6c2d2f2367088674a4c629537cf4bab9</anchor>
      <arglist>(etn)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PAFParser.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/paf/</path>
    <filename>_p_a_f_parser_8h</filename>
    <includes id="_policy_parser_8h" name="PolicyParser.h" local="yes" imported="no">lsst/pex/policy/PolicyParser.h</includes>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <class kind="class">lsst::pex::policy::paf::PAFParser</class>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>PAFParserFactory.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/paf/</path>
    <filename>_p_a_f_parser_factory_8h</filename>
    <includes id="_policy_parser_factory_8h" name="PolicyParserFactory.h" local="yes" imported="no">lsst/pex/policy/PolicyParserFactory.h</includes>
    <class kind="class">lsst::pex::policy::paf::PAFParserFactory</class>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>PAFWriter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/paf/</path>
    <filename>_p_a_f_writer_8h</filename>
    <includes id="_policy_writer_8h" name="PolicyWriter.h" local="yes" imported="no">lsst/pex/policy/PolicyWriter.h</includes>
    <class kind="class">lsst::pex::policy::paf::PAFWriter</class>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>parserexceptions.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>parserexceptions_8h</filename>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
    <class kind="class">lsst::pex::policy::ParserError</class>
    <class kind="class">lsst::pex::policy::EOFError</class>
    <class kind="class">lsst::pex::policy::SyntaxError</class>
    <class kind="class">lsst::pex::policy::FormatSyntaxError</class>
    <class kind="class">lsst::pex::policy::UnsupportedSyntax</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>Policy.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_8h</filename>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
    <class kind="class">lsst::pex::policy::Policy</class>
    <namespace>lsst::pex::policy</namespace>
    <member kind="define">
      <type>#define</type>
      <name>POL_GETSCALAR</name>
      <anchorfile>_policy_8h.html</anchorfile>
      <anchor>aa2c022333029b1c7b4db4a056b42e1ac</anchor>
      <arglist>(name, type, vtype)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>POL_GETLIST</name>
      <anchorfile>_policy_8h.html</anchorfile>
      <anchor>a52aa8b8f4f53ee20b5409bdbb2a4fdf3</anchor>
      <arglist>(name, type, vtype)</arglist>
    </member>
    <member kind="define">
      <type>#define</type>
      <name>POL_ADD</name>
      <anchorfile>_policy_8h.html</anchorfile>
      <anchor>a5f4f821b5309bc6ec96faaba28f7f69d</anchor>
      <arglist>(name, value)</arglist>
    </member>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abcb847b27afe2f2b8d5bea87f0bdd96d</anchor>
      <arglist>(std::ostream &amp;os, const Policy &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>Policy::getValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a6893991d61842731eb89cb01c44078d2</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::FilePtr</type>
      <name>Policy::getValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a25ba5e056300296362702756acce645a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ConstPtr</type>
      <name>Policy::getValue&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abedb897960f26d6afe7443ff1e652be6</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>Policy::getValueArray&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>aa6b187fa84f4a0ff7e9e70877f07aa27</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::FilePtrArray</type>
      <name>Policy::getValueArray&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>ac074f6272a55efe43a8fab3d00c9e3ce</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::PolicyPtrArray</type>
      <name>Policy::getValueArray&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a8444bec7676b84adf59f184a4e7c0fce</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ConstPolicyPtrArray</type>
      <name>Policy::getValueArray&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>ace4ea0589a704c650fd5bf7905b80b75</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a4a96837919afbf95d967773f91d7bac6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a91ef9d41d58fa0293e520e5d1f1addf6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a3456670940d18c32d8eb0ef275f69089</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a2506ac950e53453ecaf6f5840ed330fb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abe3fb620fd6a16fdd8385b9cb4dc3c92</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>adb6bc835906022e56f6ee2255cd0bd73</anchor>
      <arglist>(const std::string &amp;name, const Policy::Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a01f12520ff3c5375da609ba8daf79370</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a76f3eaf4bbe825ceec0fc764410f1591</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a131794370e2fde7a5ee2cafef65b99ce</anchor>
      <arglist>(const std::string &amp;name, const Policy::Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a584f3ac494f3db54872cd99783577ba4</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtr &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PolicyConfigured.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_configured_8h</filename>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <class kind="class">lsst::pex::policy::PolicyConfigured</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyDestination.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_destination_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <class kind="class">lsst::pex::policy::PolicyDestination</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyFile.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_file_8h</filename>
    <includes id="_policy_source_8h" name="PolicySource.h" local="yes" imported="no">lsst/pex/policy/PolicySource.h</includes>
    <includes id="_supported_formats_8h" name="SupportedFormats.h" local="yes" imported="no">lsst/pex/policy/SupportedFormats.h</includes>
    <class kind="class">lsst::pex::policy::PolicyFile</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyParser.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_parser_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_policy_parser_factory_8h" name="PolicyParserFactory.h" local="yes" imported="no">lsst/pex/policy/PolicyParserFactory.h</includes>
    <class kind="class">lsst::pex::policy::PolicyParser</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyParserFactory.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_parser_factory_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <class kind="class">lsst::pex::policy::PolicyParserFactory</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicySource.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_source_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_policy_parser_factory_8h" name="PolicyParserFactory.h" local="yes" imported="no">lsst/pex/policy/PolicyParserFactory.h</includes>
    <includes id="_supported_formats_8h" name="SupportedFormats.h" local="yes" imported="no">lsst/pex/policy/SupportedFormats.h</includes>
    <class kind="class">lsst::pex::policy::PolicySource</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyStreamDestination.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_stream_destination_8h</filename>
    <includes id="_policy_destination_8h" name="PolicyDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyDestination.h</includes>
    <class kind="class">lsst::pex::policy::PolicyStreamDestination</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyString.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_string_8h</filename>
    <includes id="_policy_source_8h" name="PolicySource.h" local="yes" imported="no">lsst/pex/policy/PolicySource.h</includes>
    <includes id="_supported_formats_8h" name="SupportedFormats.h" local="yes" imported="no">lsst/pex/policy/SupportedFormats.h</includes>
    <class kind="class">lsst::pex::policy::PolicyString</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyStringDestination.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_string_destination_8h</filename>
    <includes id="_policy_stream_destination_8h" name="PolicyStreamDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyStreamDestination.h</includes>
    <class kind="class">lsst::pex::policy::PolicyStringDestination</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyWriter.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_policy_writer_8h</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <class kind="class">lsst::pex::policy::PolicyWriter</class>
    <namespace>lsst::pex::policy</namespace>
    <member kind="define">
      <type>#define</type>
      <name>NULL_FILENAME</name>
      <anchorfile>_policy_writer_8h.html</anchorfile>
      <anchor>a39b53ea7b57d6d26eaaef96e6b641e20</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>SupportedFormats.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_supported_formats_8h</filename>
    <includes id="_policy_parser_factory_8h" name="PolicyParserFactory.h" local="yes" imported="no">lsst/pex/policy/PolicyParserFactory.h</includes>
    <class kind="class">lsst::pex::policy::SupportedFormats</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>UrnPolicyFile.h</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>_urn_policy_file_8h</filename>
    <includes id="_default_policy_file_8h" name="DefaultPolicyFile.h" local="yes" imported="no">lsst/pex/policy/DefaultPolicyFile.h</includes>
    <class kind="class">lsst::pex::policy::UrnPolicyFile</class>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/</path>
    <filename>____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst.html</anchorfile>
      <anchor>ab03431c25730b60ae821ebb4c8f74538</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/pex/</path>
    <filename>pex_2____init_____8py</filename>
    <member kind="variable">
      <type>tuple</type>
      <name>__path__</name>
      <anchorfile>namespacelsst_1_1pex.html</anchorfile>
      <anchor>a911b93fdf688c7be7c768463f2d8d041</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>__init__.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/pex/policy/</path>
    <filename>pex_2policy_2____init_____8py</filename>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>version.py</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/pex/policy/</path>
    <filename>version_8py</filename>
    <namespace>lsst::pex::policy::version</namespace>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a038554e57ca3e54029cec7f057072506</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>aa53e26dd6fa0620329dcd13ebb690542</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a69b9fb72046b6b5121086342ecc3e707</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a540434244e4b2a2e981b6d2c4519e585</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a8da3815b2586287632a7ad95ad147598</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>ab8c6bbbf1055978117267fb8850e717e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a2e098be756fdd85c8e8a9047353478ba</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>DefaultPolicyFile.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_default_policy_file_8cc</filename>
    <includes id="_default_policy_file_8h" name="DefaultPolicyFile.h" local="yes" imported="no">lsst/pex/policy/DefaultPolicyFile.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>Dictionary.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_dictionary_8cc</filename>
    <includes id="_dictionary_8h" name="Dictionary.h" local="yes" imported="no">lsst/pex/policy/Dictionary.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>exceptions.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>exceptions_8cc</filename>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
  </compound>
  <compound kind="file">
    <name>PAFParser.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/paf/</path>
    <filename>_p_a_f_parser_8cc</filename>
    <includes id="_p_a_f_parser_8h" name="PAFParser.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParser.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <includes id="_urn_policy_file_8h" name="UrnPolicyFile.h" local="yes" imported="no">lsst/pex/policy/UrnPolicyFile.h</includes>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>PAFParserFactory.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/paf/</path>
    <filename>_p_a_f_parser_factory_8cc</filename>
    <includes id="_p_a_f_parser_factory_8h" name="PAFParserFactory.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParserFactory.h</includes>
    <includes id="_p_a_f_parser_8h" name="PAFParser.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParser.h</includes>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>PAFWriter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/paf/</path>
    <filename>_p_a_f_writer_8cc</filename>
    <includes id="_p_a_f_writer_8h" name="PAFWriter.h" local="yes" imported="no">lsst/pex/policy/paf/PAFWriter.h</includes>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <namespace>lsst::pex::policy</namespace>
    <namespace>lsst::pex::policy::paf</namespace>
  </compound>
  <compound kind="file">
    <name>parserexceptions.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>parserexceptions_8cc</filename>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
  </compound>
  <compound kind="file">
    <name>Policy.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_8cc</filename>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <includes id="_urn_policy_file_8h" name="UrnPolicyFile.h" local="yes" imported="no">lsst/pex/policy/UrnPolicyFile.h</includes>
    <includes id="_policy_source_8h" name="PolicySource.h" local="yes" imported="no">lsst/pex/policy/PolicySource.h</includes>
    <includes id="_dictionary_8h" name="Dictionary.h" local="yes" imported="no">lsst/pex/policy/Dictionary.h</includes>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyConfigured.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_configured_8cc</filename>
    <includes id="_policy_configured_8h" name="PolicyConfigured.h" local="yes" imported="no">lsst/pex/policy/PolicyConfigured.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyDestination.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_destination_8cc</filename>
    <includes id="_policy_destination_8h" name="PolicyDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyDestination.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyFile.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_file_8cc</filename>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <includes id="_policy_parser_8h" name="PolicyParser.h" local="yes" imported="no">lsst/pex/policy/PolicyParser.h</includes>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
    <includes id="_p_a_f_parser_factory_8h" name="PAFParserFactory.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParserFactory.h</includes>
    <namespace>boost::filesystem</namespace>
    <namespace>lsst::pex::policy</namespace>
    <member kind="function">
      <type>path</type>
      <name>absolute</name>
      <anchorfile>namespaceboost_1_1filesystem.html</anchorfile>
      <anchor>a7e1b6d98cbdf1546fc1c926a2117b20c</anchor>
      <arglist>(const path &amp;p)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PolicyParser.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_parser_8cc</filename>
    <includes id="_policy_parser_8h" name="PolicyParser.h" local="yes" imported="no">lsst/pex/policy/PolicyParser.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyParserFactory.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_parser_factory_8cc</filename>
    <includes id="_policy_parser_factory_8h" name="PolicyParserFactory.h" local="yes" imported="no">lsst/pex/policy/PolicyParserFactory.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicySource.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_source_8cc</filename>
    <includes id="_policy_source_8h" name="PolicySource.h" local="yes" imported="no">lsst/pex/policy/PolicySource.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyStreamDestination.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_stream_destination_8cc</filename>
    <includes id="_policy_stream_destination_8h" name="PolicyStreamDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyStreamDestination.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyString.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_string_8cc</filename>
    <includes id="_policy_string_8h" name="PolicyString.h" local="yes" imported="no">lsst/pex/policy/PolicyString.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <includes id="_policy_parser_8h" name="PolicyParser.h" local="yes" imported="no">lsst/pex/policy/PolicyParser.h</includes>
    <includes id="exceptions_8h" name="exceptions.h" local="yes" imported="no">lsst/pex/policy/exceptions.h</includes>
    <includes id="parserexceptions_8h" name="parserexceptions.h" local="yes" imported="no">lsst/pex/policy/parserexceptions.h</includes>
    <includes id="_p_a_f_parser_factory_8h" name="PAFParserFactory.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParserFactory.h</includes>
    <namespace>lsst::pex::policy</namespace>
    <member kind="define">
      <type>#define</type>
      <name>PolStr_ERROR_MSG</name>
      <anchorfile>_policy_string_8cc.html</anchorfile>
      <anchor>a836436adeb532b08eb7dc8dbf5409c8e</anchor>
      <arglist>(use, msg, input)</arglist>
    </member>
  </compound>
  <compound kind="file">
    <name>PolicyStringDestination.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_string_destination_8cc</filename>
    <includes id="_policy_string_destination_8h" name="PolicyStringDestination.h" local="yes" imported="no">lsst/pex/policy/PolicyStringDestination.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>PolicyWriter.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_policy_writer_8cc</filename>
    <includes id="_policy_writer_8h" name="PolicyWriter.h" local="yes" imported="no">lsst/pex/policy/PolicyWriter.h</includes>
    <includes id="_policy_8h" name="Policy.h" local="yes" imported="no">lsst/pex/policy/Policy.h</includes>
    <includes id="_policy_file_8h" name="PolicyFile.h" local="yes" imported="no">lsst/pex/policy/PolicyFile.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>SupportedFormats.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_supported_formats_8cc</filename>
    <includes id="_supported_formats_8h" name="SupportedFormats.h" local="yes" imported="no">lsst/pex/policy/SupportedFormats.h</includes>
    <includes id="_p_a_f_parser_factory_8h" name="PAFParserFactory.h" local="yes" imported="no">lsst/pex/policy/paf/PAFParserFactory.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="file">
    <name>UrnPolicyFile.cc</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>_urn_policy_file_8cc</filename>
    <includes id="_urn_policy_file_8h" name="UrnPolicyFile.h" local="yes" imported="no">lsst/pex/policy/UrnPolicyFile.h</includes>
    <namespace>lsst::pex::policy</namespace>
  </compound>
  <compound kind="page">
    <name>pageDictionary</name>
    <title>The Dictionary Schema</title>
    <filename>page_dictionary</filename>
    <docanchor file="page_dictionary">secDictLeadingLine</docanchor>
    <docanchor file="page_dictionary">secDictTopLevel</docanchor>
    <docanchor file="page_dictionary">secDictdef</docanchor>
    <docanchor file="page_dictionary">secDictdefparam</docanchor>
  </compound>
  <compound kind="page">
    <name>pexPolicyMain</name>
    <title></title>
    <filename>pex_policy_main</filename>
    <docanchor file="pex_policy_main">secPolicyIntro</docanchor>
    <docanchor file="pex_policy_main">secPolicy</docanchor>
    <docanchor file="pex_policy_main">secLoading</docanchor>
    <docanchor file="pex_policy_main">secDictionary</docanchor>
    <docanchor file="pex_policy_main">secDefaults</docanchor>
  </compound>
  <compound kind="page">
    <name>pagePAF</name>
    <title>The Policy Authoring Format (PAF)</title>
    <filename>page_p_a_f</filename>
    <docanchor file="page_p_a_f">secPAFLeadingLine</docanchor>
    <docanchor file="page_p_a_f">secPAFSpacing</docanchor>
    <docanchor file="page_p_a_f">secPAFParamNames</docanchor>
    <docanchor file="page_p_a_f">secPAFfloat</docanchor>
    <docanchor file="page_p_a_f">secPAFbool</docanchor>
    <docanchor file="page_p_a_f">secPAFstring</docanchor>
    <docanchor file="page_p_a_f">secPAFarray</docanchor>
    <docanchor file="page_p_a_f">secPAFpolicy</docanchor>
    <docanchor file="page_p_a_f">secPAFpolicyArrays</docanchor>
    <docanchor file="page_p_a_f">secPAFinclude</docanchor>
    <docanchor file="page_p_a_f">secURNinclude</docanchor>
  </compound>
  <compound kind="namespace">
    <name>boost::filesystem</name>
    <filename>namespaceboost_1_1filesystem.html</filename>
    <member kind="function">
      <type>path</type>
      <name>absolute</name>
      <anchorfile>namespaceboost_1_1filesystem.html</anchorfile>
      <anchor>a7e1b6d98cbdf1546fc1c926a2117b20c</anchor>
      <arglist>(const path &amp;p)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::policy</name>
    <filename>namespacelsst_1_1pex_1_1policy.html</filename>
    <namespace>lsst::pex::policy::paf</namespace>
    <namespace>lsst::pex::policy::version</namespace>
    <class kind="class">lsst::pex::policy::DefaultPolicyFile</class>
    <class kind="class">lsst::pex::policy::BadNameError</class>
    <class kind="class">lsst::pex::policy::DictionaryError</class>
    <class kind="class">lsst::pex::policy::NameNotFound</class>
    <class kind="class">lsst::pex::policy::TypeError</class>
    <class kind="class">lsst::pex::policy::ParserError</class>
    <class kind="class">lsst::pex::policy::EOFError</class>
    <class kind="class">lsst::pex::policy::SyntaxError</class>
    <class kind="class">lsst::pex::policy::FormatSyntaxError</class>
    <class kind="class">lsst::pex::policy::UnsupportedSyntax</class>
    <class kind="class">lsst::pex::policy::Policy</class>
    <class kind="class">lsst::pex::policy::PolicyConfigured</class>
    <class kind="class">lsst::pex::policy::PolicyDestination</class>
    <class kind="class">lsst::pex::policy::PolicyFile</class>
    <class kind="class">lsst::pex::policy::PolicyParser</class>
    <class kind="class">lsst::pex::policy::PolicyParserFactory</class>
    <class kind="class">lsst::pex::policy::PolicySource</class>
    <class kind="class">lsst::pex::policy::PolicyStreamDestination</class>
    <class kind="class">lsst::pex::policy::PolicyString</class>
    <class kind="class">lsst::pex::policy::PolicyStringDestination</class>
    <class kind="class">lsst::pex::policy::PolicyWriter</class>
    <class kind="class">lsst::pex::policy::SupportedFormats</class>
    <class kind="class">lsst::pex::policy::UrnPolicyFile</class>
    <member kind="function">
      <type>std::ostream &amp;</type>
      <name>operator&lt;&lt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abcb847b27afe2f2b8d5bea87f0bdd96d</anchor>
      <arglist>(std::ostream &amp;os, const Policy &amp;p)</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>Policy::getValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a6893991d61842731eb89cb01c44078d2</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::FilePtr</type>
      <name>Policy::getValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a25ba5e056300296362702756acce645a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ConstPtr</type>
      <name>Policy::getValue&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abedb897960f26d6afe7443ff1e652be6</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; std::string &gt;</type>
      <name>Policy::getValueArray&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>aa6b187fa84f4a0ff7e9e70877f07aa27</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::FilePtrArray</type>
      <name>Policy::getValueArray&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>ac074f6272a55efe43a8fab3d00c9e3ce</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::PolicyPtrArray</type>
      <name>Policy::getValueArray&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a8444bec7676b84adf59f184a4e7c0fce</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ConstPolicyPtrArray</type>
      <name>Policy::getValueArray&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>ace4ea0589a704c650fd5bf7905b80b75</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a4a96837919afbf95d967773f91d7bac6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a91ef9d41d58fa0293e520e5d1f1addf6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a3456670940d18c32d8eb0ef275f69089</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Policy::ValueType</type>
      <name>Policy::getValueType&lt; Policy::ConstPtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a2506ac950e53453ecaf6f5840ed330fb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>abe3fb620fd6a16fdd8385b9cb4dc3c92</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>adb6bc835906022e56f6ee2255cd0bd73</anchor>
      <arglist>(const std::string &amp;name, const Policy::Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::setValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a01f12520ff3c5375da609ba8daf79370</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; std::string &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a76f3eaf4bbe825ceec0fc764410f1591</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; Policy::Ptr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a131794370e2fde7a5ee2cafef65b99ce</anchor>
      <arglist>(const std::string &amp;name, const Policy::Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Policy::addValue&lt; Policy::FilePtr &gt;</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy.html</anchorfile>
      <anchor>a584f3ac494f3db54872cd99783577ba4</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtr &amp;value)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::DefaultPolicyFile</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</filename>
    <base>lsst::pex::policy::PolicyFile</base>
    <member kind="function">
      <type></type>
      <name>DefaultPolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a116e091d54b03c44abd097314ab204cd</anchor>
      <arglist>(const char *const productName, const std::string &amp;filepath, const std::string &amp;repos=&quot;&quot;, bool strict=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::filesystem::path</type>
      <name>getInstallPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a5a15ef7263d32b88420b2559361ce28a</anchor>
      <arglist>(const char *const productName)</arglist>
    </member>
    <member kind="function">
      <type>const boost::filesystem::path &amp;</type>
      <name>getRepositoryPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a584180476b694ffd2a18b2d43256b258</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>ae5c5fb01a3b512e1df9336b4f4a7ea10</anchor>
      <arglist>(Policy &amp;policy) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>aea76c806dc8fed7c01da47dd586be993</anchor>
      <arglist>(const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>getPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8d6bb8ffe703648bf075ba3d0249b729</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a171c1ce636cb126020901e514e500a88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a1add404b87cb278ab1e7a15407615910</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a2f659bd7298229a776e43aa6b620cc59</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ab247525b6eff00477c6879106a76868b</anchor>
      <arglist>(const std::string &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af97e535a40a456b1304e6f48256c2c7d</anchor>
      <arglist>(const char *filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afee12d6131a3bafe5232dbcd947506e9</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a3a2982f642075d92b8817766e6ed620f</anchor>
      <arglist>(const std::string &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8b4f5a348d5ce2b47a59d3f4698079dc</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7ab1e98431cca7ea9338775f95c2ef4a</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a531272370c110e0e7827743add0c1f9a</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7d4021b4aeee034997b2876d54a872ad</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a6028042dc1601bd8941d1f60e73c377b</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af43438c3418155b1b351b674220eb0cb</anchor>
      <arglist>(Policy &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a1b850fe11329edd33bc298f237cdb359</anchor>
      <arglist>(SupportedFormats::Ptr fmts=defaultFormats)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a851b8375c2a4f3985563195b24028e84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static boost::filesystem::path</type>
      <name>installPathFor</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a57ecfc25bcc86c76a6ddd0ef9b666826</anchor>
      <arglist>(const char *const productName)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>EXT_PAF</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7d6bc065d5fbe3e090d9059b65b868cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>EXT_XML</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afb9f8fadb7b26b657ba0a7380d07960c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>SPACE_RE</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ab802be2c84260f8c3acc79f00df659c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>COMMENT</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afa13e0310e383334544844c41f04dacd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>CONTENTID</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a1b505fadb274c72bb54bcc7c735a82da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::filesystem::path</type>
      <name>_file</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a242d188083598a09955618134a5fe4d2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::BadNameError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_bad_name_error.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>BadNameError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_bad_name_error.html</anchorfile>
      <anchor>a9f8780370f26036bcb557a1993d1221f</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>BadNameError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_bad_name_error.html</anchorfile>
      <anchor>a1e78d57dae7df9af4b411fb317c5f795</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func, const std::string &amp;badname)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_bad_name_error.html</anchorfile>
      <anchor>acefe4989da9090cfb6c083ababf4a2b6</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_bad_name_error.html</anchorfile>
      <anchor>a4416d640eaa1e2530ff451a8f9c84e27</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::DictionaryError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_dictionary_error.html</filename>
    <base>lsst::pex::exceptions::DomainErrorException</base>
    <member kind="function">
      <type></type>
      <name>DictionaryError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_dictionary_error.html</anchorfile>
      <anchor>aa3bd1d7a34bd79d1f7a13d1e5a12c548</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DictionaryError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_dictionary_error.html</anchorfile>
      <anchor>a16aebe3fb324b2e669c1902465d66f95</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_dictionary_error.html</anchorfile>
      <anchor>ad834bee9dd76b7f977261b73b8d2e6fa</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_dictionary_error.html</anchorfile>
      <anchor>a471290d2b8880dbc27c5a1b4d6edf6f5</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::NameNotFound</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_name_not_found.html</filename>
    <base>lsst::pex::exceptions::NotFoundException</base>
    <member kind="function">
      <type></type>
      <name>NameNotFound</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_name_not_found.html</anchorfile>
      <anchor>a19a49c9a4050a5ba602dece527078f1a</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>NameNotFound</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_name_not_found.html</anchorfile>
      <anchor>a7797d77ff0ae0823bf59fab92218489d</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func, const std::string &amp;parameter)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_name_not_found.html</anchorfile>
      <anchor>add484ea4a53b95bddfe4d008ed6d4d19</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_name_not_found.html</anchorfile>
      <anchor>a99dc5eecd88bb0e8034b337470f1f8e5</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::TypeError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_type_error.html</filename>
    <base>lsst::pex::exceptions::DomainErrorException</base>
    <member kind="function">
      <type></type>
      <name>TypeError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_type_error.html</anchorfile>
      <anchor>af4fee5736fa04dd14678ef94f5719d58</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>TypeError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_type_error.html</anchorfile>
      <anchor>a698b7f822b5212371cde1808e6f3b1db</anchor>
      <arglist>(char const *ex_file, int ex_line, char const *ex_func, const std::string &amp;parameter, const std::string &amp;expected)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_type_error.html</anchorfile>
      <anchor>ac5dd35503fc27d08c3be2e15d7292224</anchor>
      <arglist>(void) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_type_error.html</anchorfile>
      <anchor>a2f8032330fda718da7c4a69a0d81bd39</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::ParserError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_parser_error.html</filename>
    <base>lsst::pex::exceptions::RuntimeErrorException</base>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6c894c1993794d9ad760cad32b550a39</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a48c84d09a5393c81f0d62a2a83159b70</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>ad34c99447b728df6a7bbf8939b1be5b0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>aa5c1bfb40aa0c307efd1faca3d68e2b4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6d64887a59cdbf02b918a59710c21749</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>makeLocatedMessage</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>af19e5655f832f04d459c91dc6b8c76f7</anchor>
      <arglist>(const std::string &amp;msg, int lineno)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::EOFError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</filename>
    <base>lsst::pex::policy::ParserError</base>
    <member kind="function">
      <type></type>
      <name>EOFError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>aee6ea31555f1f89fd8c5ce93b3faae2f</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EOFError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>acb140498b3ee93104ec2534a8d3cac9b</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EOFError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>a28dc12b5333ffec78c71d3efe68f92fd</anchor>
      <arglist>(POL_EARGS_TYPED, int lineno)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>EOFError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>ae04439056062b81b099781909752da02</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>a280ff38f9fc803ba751b8713b2c08d3e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_e_o_f_error.html</anchorfile>
      <anchor>a2f984b377a0668177a32a9b0d866f57f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6c894c1993794d9ad760cad32b550a39</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a48c84d09a5393c81f0d62a2a83159b70</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>ad34c99447b728df6a7bbf8939b1be5b0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>makeLocatedMessage</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>af19e5655f832f04d459c91dc6b8c76f7</anchor>
      <arglist>(const std::string &amp;msg, int lineno)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::SyntaxError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</filename>
    <base>lsst::pex::policy::ParserError</base>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a289e79975120187cf5f3033ee9039c19</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a2edd035d7c002976923d91361854418b</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a594fe9287deb7cec5f1787c6201017b5</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a93d613fd69161a064bf8829c6eb7477b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>aa09da30b8ea476176dcc8c8ea44234e5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6c894c1993794d9ad760cad32b550a39</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a48c84d09a5393c81f0d62a2a83159b70</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>ad34c99447b728df6a7bbf8939b1be5b0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>makeLocatedMessage</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>af19e5655f832f04d459c91dc6b8c76f7</anchor>
      <arglist>(const std::string &amp;msg, int lineno)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::FormatSyntaxError</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</filename>
    <base>lsst::pex::policy::SyntaxError</base>
    <member kind="function">
      <type></type>
      <name>FormatSyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</anchorfile>
      <anchor>adb2c266c545994893b878618082b2eea</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatSyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</anchorfile>
      <anchor>ad30c478636c906c74b12ef75305561e1</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FormatSyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</anchorfile>
      <anchor>a20dd3485c0770f2de403af9ca7064dca</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</anchorfile>
      <anchor>ab90e76af9441c1bde9956f6057654276</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_format_syntax_error.html</anchorfile>
      <anchor>ab528f6516679ef93edd884f831ac1989</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a289e79975120187cf5f3033ee9039c19</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a2edd035d7c002976923d91361854418b</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a594fe9287deb7cec5f1787c6201017b5</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6c894c1993794d9ad760cad32b550a39</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a48c84d09a5393c81f0d62a2a83159b70</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>ad34c99447b728df6a7bbf8939b1be5b0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>makeLocatedMessage</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>af19e5655f832f04d459c91dc6b8c76f7</anchor>
      <arglist>(const std::string &amp;msg, int lineno)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::UnsupportedSyntax</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</filename>
    <base>lsst::pex::policy::SyntaxError</base>
    <member kind="function">
      <type></type>
      <name>UnsupportedSyntax</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</anchorfile>
      <anchor>a419f4f5cace2e327013f50ff160be1fc</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>UnsupportedSyntax</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</anchorfile>
      <anchor>a3a0b9f71cac7311ff07a970be999d282</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>UnsupportedSyntax</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</anchorfile>
      <anchor>a78fdb5f927c54becc0f6dc4c06e7dcd0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual char const *</type>
      <name>getType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</anchorfile>
      <anchor>a30827028c7482ac9dc216686dfdd1e15</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual lsst::pex::exceptions::Exception *</type>
      <name>clone</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_unsupported_syntax.html</anchorfile>
      <anchor>a54c2f02c4d00483819bc6e03a0d5beb7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a289e79975120187cf5f3033ee9039c19</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a2edd035d7c002976923d91361854418b</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SyntaxError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_syntax_error.html</anchorfile>
      <anchor>a594fe9287deb7cec5f1787c6201017b5</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a6c894c1993794d9ad760cad32b550a39</anchor>
      <arglist>(POL_EARGS_TYPED)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>a48c84d09a5393c81f0d62a2a83159b70</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ParserError</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>ad34c99447b728df6a7bbf8939b1be5b0</anchor>
      <arglist>(POL_EARGS_TYPED, const std::string &amp;msg, int lineno)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>makeLocatedMessage</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_parser_error.html</anchorfile>
      <anchor>af19e5655f832f04d459c91dc6b8c76f7</anchor>
      <arglist>(const std::string &amp;msg, int lineno)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::Policy</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <base>lsst::daf::base::Persistable</base>
    <member kind="enumeration">
      <name>ValueType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNDETERMINED</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a09fb69f782479901cafd1b619c2f2fc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>UNDEF</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a3e91c5c44cb8a3c676e2f6214f155386</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>BOOL</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a7ab947804bfc87235d4f0f899ea396da</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>INT</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a5bc059f09b11ac1352ad8a65e0a06037</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>DOUBLE</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a9eb90fcf81479d8e991102047aa8b6ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>STRING</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a4a454a60cf5572bdb7f26dd260c4d068</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>POLICY</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065aae2397d243bded3343d558d322302405</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <name>FILE</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a8d46d5de96cf2368ff40815a69065a0ed9998bf2d91dfa3165670c4b3fa2c4</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Policy &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a0e419b8db15805df761eb5d8503a4d14</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; const Policy &gt;</type>
      <name>ConstPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad9bc8b0f9568918f70c89581d4297349</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; Dictionary &gt;</type>
      <name>DictPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3c7f0c882ea3d0853ce451333f75e433</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; const Dictionary &gt;</type>
      <name>ConstDictPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab5de18cb1e79ade9c3fa51a961fa28cf</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PolicyFile &gt;</type>
      <name>FilePtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3a1c4e08c9f78f51f792376d99ac595d</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; bool &gt;</type>
      <name>BoolArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>abedfbe7574ec2ebdfd4c75d20247d543</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; int &gt;</type>
      <name>IntArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>adf815a56b9dfa8d298b60f2f209214b9</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; double &gt;</type>
      <name>DoubleArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a0f98a436875bb0adaed11ee835d6b655</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; std::string &gt;</type>
      <name>StringArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a6f1b7a9fc5661930885f6c230d748c8b</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; Ptr &gt;</type>
      <name>PolicyPtrArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a54aa545c630cfc1e4028d96594a8ef79</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; FilePtr &gt;</type>
      <name>FilePtrArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a37709b2ac23238c5e8654fb28ca75780</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>std::vector&lt; ConstPtr &gt;</type>
      <name>ConstPolicyPtrArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4e3035b15957fde6bef6e304dc978322</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a843b16ad8929e6ad380aab279a1788d0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>afce1bc24b89128285d79680a2392464c</anchor>
      <arglist>(const PolicySource &amp;source)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a9d6484eacfdae4b21cc36f82c5bfea46</anchor>
      <arglist>(bool validate, const Dictionary &amp;dict, const boost::filesystem::path &amp;repository=&quot;&quot;)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a1325e23f037b9ea8979e14373bc30e4c</anchor>
      <arglist>(Policy &amp;pol, bool deep=false)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a41432b5899fab598fc504520220eb769</anchor>
      <arglist>(const Policy &amp;pol)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3b57ae41212e31701282f7e3c765446d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>nameCount</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4e58a20d7f6b4e46f6987f4e136a4bbc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isDictionary</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a2002296ffaa016097f662f4af3b28f47</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>canValidate</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a2077caef2751eb036d0f208c129ee86b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>const ConstDictPtr</type>
      <name>getDictionary</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>abe6821da1303a9ff1808e926674fd0a1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setDictionary</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a89f58ac3f3b1b264b8d461958fb5cc95</anchor>
      <arglist>(const Dictionary &amp;dict)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>validate</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a7fc7a00acfe905646570948e330617e7</anchor>
      <arglist>(ValidationError *errs=0) const </arglist>
    </member>
    <member kind="function">
      <type>size_t</type>
      <name>valueCount</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a14d0a3d6782c502e51f63a9b2f08e55f</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a7cde9f57031b9286f2df5fadf8104b1b</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af5446be9f7e657620db492778943e0a2</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isBool</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4a179ae8377f5da57d95d945b7b1191a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isInt</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af7dc85e83722b743b151790bba001acf</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isDouble</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>abafe626eb78446b38467858487445f5c</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a8d9d678077dbe9a2befcb69f288925df</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>aceb963f62703aa00c0059719b4d3f4b3</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab1572d4e9c309006d322a9b04ccc456b</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const std::type_info &amp;</type>
      <name>getTypeInfo</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>aff55141b26df30205e640b0b0cebcd68</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const std::type_info &amp;</type>
      <name>typeOf</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad9f6fc052266150d48c9c21ef048d28a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>ValueType</type>
      <name>getValueType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>afac00813dcf9ee9e04fd6fd02b4e937a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getTypeName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a6613b3dce18e3193fe74c036fd58db8a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>T</type>
      <name>getValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad34677787a34f5fd62dce30c32df16d6</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>std::vector&lt; T &gt;</type>
      <name>getValueArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af93c8340f6338c3ccef15a32e5708839</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>FilePtr</type>
      <name>getFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a7a269f291e4252a432a5ec2776385eac</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getBool</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a1255a2d3bcd7f0decfc2c4497d3b0839</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getInt</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4518a1a8a8df4facf5ca49c5c9ed14c4</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>double</type>
      <name>getDouble</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a45451d6add2e45a1a06db9c1a0d8d91f</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>getString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad1eaa82260d1a802b6929b410ee0735a</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>FilePtrArray</type>
      <name>getFileArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a19a97ada23833d2aa26078f4b24b9747</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>remove</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a769f232f5872d730cdb2c87cd77daee4</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>loadPolicyFiles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>adaca6ebb23679163b76e157b55deb44b</anchor>
      <arglist>(bool strict=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>loadPolicyFiles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af1af3d49d10d3cec1a9e9f32333638f0</anchor>
      <arglist>(const boost::filesystem::path &amp;repository, bool strict=true)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>mergeDefaults</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3bff27ca64468dfd4ecd1df0938ddd3b</anchor>
      <arglist>(const Policy &amp;defaultPol, bool keepForValidation=true, ValidationError *errs=0)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::string</type>
      <name>str</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a97a6010d21a64ea68c38e2c998a11e46</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;indent=&quot;&quot;) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>print</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a43ac37223c73714aaeb3fbd2f25ee022</anchor>
      <arglist>(std::ostream &amp;out, const std::string &amp;label=&quot;Policy&quot;, const std::string &amp;indent=&quot;&quot;) const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ac5119ca76b0307dd1f76d631f1ac93ca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>lsst::daf::base::PropertySet::Ptr</type>
      <name>asPropertySet</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3ea0cfbba545c6a2b6a34995a53cdf45</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>afd1d5882030fe93c33ca6b95317a6e8e</anchor>
      <arglist>(const std::string &amp;name, const bool &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>aa199d24400c8dc8a3b7d39df3eadf566</anchor>
      <arglist>(const std::string &amp;name, const int &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>afb7e1390785b325d6d8b3e0878b647dc</anchor>
      <arglist>(const std::string &amp;name, const double &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a6dd6d34b9cdee72dc163b6840f9a9f79</anchor>
      <arglist>(const std::string &amp;name, const bool &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a0d2c1bfcecbabb4a00b712389622c3e4</anchor>
      <arglist>(const std::string &amp;name, const int &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a70ddd492dac3871cee7cbeeb0e7be8d6</anchor>
      <arglist>(const std::string &amp;name, const double &amp;value)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a50978d68dc0b274b3d732477fe6a8571</anchor>
      <arglist>(const std::string &amp;pathOrUrn)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>aca1b9464d5eb0f43d4f75286e45fe62f</anchor>
      <arglist>(const char *pathOrUrn)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>names</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab071e8dfa72e5c7f34f5aeeab8606e64</anchor>
      <arglist>(std::list&lt; std::string &gt; &amp;names, bool topLevelOnly=false, bool append=false) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>paramNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad4489f8c27879121b170f95a490a2f56</anchor>
      <arglist>(std::list&lt; std::string &gt; &amp;names, bool topLevelOnly=false, bool append=false) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>policyNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a492b258f59c613881203cdd3880150f3</anchor>
      <arglist>(std::list&lt; std::string &gt; &amp;names, bool topLevelOnly=false, bool append=false) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>fileNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a9ba3bfea4d798f20f4918c51b4594144</anchor>
      <arglist>(std::list&lt; std::string &gt; &amp;names, bool topLevelOnly=false, bool append=false) const </arglist>
    </member>
    <member kind="function">
      <type>StringArray</type>
      <name>names</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a622379c671c85a82450004d17934a5cd</anchor>
      <arglist>(bool topLevelOnly=false) const </arglist>
    </member>
    <member kind="function">
      <type>StringArray</type>
      <name>paramNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad572062f44ec272555dbdf3e4c8b5ea4</anchor>
      <arglist>(bool topLevelOnly=false) const </arglist>
    </member>
    <member kind="function">
      <type>StringArray</type>
      <name>policyNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3373c211c78a0c5731883bae6f09cfe0</anchor>
      <arglist>(bool topLevelOnly=false) const </arglist>
    </member>
    <member kind="function">
      <type>StringArray</type>
      <name>fileNames</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a5071730db7076f13311d4bf47721ff16</anchor>
      <arglist>(bool topLevelOnly=false) const </arglist>
    </member>
    <member kind="function">
      <type>ConstPtr</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4c1f3068e5cf6841fda9e82e8d813172</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>Ptr</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af7b0dd068f1441db4fd40c254183ff30</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function">
      <type>PolicyPtrArray</type>
      <name>getPolicyArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a87561e35e5d1665ff6c267bcba7d87da</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>ConstPolicyPtrArray</type>
      <name>getConstPolicyArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>acafb03e1c061c8b4d75e8dd2b8751978</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>BoolArray</type>
      <name>getBoolArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a25ed9c73db7dd87c203a71b6362b3949</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>IntArray</type>
      <name>getIntArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a06254044bf4ceb107cfb859ae199f8e0</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>DoubleArray</type>
      <name>getDoubleArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a5d75cdae341149c7bd89a2c735659b0b</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>StringArray</type>
      <name>getStringArray</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4d70dadec4d36a35c7f2f4b426bb67a8</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a2a141efc781d4a8dee708d6c3da54546</anchor>
      <arglist>(const std::string &amp;name, const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ac9fc034dcb0f94d5f6b469cfa29f7263</anchor>
      <arglist>(const std::string &amp;name, const Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ac5def1f08579058baa142936cc938ac6</anchor>
      <arglist>(const std::string &amp;name, const FilePtr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a1ae03e4bf1680a2dc8f434e8e7352722</anchor>
      <arglist>(const std::string &amp;name, bool value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a592f4133b552345b66bde470c5dff720</anchor>
      <arglist>(const std::string &amp;name, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>afa58e575f327bf0fea79c498bfe8b788</anchor>
      <arglist>(const std::string &amp;name, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ae30b01820bbec445cc7da45f0e765762</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ac9ad5e0e16b022b5101fc7281d959c45</anchor>
      <arglist>(const std::string &amp;name, const char *value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>addValue</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad824d1cfab19b298f523ff3326d85017</anchor>
      <arglist>(const std::string &amp;name, const T &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>aaa41ff968eeca31427bf1652c8ca4914</anchor>
      <arglist>(const std::string &amp;name, const Ptr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a132a20cafbdff78f8d6b658552b56d76</anchor>
      <arglist>(const std::string &amp;name, const FilePtr &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a5f50a9259943210dc161b8ff1ee55b1d</anchor>
      <arglist>(const std::string &amp;name, bool value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a8e652197271ed8221eaa544613efa7e5</anchor>
      <arglist>(const std::string &amp;name, int value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4501da4e6f68f346d80c1434d6058970</anchor>
      <arglist>(const std::string &amp;name, double value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a0c5f00c41fb124aaf91f3c84b1c64c27</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>add</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a694e3bc6ade83fda23e6949752e2a7ec</anchor>
      <arglist>(const std::string &amp;name, const char *value)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicyFromUrn</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a4fbaf68c4d6c6d51a993c0d9391ba497</anchor>
      <arglist>(const std::string &amp;urn, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static FilePtr</type>
      <name>createPolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad062189f8932a3f354a3b56faf341136</anchor>
      <arglist>(const std::string &amp;pathOrUrn, bool strict=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ValueType</type>
      <name>getValueType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a606a3c62d5bc6cd0f6a7ec20ff5acc5e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ValueType</type>
      <name>getTypeByName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab4b7507a4be6b53388eb9a9ac817cbf3</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a3c4084ea67ea854e6b172c7e89ec0d71</anchor>
      <arglist>(PolicySource &amp;input, bool doIncludes=true, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a6dbde831c516224a83c03a9479ee9876</anchor>
      <arglist>(const std::string &amp;input, bool doIncludes=true, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>acace56b023c020771a8f547ff619c1e1</anchor>
      <arglist>(PolicySource &amp;input, const boost::filesystem::path &amp;repos, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a7db35d87492900d46476a90d50574b2e</anchor>
      <arglist>(PolicySource &amp;input, const std::string &amp;repos, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ad7176a1e79388c9ccfe0e029f7b11db0</anchor>
      <arglist>(PolicySource &amp;input, const char *repos, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>af3cd143f7ac5569a3cc36b6efa4bb8dc</anchor>
      <arglist>(const std::string &amp;input, const boost::filesystem::path &amp;repos, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab6bc171005dea50b2c01d83a0395797d</anchor>
      <arglist>(const std::string &amp;input, const std::string &amp;repos, bool validate=true)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Policy *</type>
      <name>createPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>ab6a60a9ecb2289681e30ee1ea60004a3</anchor>
      <arglist>(const std::string &amp;input, const char *repos, bool validate=true)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const char *const</type>
      <name>typeName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a67c969f1a95686c3b4271862bd93f4ad</anchor>
      <arglist>[]</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Policy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy.html</anchorfile>
      <anchor>a0ebc3cbd2c638c49768b15cbd3c05700</anchor>
      <arglist>(const lsst::daf::base::PropertySet::Ptr ps)</arglist>
    </member>
    <docanchor file="classlsst_1_1pex_1_1policy_1_1_policy">secPolicyLoading</docanchor>
    <docanchor file="classlsst_1_1pex_1_1policy_1_1_policy">secPolicyDefaults</docanchor>
    <docanchor file="classlsst_1_1pex_1_1policy_1_1_policy">secPolicyVer</docanchor>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyConfigured</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</filename>
    <member kind="typedef">
      <type>Policy::Ptr</type>
      <name>PolicyPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>aa014d2eecd31373378d735c9777c807c</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>Policy::ConstPtr</type>
      <name>ConstPolicyPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a89725259f4ac25b442e412541e414f32</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PolicySource &gt;</type>
      <name>PolicySourcePtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a2bd7d89c1bbf3c575d8e23d7243265e2</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyConfigured</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>ac313de1c78195c1b1d041e9fac052380</anchor>
      <arglist>(const PolicyPtr &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyConfigured</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>ad0ad4b9d193ce5616b9170b831d4f1ee</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyConfigured</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>ace73ce2352f9e291080c899aabe25986</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>ConstPolicyPtr</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>aff73bd3c1370f6cbfd5a16aa2467d906</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isConfigured</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>abc02a08c32f46cecc9e91286c7e50e2f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static PolicySourcePtr</type>
      <name>getDefaultPolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a3e99c28f9e5da728290821b077ca87c9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>PolicyPtr</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a63d176c0a479f2cd4df6c694858d6854</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>forgetPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a5882ae617aeab20307840b111f1e05a7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected">
      <type>void</type>
      <name>configured</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>afdf6f95a5135e93f7ae3fa0b46559f4b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>done</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_configured.html</anchorfile>
      <anchor>a81ae0bb182e66b269b2e1b5baa165e4c</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyDestination</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ae6cfae4df2c44b175b370a5f1b42c926</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>a1a84e670def5cbd0649e84b7e7edc7fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual std::ostream &amp;</type>
      <name>getOutputStream</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ad44046cdeb2a25ab8c342990ef79c41b</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ab97276746ed78d839431fc5531170ab1</anchor>
      <arglist>(const PolicyDestination &amp;that)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyFile</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_file.html</filename>
    <base>lsst::pex::policy::PolicySource</base>
    <base>lsst::daf::base::Persistable</base>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>aea76c806dc8fed7c01da47dd586be993</anchor>
      <arglist>(const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>getPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8d6bb8ffe703648bf075ba3d0249b729</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a171c1ce636cb126020901e514e500a88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a1add404b87cb278ab1e7a15407615910</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a2f659bd7298229a776e43aa6b620cc59</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ab247525b6eff00477c6879106a76868b</anchor>
      <arglist>(const std::string &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af97e535a40a456b1304e6f48256c2c7d</anchor>
      <arglist>(const char *filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afee12d6131a3bafe5232dbcd947506e9</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a3a2982f642075d92b8817766e6ed620f</anchor>
      <arglist>(const std::string &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8b4f5a348d5ce2b47a59d3f4698079dc</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7ab1e98431cca7ea9338775f95c2ef4a</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a531272370c110e0e7827743add0c1f9a</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7d4021b4aeee034997b2876d54a872ad</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a6028042dc1601bd8941d1f60e73c377b</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ad78afeec1d22eac32616607ee8f5322c</anchor>
      <arglist>(Policy &amp;policy) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af43438c3418155b1b351b674220eb0cb</anchor>
      <arglist>(Policy &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a1b850fe11329edd33bc298f237cdb359</anchor>
      <arglist>(SupportedFormats::Ptr fmts=defaultFormats)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a851b8375c2a4f3985563195b24028e84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>EXT_PAF</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7d6bc065d5fbe3e090d9059b65b868cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>EXT_XML</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afb9f8fadb7b26b657ba0a7380d07960c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>SPACE_RE</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ab802be2c84260f8c3acc79f00df659c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>COMMENT</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afa13e0310e383334544844c41f04dacd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>CONTENTID</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a1b505fadb274c72bb54bcc7c735a82da</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static SupportedFormats::Ptr</type>
      <name>defaultFormats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a0cf4c2ccf9fd6909f190433555c4ffcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::filesystem::path</type>
      <name>_file</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a242d188083598a09955618134a5fe4d2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>SupportedFormats::Ptr</type>
      <name>_formats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a4d010abff1afd6be3107caf2031e157d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyParser</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function">
      <type></type>
      <name>PolicyParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>adc16938c8b55950d60f12e0f64385d35</anchor>
      <arglist>(Policy &amp;policy, bool strict=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>af77af34be37eafaaa0356fff137a9092</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isStrict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>ab8537e1f6e154b10cb97998a757aea44</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStrict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>acdaf908212faa3a882dda6bf349b63bc</anchor>
      <arglist>(bool strict)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual int</type>
      <name>parse</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a71bdc3cca5850cdfb887a1d62984c0da</anchor>
      <arglist>(std::istream &amp;is)=0</arglist>
    </member>
    <member kind="function">
      <type>Policy &amp;</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a8c2dc593592c43440ed9f03b0efc6790</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Policy &amp;</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>af914480aafaacb4178adb5af44bc07b2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Policy &amp;</type>
      <name>_pol</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a4590f3ce0846c08b42956bd245211db9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_strict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a0905e85774b5c69476fc7c899da69546</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyParserFactory</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</filename>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PolicyParserFactory &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>ae81ca1802e636e52df3d49aceba2a35a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyParserFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>ae9e7ec506767a9a427b5c5dc335f1ccb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyParserFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>abb446f8194897911870e644d5def04c3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual PolicyParser *</type>
      <name>createParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>a99026a5213fd66fbe1c17f6e458ac882</anchor>
      <arglist>(Policy &amp;policy, bool strict=true) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual bool</type>
      <name>isRecognized</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>a2aa91a9e252667bcaa14ffc41f4813a7</anchor>
      <arglist>(const std::string &amp;leaders) const =0</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>a600d07ba6a975ec91ffdaf140023724b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>UNRECOGNIZED</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>a4445ca6c96d2a2cd87e26c172644bead</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicySource</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_source.html</filename>
    <base>lsst::daf::base::Citizen</base>
    <member kind="function">
      <type></type>
      <name>PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a1b850fe11329edd33bc298f237cdb359</anchor>
      <arglist>(SupportedFormats::Ptr fmts=defaultFormats)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a851b8375c2a4f3985563195b24028e84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a9553eb082f7efe53febc706dfe47e686</anchor>
      <arglist>()=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>abb9a94a3224425ae213dfad5d3f650a7</anchor>
      <arglist>(Policy &amp;policy) const =0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>ad54de7fe2d691daa8d57291a13bb88d8</anchor>
      <arglist>(Policy &amp;policy)=0</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static SupportedFormats::Ptr</type>
      <name>defaultFormats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a0cf4c2ccf9fd6909f190433555c4ffcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>SupportedFormats::Ptr</type>
      <name>_formats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a4d010abff1afd6be3107caf2031e157d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyStreamDestination</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</filename>
    <base>lsst::pex::policy::PolicyDestination</base>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; std::ostream &gt;</type>
      <name>StreamPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a8352e3df7b4dd9a704db5d236950f64e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyStreamDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a6f757d0fae6a4fa60985271661452ace</anchor>
      <arglist>(StreamPtr ostrm)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyStreamDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>ac09bfb487f7c474798d69b270bcb21ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>getOutputStream</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a31565b8b14768e0153a75f12f2f92c51</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ae6cfae4df2c44b175b370a5f1b42c926</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>a1a84e670def5cbd0649e84b7e7edc7fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>StreamPtr</type>
      <name>_ostrm</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a920cec14683981f41d027dd424a7f0a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ab97276746ed78d839431fc5531170ab1</anchor>
      <arglist>(const PolicyDestination &amp;that)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyString</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_string.html</filename>
    <base>lsst::pex::policy::PolicySource</base>
    <base>lsst::daf::base::Persistable</base>
    <member kind="function">
      <type></type>
      <name>PolicyString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>ad01b0d3b13142f5db6d54ddcc92f4851</anchor>
      <arglist>(const std::string &amp;data, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a6a2a9fda82e770830d85827d5f047562</anchor>
      <arglist>(const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getData</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a040e4fce361dac930f91d15891c56323</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a83cb09cfb111e5f79a9e5856af85e268</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>ad5ed06683545685805a18a91391c7340</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a2c1883bb4ac0a6fec30058550781d5c3</anchor>
      <arglist>(Policy &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a6b6c95cdefd72860a0f6f92c806d4852</anchor>
      <arglist>(Policy &amp;policy) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a1b850fe11329edd33bc298f237cdb359</anchor>
      <arglist>(SupportedFormats::Ptr fmts=defaultFormats)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a851b8375c2a4f3985563195b24028e84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>SPACE_RE</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>aa98f56070cb37447945a0c55f9601f8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>COMMENT</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>af7785a9c719e2dd7c93897f84fec0633</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>CONTENTID</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>acb068a5d9ba3f70556568bdbad09843d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static SupportedFormats::Ptr</type>
      <name>defaultFormats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a0cf4c2ccf9fd6909f190433555c4ffcf</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>_data</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string.html</anchorfile>
      <anchor>a03cd2437aa3ec43d595ae49d46fb1e1d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>SupportedFormats::Ptr</type>
      <name>_formats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a4d010abff1afd6be3107caf2031e157d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyStringDestination</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_string_destination.html</filename>
    <base>lsst::pex::policy::PolicyStreamDestination</base>
    <member kind="function">
      <type></type>
      <name>PolicyStringDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string_destination.html</anchorfile>
      <anchor>af1bb3d2ea13a4b1dc91cfd5e9a233199</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyStringDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string_destination.html</anchorfile>
      <anchor>a36ea8daf1e662ed34e851f28333b1247</anchor>
      <arglist>(const std::string &amp;str)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyStringDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string_destination.html</anchorfile>
      <anchor>a622c934ce8bce7d070d3455303506e33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>getData</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_string_destination.html</anchorfile>
      <anchor>a684dd4bbbf422e656a17edd2696dfc78</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyStreamDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a6f757d0fae6a4fa60985271661452ace</anchor>
      <arglist>(StreamPtr ostrm)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyStreamDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>ac09bfb487f7c474798d69b270bcb21ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual std::ostream &amp;</type>
      <name>getOutputStream</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a31565b8b14768e0153a75f12f2f92c51</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ae6cfae4df2c44b175b370a5f1b42c926</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>a1a84e670def5cbd0649e84b7e7edc7fc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; std::ostream &gt;</type>
      <name>StreamPtr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a8352e3df7b4dd9a704db5d236950f64e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>PolicyDestination</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_destination.html</anchorfile>
      <anchor>ab97276746ed78d839431fc5531170ab1</anchor>
      <arglist>(const PolicyDestination &amp;that)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>StreamPtr</type>
      <name>_ostrm</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_stream_destination.html</anchorfile>
      <anchor>a920cec14683981f41d027dd424a7f0a7</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::PolicyWriter</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</filename>
    <member kind="function">
      <type></type>
      <name>PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>ab510485e781cc7e7eccde5529bbe1dfa</anchor>
      <arglist>(std::ostream *out=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a8e58701a249f5520c6a3c5d680461bc6</anchor>
      <arglist>(const std::string &amp;file, bool append=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a667fea16d6be33a0c7eb51b26a3fd1a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a38f910190541ce4e85340a4513ce3440</anchor>
      <arglist>(const Policy &amp;policy, bool doDecl=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>close</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a787f9df9f2c191d76b1f4120406bebce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a6500dcebcca83473bc93dfdc42745825</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeBool</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a0e4326a3382dc59b8045850f5fcdf9b4</anchor>
      <arglist>(const std::string &amp;name, bool value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeInt</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a1c509540d384398256c054aed5ec6949</anchor>
      <arglist>(const std::string &amp;name, int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeDouble</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a88051d8c1fa18d0fb9c4932de55aee16</anchor>
      <arglist>(const std::string &amp;name, double value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a2d72257f19392019e0246c6e9d90e2fd</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writePolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a5460c2a36babb8194e51bc153d13b0f4</anchor>
      <arglist>(const std::string &amp;name, const Policy &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a75bb343daa7b6a26e57edc0a0d7e04c2</anchor>
      <arglist>(const std::string &amp;name, const PolicyFile &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeBools</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a558087a722566787e33c9dd79bcc3746</anchor>
      <arglist>(const std::string &amp;name, const Policy::BoolArray &amp;values)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeInts</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a46a36b981bd05c92658258433ca3dd4f</anchor>
      <arglist>(const std::string &amp;name, const Policy::IntArray &amp;values)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeDoubles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a3d6f2681ef3edd0267b7d1073f4f3608</anchor>
      <arglist>(const std::string &amp;name, const Policy::DoubleArray &amp;values)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeStrings</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>aa538f8aab895147834c5db6684b3f380</anchor>
      <arglist>(const std::string &amp;name, const Policy::StringArray &amp;values)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writePolicies</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a15dab1b68e0abe0f8cf2a9b60b83df7a</anchor>
      <arglist>(const std::string &amp;name, const Policy::PolicyPtrArray &amp;values)=0</arglist>
    </member>
    <member kind="function" virtualness="pure">
      <type>virtual void</type>
      <name>writeFiles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a90ffdb9ca54aeb0c96208795583bc8c6</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtrArray &amp;values)=0</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::ostream *</type>
      <name>_os</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>aebfdb2caa460dedf5487ccbdb4f7e16b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::SupportedFormats</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</filename>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; SupportedFormats &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>aef95a885634c5161ffa1dd8b259a28c9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>SupportedFormats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>add9c59d067517a79d8a7025d327a201c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>registerFormat</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>a7447de368acd396808d1bf3842d5b77c</anchor>
      <arglist>(const PolicyParserFactory::Ptr &amp;factory)</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>recognizeType</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>ae40bf92c0ff8f9c3021977ab9d74c9ce</anchor>
      <arglist>(const std::string &amp;leaders) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>supports</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>ab965fca47cb531cf0b9316de4b4d986b</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>PolicyParserFactory::Ptr</type>
      <name>getFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>a1dec6b5d19c68e54422d9ac8cd49e2e4</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>size</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>ad67f689ceb867b73249f8a5f4053d042</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>initDefaultFormats</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_supported_formats.html</anchorfile>
      <anchor>a3294460238ec9414ac8a3b409451fbe0</anchor>
      <arglist>(SupportedFormats &amp;sf)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::UrnPolicyFile</name>
    <filename>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</filename>
    <base>lsst::pex::policy::DefaultPolicyFile</base>
    <member kind="function">
      <type></type>
      <name>UrnPolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>a2c152e53904744cb26477cf9f22ea880</anchor>
      <arglist>(const std::string &amp;urn, bool strictUrn=false, bool strictLoads=true)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>DefaultPolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a116e091d54b03c44abd097314ab204cd</anchor>
      <arglist>(const char *const productName, const std::string &amp;filepath, const std::string &amp;repos=&quot;&quot;, bool strict=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual boost::filesystem::path</type>
      <name>getInstallPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a5a15ef7263d32b88420b2559361ce28a</anchor>
      <arglist>(const char *const productName)</arglist>
    </member>
    <member kind="function">
      <type>const boost::filesystem::path &amp;</type>
      <name>getRepositoryPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a584180476b694ffd2a18b2d43256b258</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>ae5c5fb01a3b512e1df9336b4f4a7ea10</anchor>
      <arglist>(Policy &amp;policy) const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>aea76c806dc8fed7c01da47dd586be993</anchor>
      <arglist>(const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type>const std::string</type>
      <name>getPath</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8d6bb8ffe703648bf075ba3d0249b729</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>exists</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a171c1ce636cb126020901e514e500a88</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a1add404b87cb278ab1e7a15407615910</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a2f659bd7298229a776e43aa6b620cc59</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>ab247525b6eff00477c6879106a76868b</anchor>
      <arglist>(const std::string &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af97e535a40a456b1304e6f48256c2c7d</anchor>
      <arglist>(const char *filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>afee12d6131a3bafe5232dbcd947506e9</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a3a2982f642075d92b8817766e6ed620f</anchor>
      <arglist>(const std::string &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a8b4f5a348d5ce2b47a59d3f4698079dc</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7ab1e98431cca7ea9338775f95c2ef4a</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a531272370c110e0e7827743add0c1f9a</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const SupportedFormats::Ptr &amp;fmts=defaultFormats)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a7d4021b4aeee034997b2876d54a872ad</anchor>
      <arglist>(const std::string &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a6028042dc1601bd8941d1f60e73c377b</anchor>
      <arglist>(const boost::filesystem::path &amp;filepath, const boost::filesystem::path &amp;reposDir, const PolicyParserFactory::Ptr &amp;parserFactory)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>load</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>af43438c3418155b1b351b674220eb0cb</anchor>
      <arglist>(Policy &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a1b850fe11329edd33bc298f237cdb359</anchor>
      <arglist>(SupportedFormats::Ptr fmts=defaultFormats)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicySource</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_source.html</anchorfile>
      <anchor>a851b8375c2a4f3985563195b24028e84</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>productNameFromUrn</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>ad993b4e2dca2f296cefca5472ac1df4b</anchor>
      <arglist>(const std::string &amp;urn, bool strictUrn=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>filePathFromUrn</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>afd4abfcaf225a9a9d39ff748524cf253</anchor>
      <arglist>(const std::string &amp;urn, bool strictUrn=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>reposFromUrn</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>a311c914d001dbe25bc28d1b2a42d1931</anchor>
      <arglist>(const std::string &amp;urn, bool strictUrn=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>looksLikeUrn</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>aaac4a7f2b4352c32cff6317644f73d8c</anchor>
      <arglist>(const std::string &amp;s, bool strict=false)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static boost::filesystem::path</type>
      <name>installPathFor</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_default_policy_file.html</anchorfile>
      <anchor>a57ecfc25bcc86c76a6ddd0ef9b666826</anchor>
      <arglist>(const char *const productName)</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>URN_PREFIX</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>a59fb81ced4159514e19a9e8f98e90617</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>URN_PREFIX_ABBREV</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_urn_policy_file.html</anchorfile>
      <anchor>a8b3410e15680c7c680460bfaa70e1d2c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>boost::filesystem::path</type>
      <name>_file</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_file.html</anchorfile>
      <anchor>a242d188083598a09955618134a5fe4d2</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::policy::paf</name>
    <filename>namespacelsst_1_1pex_1_1policy_1_1paf.html</filename>
    <class kind="class">lsst::pex::policy::paf::PAFParser</class>
    <class kind="class">lsst::pex::policy::paf::PAFParserFactory</class>
    <class kind="class">lsst::pex::policy::paf::PAFWriter</class>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::paf::PAFParser</name>
    <filename>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser.html</filename>
    <base>lsst::pex::policy::PolicyParser</base>
    <member kind="function">
      <type></type>
      <name>PAFParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser.html</anchorfile>
      <anchor>aa1426fb8e29f36179371246807bb1038</anchor>
      <arglist>(Policy &amp;policy)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PAFParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser.html</anchorfile>
      <anchor>a614b85a27ed3ad0a1b9fa96b7d49af3e</anchor>
      <arglist>(Policy &amp;policy, bool strict)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PAFParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser.html</anchorfile>
      <anchor>a6e4dfb733baf14a7e27e62764e88e822</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int</type>
      <name>parse</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser.html</anchorfile>
      <anchor>a47741e23d128f4cbbc4a64a93def9b95</anchor>
      <arglist>(std::istream &amp;is)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>adc16938c8b55950d60f12e0f64385d35</anchor>
      <arglist>(Policy &amp;policy, bool strict=true)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>af77af34be37eafaaa0356fff137a9092</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>isStrict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>ab8537e1f6e154b10cb97998a757aea44</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setStrict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>acdaf908212faa3a882dda6bf349b63bc</anchor>
      <arglist>(bool strict)</arglist>
    </member>
    <member kind="function">
      <type>Policy &amp;</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a8c2dc593592c43440ed9f03b0efc6790</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const Policy &amp;</type>
      <name>getPolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>af914480aafaacb4178adb5af44bc07b2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Policy &amp;</type>
      <name>_pol</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a4590f3ce0846c08b42956bd245211db9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>bool</type>
      <name>_strict</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser.html</anchorfile>
      <anchor>a0905e85774b5c69476fc7c899da69546</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::paf::PAFParserFactory</name>
    <filename>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</filename>
    <base>lsst::pex::policy::PolicyParserFactory</base>
    <member kind="function">
      <type></type>
      <name>PAFParserFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>abbab87045599b921fe2e8e8ad05c3148</anchor>
      <arglist>(const boost::regex &amp;contIdPatt=CONTENTID)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual PolicyParser *</type>
      <name>createParser</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>a7594fdda993ac6086a0be807e13f4297</anchor>
      <arglist>(Policy &amp;policy, bool strict=true) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual bool</type>
      <name>isRecognized</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>a4a98e292959d28ea2644999a6d80b7e4</anchor>
      <arglist>(const std::string &amp;leaders) const </arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual const std::string &amp;</type>
      <name>getFormatName</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>aeba4eb68082ec5b2a26d4ee9de578bff</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyParserFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>ae9e7ec506767a9a427b5c5dc335f1ccb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyParserFactory</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>abb446f8194897911870e644d5def04c3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>FORMAT_NAME</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>ac63d595887f8e95a765b87cf079a640e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>LEADER_PATTERN</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>a33fc681118c15a0598d91481036d13d7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const boost::regex</type>
      <name>CONTENTID</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_parser_factory.html</anchorfile>
      <anchor>a1781da440c70f7623f79fe150a01d8a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" static="yes">
      <type>static const std::string</type>
      <name>UNRECOGNIZED</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>a4445ca6c96d2a2cd87e26c172644bead</anchor>
      <arglist></arglist>
    </member>
    <member kind="typedef">
      <type>boost::shared_ptr&lt; PolicyParserFactory &gt;</type>
      <name>Ptr</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_parser_factory.html</anchorfile>
      <anchor>ae81ca1802e636e52df3d49aceba2a35a</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>lsst::pex::policy::paf::PAFWriter</name>
    <filename>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</filename>
    <base>lsst::pex::policy::PolicyWriter</base>
    <member kind="function">
      <type></type>
      <name>PAFWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a10c2be793704d1b87f6a149ea0dc034b</anchor>
      <arglist>(std::ostream *out=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PAFWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>ab9b04ed4c2b979b9d829e0bcfd6ecca3</anchor>
      <arglist>(std::ostream *out, const std::string indent)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PAFWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a72f64a5a59c67c7c2fb0156da3d8a12e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PAFWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a161a14732247df24e8a210b7fce29f94</anchor>
      <arglist>(const std::string &amp;file)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PAFWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a1219039d3a45b3c7d9e76f5f9a33820e</anchor>
      <arglist>(const char *file)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeBools</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>ad962ce00348aeece106f6400b3e4499c</anchor>
      <arglist>(const std::string &amp;name, const Policy::BoolArray &amp;values)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeInts</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a08669d104cb27ffed97685d009c90f9a</anchor>
      <arglist>(const std::string &amp;name, const Policy::IntArray &amp;values)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeDoubles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>a3734b329433a96a327657513e6b6e086</anchor>
      <arglist>(const std::string &amp;name, const Policy::DoubleArray &amp;values)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeStrings</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>ac731f914b98172edb4796ea286b8ece2</anchor>
      <arglist>(const std::string &amp;name, const Policy::StringArray &amp;values)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writePolicies</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>ac9fa459fcb70aad13eaa896181aa1a6a</anchor>
      <arglist>(const std::string &amp;name, const Policy::PolicyPtrArray &amp;values)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeFiles</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>aed4c8a4bd90e34f883dd41a3a28bf5e4</anchor>
      <arglist>(const std::string &amp;name, const Policy::FilePtrArray &amp;values)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>ab510485e781cc7e7eccde5529bbe1dfa</anchor>
      <arglist>(std::ostream *out=0)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a8e58701a249f5520c6a3c5d680461bc6</anchor>
      <arglist>(const std::string &amp;file, bool append=false)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~PolicyWriter</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a667fea16d6be33a0c7eb51b26a3fd1a4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>write</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a38f910190541ce4e85340a4513ce3440</anchor>
      <arglist>(const Policy &amp;policy, bool doDecl=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>close</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a787f9df9f2c191d76b1f4120406bebce</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>toString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a6500dcebcca83473bc93dfdc42745825</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeBool</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a0e4326a3382dc59b8045850f5fcdf9b4</anchor>
      <arglist>(const std::string &amp;name, bool value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeInt</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a1c509540d384398256c054aed5ec6949</anchor>
      <arglist>(const std::string &amp;name, int value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeDouble</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a88051d8c1fa18d0fb9c4932de55aee16</anchor>
      <arglist>(const std::string &amp;name, double value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeString</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a2d72257f19392019e0246c6e9d90e2fd</anchor>
      <arglist>(const std::string &amp;name, const std::string &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writePolicy</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a5460c2a36babb8194e51bc153d13b0f4</anchor>
      <arglist>(const std::string &amp;name, const Policy &amp;value)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual void</type>
      <name>writeFile</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>a75bb343daa7b6a26e57edc0a0d7e04c2</anchor>
      <arglist>(const std::string &amp;name, const PolicyFile &amp;value)</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::string</type>
      <name>_indent</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1paf_1_1_p_a_f_writer.html</anchorfile>
      <anchor>ac9a2d3e2944363162c290d1dd59cb689</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>std::ostream *</type>
      <name>_os</name>
      <anchorfile>classlsst_1_1pex_1_1policy_1_1_policy_writer.html</anchorfile>
      <anchor>aebfdb2caa460dedf5487ccbdb4f7e16b</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>lsst::pex::policy::version</name>
    <filename>namespacelsst_1_1pex_1_1policy_1_1version.html</filename>
    <member kind="variable">
      <type>string</type>
      <name>__version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a038554e57ca3e54029cec7f057072506</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__repo_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>aa53e26dd6fa0620329dcd13ebb690542</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>string</type>
      <name>__fingerprint__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a69b9fb72046b6b5121086342ecc3e707</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__version_info__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a540434244e4b2a2e981b6d2c4519e585</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>__rebuild_version__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a8da3815b2586287632a7ad95ad147598</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>dictionary</type>
      <name>__dependency_versions__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>ab8c6bbbf1055978117267fb8850e717e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>tuple</type>
      <name>__all__</name>
      <anchorfile>namespacelsst_1_1pex_1_1policy_1_1version.html</anchorfile>
      <anchor>a2e098be756fdd85c8e8a9047353478ba</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>std</name>
    <filename>namespacestd.html</filename>
    <class kind="class">std::allocator</class>
    <class kind="class">std::array</class>
    <class kind="class">std::auto_ptr</class>
    <class kind="class">std::smart_ptr</class>
    <class kind="class">std::unique_ptr</class>
    <class kind="class">std::weak_ptr</class>
    <class kind="class">std::ios_base</class>
    <class kind="class">std::error_code</class>
    <class kind="class">std::error_category</class>
    <class kind="class">std::system_error</class>
    <class kind="class">std::error_condition</class>
    <class kind="class">std::thread</class>
    <class kind="class">std::basic_ios</class>
    <class kind="class">std::basic_istream</class>
    <class kind="class">std::basic_ostream</class>
    <class kind="class">std::basic_iostream</class>
    <class kind="class">std::basic_ifstream</class>
    <class kind="class">std::basic_ofstream</class>
    <class kind="class">std::basic_fstream</class>
    <class kind="class">std::basic_istringstream</class>
    <class kind="class">std::basic_ostringstream</class>
    <class kind="class">std::basic_stringstream</class>
    <class kind="class">std::ios</class>
    <class kind="class">std::wios</class>
    <class kind="class">std::istream</class>
    <class kind="class">std::wistream</class>
    <class kind="class">std::ostream</class>
    <class kind="class">std::wostream</class>
    <class kind="class">std::ifstream</class>
    <class kind="class">std::wifstream</class>
    <class kind="class">std::ofstream</class>
    <class kind="class">std::wofstream</class>
    <class kind="class">std::fstream</class>
    <class kind="class">std::wfstream</class>
    <class kind="class">std::istringstream</class>
    <class kind="class">std::wistringstream</class>
    <class kind="class">std::ostringstream</class>
    <class kind="class">std::wostringstream</class>
    <class kind="class">std::stringstream</class>
    <class kind="class">std::wstringstream</class>
    <class kind="class">std::basic_string</class>
    <class kind="class">std::string</class>
    <class kind="class">std::wstring</class>
    <class kind="class">std::complex</class>
    <class kind="class">std::bitset</class>
    <class kind="class">std::deque</class>
    <class kind="class">std::list</class>
    <class kind="class">std::forward_list</class>
    <class kind="class">std::map</class>
    <class kind="class">std::unordered_map</class>
    <class kind="class">std::multimap</class>
    <class kind="class">std::unordered_multimap</class>
    <class kind="class">std::set</class>
    <class kind="class">std::unordered_set</class>
    <class kind="class">std::multiset</class>
    <class kind="class">std::unordered_multiset</class>
    <class kind="class">std::vector</class>
    <class kind="class">std::queue</class>
    <class kind="class">std::priority_queue</class>
    <class kind="class">std::stack</class>
    <class kind="class">std::valarray</class>
    <class kind="class">std::exception</class>
    <class kind="class">std::bad_alloc</class>
    <class kind="class">std::bad_cast</class>
    <class kind="class">std::bad_typeid</class>
    <class kind="class">std::logic_error</class>
    <class kind="class">std::runtime_error</class>
    <class kind="class">std::bad_exception</class>
    <class kind="class">std::domain_error</class>
    <class kind="class">std::invalid_argument</class>
    <class kind="class">std::length_error</class>
    <class kind="class">std::out_of_range</class>
    <class kind="class">std::range_error</class>
    <class kind="class">std::overflow_error</class>
    <class kind="class">std::underflow_error</class>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <dir>include/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>python/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/</path>
    <filename>dir_d636e51d042aeddf07edacc9f18ac821.html</filename>
    <dir>python/lsst/pex</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/</path>
    <filename>dir_807bbb3fd749a7d186583945b5757323.html</filename>
    <dir>include/lsst/pex</dir>
  </compound>
  <compound kind="dir">
    <name>src/paf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/paf/</path>
    <filename>dir_4cb7fd0723b3503693a4f3ef2d5dd747.html</filename>
    <file>PAFParser.cc</file>
    <file>PAFParserFactory.cc</file>
    <file>PAFWriter.cc</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex/policy/paf</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/paf/</path>
    <filename>dir_2db3e28b91431191e6c4d1b114899d0c.html</filename>
    <file>PAFParser.h</file>
    <file>PAFParserFactory.h</file>
    <file>PAFWriter.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/pex/</path>
    <filename>dir_513028b95738c600a2c7d67ec6cba6e9.html</filename>
    <dir>python/lsst/pex/policy</dir>
    <file>__init__.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/</path>
    <filename>dir_da59fd778d3d02495d7b88077ed25f4c.html</filename>
    <dir>include/lsst/pex/policy</dir>
    <file>policy.h</file>
  </compound>
  <compound kind="dir">
    <name>python/lsst/pex/policy</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/lsst/pex/policy/</path>
    <filename>dir_d6f3970bd2b20b3d44476c282e1d440f.html</filename>
    <file>__init__.py</file>
    <file>version.py</file>
  </compound>
  <compound kind="dir">
    <name>include/lsst/pex/policy</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/include/lsst/pex/policy/</path>
    <filename>dir_74c8567d6d8da80fcc691f4b663f2375.html</filename>
    <dir>include/lsst/pex/policy/paf</dir>
    <file>DefaultPolicyFile.h</file>
    <file>Dictionary.h</file>
    <file>exceptions.h</file>
    <file>parserexceptions.h</file>
    <file>Policy.h</file>
    <file>PolicyConfigured.h</file>
    <file>PolicyDestination.h</file>
    <file>PolicyFile.h</file>
    <file>PolicyParser.h</file>
    <file>PolicyParserFactory.h</file>
    <file>PolicySource.h</file>
    <file>PolicyStreamDestination.h</file>
    <file>PolicyString.h</file>
    <file>PolicyStringDestination.h</file>
    <file>PolicyWriter.h</file>
    <file>SupportedFormats.h</file>
    <file>UrnPolicyFile.h</file>
  </compound>
  <compound kind="dir">
    <name>python</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/python/</path>
    <filename>dir_7837fde3ab9c1fb2fc5be7b717af8d79.html</filename>
    <dir>python/lsst</dir>
  </compound>
  <compound kind="dir">
    <name>src</name>
    <path>/des002/devel/felipe/LSSTstack/lsst.rhel6.x86_64/EupsBuildDir/Linux64/pex_policy-6.2.0.0+4/pex_policy-6.2.0.0/src/</path>
    <filename>dir_68267d1309a1af8e8297ef4c3efbcdba.html</filename>
    <dir>src/paf</dir>
    <file>DefaultPolicyFile.cc</file>
    <file>Dictionary.cc</file>
    <file>exceptions.cc</file>
    <file>parserexceptions.cc</file>
    <file>Policy.cc</file>
    <file>PolicyConfigured.cc</file>
    <file>PolicyDestination.cc</file>
    <file>PolicyFile.cc</file>
    <file>PolicyParser.cc</file>
    <file>PolicyParserFactory.cc</file>
    <file>PolicySource.cc</file>
    <file>PolicyStreamDestination.cc</file>
    <file>PolicyString.cc</file>
    <file>PolicyStringDestination.cc</file>
    <file>PolicyWriter.cc</file>
    <file>SupportedFormats.cc</file>
    <file>UrnPolicyFile.cc</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>lsst::pex::policy: configuration data management</title>
    <filename>index</filename>
  </compound>
</tagfile>
